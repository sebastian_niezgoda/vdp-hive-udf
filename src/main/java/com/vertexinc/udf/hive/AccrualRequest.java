package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;

@Description(name = "accrualRequest",
    value = "accrualRequest(company, division, destCountry, vendorClass, vendorCode, physOriginCountry, deliveryTerm, purchaseCode, extendedPrice (double);) - " +
        "O Series accrual call",
    extended = "Example: SELECT accrualRequest('10000', '10001', '19312', '90210', 'productclass', '5000') FROM hive_table; ")

public class AccrualRequest extends OSeriesUDF {

  private static final String ACCRUAL_RESPONSE = "AccrualResponse";
  private static final String SOAP_ACTION = "CalculateTax60";

  private String company;
  private String division;
  private String destCountry;
  private String vendorClass;
  private String vendorCode;
  private String physOriginCountry;
  private String deliveryTerm;
  private String purchaseCode;
  private String extendedPrice;

  protected String buildRequestBodyXML() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append("<AccrualRequest returnAssistedParametersIndicator=\"true\" transactionType=\"PURCHASE\">");
    stringBuilder.append("<Buyer>");
    stringBuilder.append(createElement("Company", company));
    stringBuilder.append(createElement("Division", division));
    stringBuilder.append("<Destination>");
    stringBuilder.append(createElement("Country", destCountry));
    stringBuilder.append("</Destination>");
    stringBuilder.append("</Buyer>");
    stringBuilder.append("<Vendor>");
    stringBuilder.append(createElementWithTagValue("VendorCode", "classCode", vendorClass, vendorCode));
    stringBuilder.append("<PhysicalOrigin>");
    stringBuilder.append(createElement("Country", physOriginCountry));
    stringBuilder.append("</PhysicalOrigin>");
    stringBuilder.append("</Vendor>");
    stringBuilder.append("<LineItem deliveryTerm=\"");
    stringBuilder.append(deliveryTerm);
    stringBuilder.append("\">");
    stringBuilder.append(createElement("Purchase", purchaseCode));
    stringBuilder.append(createElement("ExtendedPrice", extendedPrice));
    stringBuilder.append(createElement("ChargedTax", "0"));
    stringBuilder.append("</LineItem>");
    stringBuilder.append("</AccrualRequest>");

    return stringBuilder.toString();
  }

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    company = getArgumentValue(arguments, 0);
    division = getArgumentValue(arguments, 1);
    destCountry = getArgumentValue(arguments, 2);
    vendorClass = getArgumentValue(arguments, 3);
    vendorCode = getArgumentValue(arguments, 4);
    physOriginCountry = getArgumentValue(arguments, 5);
    deliveryTerm = getArgumentValue(arguments, 6);
    purchaseCode = getArgumentValue(arguments, 7);
    extendedPrice = getArgumentValue(arguments, 8);

    try {
      Double.parseDouble(extendedPrice);
    } catch (Exception e) {
      return null;
    }

    if (company == null) {
      return null;
    }

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    StringBuilder sb = new StringBuilder("<");

    String[] split = response.split(ACCRUAL_RESPONSE);
    if (split.length > 1) {
      sb.append(ACCRUAL_RESPONSE);
      sb.append(split[1].substring(0, split[1].length() - 2));
      sb.append("</");
      sb.append(ACCRUAL_RESPONSE);
      sb.append(">");
    }

    return sb.toString();
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_CALC_PARAM);
  }
}

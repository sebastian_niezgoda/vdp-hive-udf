package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Description(name = "AddMonths", value = "AddMonths(date,months) - Add months to date",
    extended = "Example: SELECT addMonths('2015-02-01', 2) FROM hive_table; ")
public class AddMonths extends VertexUDF {

  public Text evaluate(Text dateString, Integer months) {
    Text result = null;

    if (null != dateString && null != months) {
      Calendar date = Calendar.getInstance();
      SimpleDateFormat hiveDateFormatter = IVertexUDF.getHiveDataFormatter();

      try {
        date.setTime(hiveDateFormatter.parse(dateString.toString()));
        date.add(Calendar.MONTH, months);
        date.set(Calendar.DAY_OF_MONTH, 1);
        result = new Text(hiveDateFormatter.format(date.getTime()));
      } catch (ParseException e) {
      }
    }

    return result;
  }

}

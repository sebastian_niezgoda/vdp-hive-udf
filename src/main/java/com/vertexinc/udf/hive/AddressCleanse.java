package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;

@Description(name = "addressCleanse",
    value = "addressCleanse(String streetAddress1, String city, String state, String postalCode) - O Series address call",
    extended = "Example:  SELECT addressCleanse('1041 Old Cassatt Rd', 'Berwyn', 'PA', '19312') FROM hive_table; ")
public class AddressCleanse extends OSeriesUDF {

  private static final String TAXAREA_RESPONSE = "TaxAreaResponse";
  private static final String SOAP_ACTION = "LookupTaxAreas60";

  private String streetAddress1;
  private String city;
  private String state;
  private String postalCode;

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    streetAddress1 = getArgumentValue(arguments, 0);
    city = getArgumentValue(arguments, 1);
    state = getArgumentValue(arguments, 2);
    postalCode = getArgumentValue(arguments, 3);

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    String result = null;

    String[] split = response.split(TAXAREA_RESPONSE + ">");
    if (split.length > 1) {
      result = split[1].substring(0, split[1].length() - 2);
    }

    return result;
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_LOOKUP_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    StringBuilder sb = new StringBuilder();

    sb.append("<TaxAreaRequest><TaxAreaLookup><PostalAddress><StreetAddress1>");
    sb.append(streetAddress1);
    sb.append("</StreetAddress1><City>");
    sb.append(city);
    sb.append("</City><MainDivision>");
    sb.append(state);
    sb.append("</MainDivision><PostalCode>");
    sb.append(postalCode);
    sb.append("</PostalCode><Country>USA</Country></PostalAddress></TaxAreaLookup></TaxAreaRequest>");

    return sb.toString();
  }
}

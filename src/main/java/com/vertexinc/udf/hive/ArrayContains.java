package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.serde2.objectinspector.ListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
import org.apache.hadoop.io.BooleanWritable;

import java.util.List;

public class ArrayContains extends VertexGenericUDF {

  private ListObjectInspector listObjectInspector;
  private StringObjectInspector stringObjectInspector;

  @Override
  public String getDisplayString(String[] arg0) {
    return "arrayContains()";
  }

  @Override
  public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
    if (arguments.length != 2) {
      throw new UDFArgumentLengthException("Function arrayContains expects two arguments: List<T>, T");
    }
    // 1. Check we received the right object types.
    ObjectInspector a = arguments[0];
    ObjectInspector b = arguments[1];
    if (!(a instanceof ListObjectInspector) || !(b instanceof StringObjectInspector)) {
      throw new UDFArgumentException("first argument must be a list / array, second argument must be a string");
    }
    this.listObjectInspector = (ListObjectInspector) a;
    this.stringObjectInspector = (StringObjectInspector) b;

    // 2. Check that the list contains strings
    if (!(listObjectInspector.getListElementObjectInspector() instanceof StringObjectInspector)) {
      throw new UDFArgumentException("first argument must be a list of strings");
    }

    // the return type of our function is a boolean, so we provide the correct object inspector
    return PrimitiveObjectInspectorFactory.javaBooleanObjectInspector;
  }

  @Override
  public Object evaluate(DeferredObject[] arguments) throws HiveException {
    if (arguments == null || arguments[0] == null || arguments[1] == null || arguments.length != 2) {
      return null;
    }

    BooleanWritable result = new BooleanWritable();

    // get the list and string from the deferred objects using the object inspectors
    List<String> list = (List<String>) this.listObjectInspector.getList(arguments[0].get());
    String arg = stringObjectInspector.getPrimitiveJavaObject(arguments[1].get());

    // check for nulls
    if (list == null || arg == null) {
      return null;
    }

    if (list.contains(arg)) {
      result.set(true);
      return result;
    }

    result.set(false);
    return result;
  }
}
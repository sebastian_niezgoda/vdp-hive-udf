package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;

@Description(
        name = "AssetMovementRequest",
        value = "AssetMovementRequest(String company, String division, String destCountry, String physOriginCountry, " +
                "String movementMethod, String purchaseCode, double previousTaxPaid, double extendedPrice) - O Series tax calc call",
        extended = "Example: \nSELECT AssetMovementRequest('10000', '10001', 'FRANCE', 'BELGIUM', 'CONSIGNMENT'," +
                " 'purchasecode', '100', '1000') FROM hive_table; "
)
public class AssetMovementRequest extends OSeriesUDF {
  private String movementMethod;
  private String company;
  private String division;
  private String physOriginCountry;
  private String destCountry;
  private String purchaseCode;
  private String previousTaxPaid;
  private String extendedPrice;

  private static final String ASSET_MOVEMENT_RESPONSE = "AssetMovementResponse";
  private static final String SOAP_ACTION = "CalculateTax60";

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    company = getArgumentValue(arguments, 0);
    division = getArgumentValue(arguments, 1);
    destCountry = getArgumentValue(arguments, 2);
    physOriginCountry = getArgumentValue(arguments, 3);
    movementMethod = getArgumentValue(arguments, 4);
    purchaseCode = getArgumentValue(arguments, 5);
    previousTaxPaid = getArgumentValue(arguments, 6);
    extendedPrice = getArgumentValue(arguments, 7);

    try {
      Double.parseDouble(previousTaxPaid);
      Double.parseDouble(extendedPrice);
    } catch (Exception e) {
      return null;
    }

    if (company == null) {
      return null;
    }

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    if (response == null) {
      return null;
    } else {
      try {
        String[] strs = response.split(ASSET_MOVEMENT_RESPONSE);
        return "<" + ASSET_MOVEMENT_RESPONSE
                + strs[1].substring(0, strs[1].length() - 2)
                + "</" + ASSET_MOVEMENT_RESPONSE + ">";
      } catch (Exception e) {
        return null;
      }
    }
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_CALC_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    StringBuilder stringBuilder = new StringBuilder();

    stringBuilder.append("<AssetMovementRequest returnAssistedParametersIndicator=\"true\" movementMethod=\"");
    stringBuilder.append(movementMethod);
    stringBuilder.append("\">");
    stringBuilder.append("<Owner>");
    stringBuilder.append("<Company>");
    stringBuilder.append(company);
    stringBuilder.append("</Company>");
    stringBuilder.append("<Division>");
    stringBuilder.append(division);
    stringBuilder.append("</Division>");
    stringBuilder.append("<PhysicalOrigin>");
    stringBuilder.append("<Country>");
    stringBuilder.append(physOriginCountry);
    stringBuilder.append("</Country>");
    stringBuilder.append("</PhysicalOrigin>");
    stringBuilder.append("<Destination>");
    stringBuilder.append("<Country>");
    stringBuilder.append(destCountry);
    stringBuilder.append("</Country>");
    stringBuilder.append("</Destination>");
    stringBuilder.append("</Owner>");
    stringBuilder.append("<LineItem>");
    stringBuilder.append("<Purchase>");
    stringBuilder.append(purchaseCode);
    stringBuilder.append("</Purchase>");
    stringBuilder.append("<PreviousTaxPaid>");
    stringBuilder.append(previousTaxPaid);
    stringBuilder.append("</PreviousTaxPaid>");
    stringBuilder.append("<ExtendedPrice>");
    stringBuilder.append(extendedPrice);
    stringBuilder.append("</ExtendedPrice>");
    stringBuilder.append("</LineItem>");
    stringBuilder.append("</AssetMovementRequest>");
    stringBuilder.append("</VertexEnvelope></soapenv:Body></soapenv:Envelope>");

    return stringBuilder.toString();
  }
}

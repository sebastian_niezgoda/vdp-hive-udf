package com.vertexinc.udf.hive;

import jodd.typeconverter.Convert;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;

@Description(name = "calcTax", value = "calcTax("
        + "String company, "
        + "String division, "
        + "String shipFromPostalCode, "
        + "String shipToPostalCode, "
        + "String product, "
        + "String extendedPrice, "
        + "String shipFromState, "
        + "String shipFromCountry, "
        + "String billFromState, "
        + "String billFromPostalCode, "
        + "String billFromCountry,"
        + "String shipToState, "
        + "String shipToCountry,"
        + "String billToState, "
        + "String billToPostalCode,"
        + "String billToCountry, "
        + "String customerCode, "
        + "String customerRegistration, "
        + "String deliveryTerm, "
        + "String currencyCode"
        + " ) - "
        + "O Series tax calc call", extended = "Example: \n"
        + " SELECT calcTax('10000', '10001', '85254', '19312', 'productcodehere', '5000', 'AZ', 'USA', 'AZ', '85254', 'USA', 'PA', 'USA', 'PA', '19312', 'USA', 'CUST001', '123455', 'CUS', 'USD' ) FROM hive_table; ")
public class CalcTax extends OSeriesUDF {
  private static final String QUOTATION_RESPONSE = "QuotationResponse";
  private static final String SOAP_ACTION = "CalculateTax60";

  private String company;
  private String division;
  private String shipFromPostalCode;
  private String shipToPostalCode;
  private String product;
  private String extendedPrice;

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    company = getArgumentValue(arguments, 0);
    division = getArgumentValue(arguments, 1);
    shipFromPostalCode = getArgumentValue(arguments, 2);
    shipToPostalCode = getArgumentValue(arguments, 3);
    product = getArgumentValue(arguments, 4);
    extendedPrice = getArgumentValue(arguments, 5);

    try {
      Double.parseDouble(extendedPrice);
    } catch (Exception e) {
      return null;
    }

    if (company == null) {
      return null;
    }

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    if (response == null)
      return null;
    try {
      String[] strs = response.split(QUOTATION_RESPONSE);
      return "<" + QUOTATION_RESPONSE + strs[1].substring(0, strs[1].length() - 2) + "</" + QUOTATION_RESPONSE
              + ">";
    } catch (Exception e) {
      return null;
    }
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_CALC_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    String result = "<QuotationRequest transactionType=\"SALE\">"
            + "<LineItem>"
            + "<Seller>"
            + createElement("Company", company)
            + createElement("Division", division);
    if (shipFromPostalCode != null && !shipFromPostalCode.trim().isEmpty()) {
      result += "<PhysicalOrigin>"
              + createElement("PostalCode", shipFromPostalCode)
              + "</PhysicalOrigin>";
    }
    result += "</Seller>"
            + "<Customer>"
            + "<Destination>"
            + createElement("PostalCode", shipToPostalCode)
            + "</Destination>"
            + "</Customer>"
            + createElement("Product", product)
            + createElement("ExtendedPrice", Convert.toString(extendedPrice))
            + "</LineItem>"
            + "</QuotationRequest>";

    return result;
  }
}

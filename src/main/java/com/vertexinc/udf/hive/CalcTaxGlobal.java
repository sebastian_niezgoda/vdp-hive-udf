package com.vertexinc.udf.hive;

import com.google.common.base.Strings;
import jodd.typeconverter.Convert;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;

import java.util.Objects;

// this is the documentation from the field dev code, which shows the parameters in the wrong order
//@Description(name = "calcTaxGlobal", value = "calcTaxGlobal(" +
//        "String company, " +
//        "String division, " +
//        "String shipFromPostalCode, " +
//        "String shipToPostalCode, " +
//        "String product, " +
//        "String extendedPrice, " +
//        "String shipFromState, " +
//        "String shipFromCountry, " +
//        "String billFromState, " +
//        "String billFromPostalCode, " +
//        "String billFromCountry," +
//        "String shipToState, " +
//        "String shipToCountry," +
//        "String billToState, " +
//        "String billToPostalCode," +
//        "String billToCountry, " +
//        "String customerCode, " +
//        "String customerRegistration, " +
//        "String deliveryTerm, " +
//        "String currencyCode" + " ) - "
//        + "O Series tax calc call", extended = "Example: \n"
//        + " SELECT calcTax('10000', '10001', '85254', '19312', 'product', '5000', 'AZ', 'USA', 'AZ', '85254', 'USA', 'PA', 'USA', 'PA', '19312', 'USA', 'CUST001', '123455', 'CUS', 'USD' ) FROM hive_table; ")
public class CalcTaxGlobal extends OSeriesUDF {
  private static final String QUOTATION_RESPONSE = "QuotationResponse";
  private static final String SOAP_ACTION = "CalculateTax60";

  private String company = "";
  private String division = "";
  private String shipFromPostalCode = "";
  private String shipToPostalCode = "";
  private String product = "";
  private String extendedPrice = "";
  private String shipFromState = "";
  private String shipFromCountry = "";
  private String billFromState = "";
  private String billFromPostalCode = "";
  private String billFromCountry = "";
  private String shipToState = "";
  private String shipToCountry = "";
  private String billToState = "";
  private String billToPostalCode = "";
  private String billToCountry = "";
  private String customerCode = "";
  private String customerRegistration = "";
  private String deliveryTerm = "";
  private String currencyCode = "";

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    // these were in the wrong order, even though they follow the order of the documentation
//    company = getArgumentValue(arguments, 0);
//    division = getArgumentValue(arguments, 1);
//    shipFromPostalCode = getArgumentValue(arguments, 2);
//    shipToPostalCode = getArgumentValue(arguments, 3);
//    product = getArgumentValue(arguments, 4);
//    extendedPrice = getArgumentValue(arguments, 5);
//    shipFromState = getArgumentValue(arguments, 6);
//    shipFromCountry = getArgumentValue(arguments, 7);
//    billFromState = getArgumentValue(arguments, 8);
//    billFromPostalCode = getArgumentValue(arguments, 9);
//    billFromCountry = getArgumentValue(arguments, 10);
//    shipToState = getArgumentValue(arguments, 11);
//    shipToCountry = getArgumentValue(arguments, 12);
//    billToState = getArgumentValue(arguments, 13);
//    billToPostalCode = getArgumentValue(arguments, 14);
//    billToCountry = getArgumentValue(arguments, 15);
//    customerCode = getArgumentValue(arguments, 16);
//    customerRegistration = getArgumentValue(arguments, 17);
//    deliveryTerm = getArgumentValue(arguments, 18);
//    currencyCode = getArgumentValue(arguments, 19);

    shipToCountry = getArgumentValue(arguments, 0);
    extendedPrice = getArgumentValue(arguments, 1);
    shipToState = getArgumentValue(arguments, 2);
    shipToPostalCode = getArgumentValue(arguments, 3);
    shipFromState = getArgumentValue(arguments, 4);
    shipFromPostalCode = getArgumentValue(arguments, 5);
    shipFromCountry = getArgumentValue(arguments, 6);
    billToState = getArgumentValue(arguments, 7);
    billToPostalCode = getArgumentValue(arguments, 8);
    billToCountry = getArgumentValue(arguments, 9);
    billFromState = getArgumentValue(arguments, 10);
    billFromPostalCode = getArgumentValue(arguments, 11);
    billFromCountry = getArgumentValue(arguments, 12);
    company = getArgumentValue(arguments, 13);
    division = getArgumentValue(arguments, 14);
    customerCode = getArgumentValue(arguments, 15);
    customerRegistration = getArgumentValue(arguments, 16);
    currencyCode = getArgumentValue(arguments, 17);
    deliveryTerm = getArgumentValue(arguments, 18);
    product = getArgumentValue(arguments, 19);

    try {
      if (!Strings.isNullOrEmpty(extendedPrice)) {
        Double.parseDouble(extendedPrice);
      } else {
        return null;
      }
    } catch (NumberFormatException nfe) {
      return null;
    }

    // This check is performed in the field dev code, but it is performed incorrectly. I have commented it out to
    // emulate how the field dev code actually performs.
//    if (company == null) {
//      return null;
//    }

    if (currencyCode == null) {
      currencyCode = "USD";
    }

    if (!Objects.equals(shipToCountry, "USA") && !Objects.equals(shipToCountry, "United States")
            && !Strings.isNullOrEmpty(shipToCountry)) {
      if (Strings.isNullOrEmpty(deliveryTerm)) {
        deliveryTerm = "CUS";
      }
    }

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    if (response == null)
      return null;
    try {
      String[] strs = response.split(QUOTATION_RESPONSE);
      return "<" + QUOTATION_RESPONSE + strs[1].substring(0, strs[1].length() - 2) + "</" + QUOTATION_RESPONSE
              + ">";
    } catch (Exception e) {
      return null;
    }
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_CALC_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    String result = "<QuotationRequest transactionType=\"SALE\">" +
            "<Currency isoCurrencyCodeAlpha=\"" + currencyCode + "\"/>";

    result += "<LineItem";
    if (!isNullOrEmpty(deliveryTerm)) {
      result += " deliveryTerm=\"" + deliveryTerm + "\"";
    }
    result += ">";

    result += "<Seller>" +
            createElement("Company", company) +
            createElement("Division", division);

    if (!isNullOrEmpty(shipFromState) || !isNullOrEmpty(shipFromPostalCode) || !isNullOrEmpty(shipFromCountry)) {
      result += "<PhysicalOrigin>" +
              createElement("MainDivision", shipFromState) +
              createElement("PostalCode", shipFromPostalCode) +
              createElement("Country", shipFromCountry) +
              "</PhysicalOrigin>";
    }

    if (!isNullOrEmpty(billFromState) || !isNullOrEmpty(billFromPostalCode) || !isNullOrEmpty(billFromCountry)) {
      result += "<AdministrativeOrigin>" +
              createElement("MainDivision", billFromState) +
              createElement("PostalCode", billFromPostalCode) +
              createElement("Country", billFromCountry) +
              "</AdministrativeOrigin>";
    }

    result += "</Seller>" +
            "<Customer>" +
            createElement("CustomerCode", customerCode);

    if (!isNullOrEmpty(customerRegistration) && shipToCountry.length() <= 3) {
      result += "<TaxRegistration isoCountryCode=\"" + shipToCountry + "\">" +
              createElement("TaxRegistrationNumber", customerRegistration) +
              "</TaxRegistration>";
    }

    result += "<Destination>" +
            createElement("MainDivision", shipToState) +
            createElement("PostalCode", shipToPostalCode) +
            createElement("Country", shipToCountry) +
            "</Destination>";

    if (!isNullOrEmpty(billToState) || !isNullOrEmpty(billToPostalCode) || !isNullOrEmpty(billToCountry)) {
      result += "<AdministrativeDestination>" +
              createElement("MainDivision", billToState) +
              createElement("PostalCode", billToPostalCode) +
              createElement("Country", billToCountry) +
              "</AdministrativeDestination>";
    }

    result += "</Customer>" +
            createElement("Product", product) +
            createElement("ExtendedPrice", Convert.toString(extendedPrice)) +
            "</LineItem>" +
            "</QuotationRequest>";

    return result;
  }
}

package com.vertexinc.udf.hive;

import jodd.typeconverter.Convert;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

//these parameters are in the wrong order, this documentation is from field dev code
//@Description(name = "calcTaxGlobalPO", value = "calcTaxGlobalPO(" +
//        "String company, " +
//        "String division, " +
//        "String shipFromPostalCode, " +
//        "String shipToPostalCode, " +
//        "String product, " +
//        "String extendedPrice, " +
//        "String shipFromState, " +
//        "String shipFromCountry, " +
//        "String billFromState, " +
//        "String billFromPostalCode, " +
//        "String billFromCountry," +
//        "String shipToState, " +
//        "String shipToCountry," +
//        "String billToState, " +
//        "String billToPostalCode," +
//        "String billToCountry, " +
//        "String vendorCode, " +
//        "String vendorRegistration, " +
//        "String deliveryTerm, " +
//        "String currencyCode," +
//        "String taxDate" +
//        "String calcVendorTax ) - " + "O Series tax calc call", extended = "Example: \n"
//        + " SELECT calcTaxGlobalPO('10000', '10001', '85254', '19312', 'product', '5000', 'AZ', 'USA', 'AZ', '85254', 'USA', 'PA', 'USA', 'PA', '19312', 'USA', 'CUST001', '123455', 'CUS', 'USD', '2017-10-31' ) FROM hive_table; ")
public class CalcTaxGlobalPO extends OSeriesUDF {
  private static final String PURCHASE_ORDER_RESPONSE = "PurchaseOrderResponse";
  private static final String SOAP_ACTION = "CalculateTax60";

  String company;
  String division;
  String shipFromPostalCode;
  String shipToPostalCode;
  String product;
  String extendedPrice;
  String shipFromState;
  String shipFromCountry;
  String billFromState;
  String billFromPostalCode;
  String billFromCountry;
  String shipToState;
  String shipToCountry;
  String billToState;
  String billToPostalCode;
  String billToCountry;
  String vendorCode;
  String vendorRegistration;
  String deliveryTerm;
  String currencyCode;
  String taxDate;
  String calcVendorTax;

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    // these were in the wrong order, even though they follow the order of the documentation
//    company = getArgumentValue(arguments, 0);
//    division = getArgumentValue(arguments, 1);
//    shipFromPostalCode = getArgumentValue(arguments, 2);
//    shipToPostalCode = getArgumentValue(arguments, 3);
//    product = getArgumentValue(arguments, 4);
//    extendedPrice = getArgumentValue(arguments, 5);
//    shipFromState = getArgumentValue(arguments, 6);
//    shipFromCountry = getArgumentValue(arguments, 7);
//    billFromState = getArgumentValue(arguments, 8);
//    billFromPostalCode = getArgumentValue(arguments, 9);
//    billFromCountry = getArgumentValue(arguments, 10);
//    shipToState = getArgumentValue(arguments, 11);
//    shipToCountry = getArgumentValue(arguments, 12);
//    billToState = getArgumentValue(arguments, 13);
//    billToPostalCode = getArgumentValue(arguments, 14);
//    billToCountry = getArgumentValue(arguments, 15);
//    vendorCode = getArgumentValue(arguments, 16);
//    vendorRegistration = getArgumentValue(arguments, 17);
//    deliveryTerm = getArgumentValue(arguments, 18);
//    currencyCode = getArgumentValue(arguments, 19);
//    taxDate = getArgumentValue(arguments, 20);
//    calcVendorTax = getArgumentValue(arguments, 21);

    shipToCountry = getArgumentValue(arguments, 0);
    extendedPrice = getArgumentValue(arguments, 1);
    shipToState = getArgumentValue(arguments, 2);
    shipToPostalCode = getArgumentValue(arguments, 3);
    shipFromState = getArgumentValue(arguments, 4);
    shipFromPostalCode = getArgumentValue(arguments, 5);
    shipFromCountry = getArgumentValue(arguments, 6);
    billToState = getArgumentValue(arguments, 7);
    billToPostalCode = getArgumentValue(arguments, 8);
    billToCountry = getArgumentValue(arguments, 9);
    billFromState = getArgumentValue(arguments, 10);
    billFromPostalCode = getArgumentValue(arguments, 11);
    billFromCountry = getArgumentValue(arguments, 12);
    company = getArgumentValue(arguments, 13);
    division = getArgumentValue(arguments, 14);
    vendorCode = getArgumentValue(arguments, 15);
    vendorRegistration = getArgumentValue(arguments, 16);
    currencyCode = getArgumentValue(arguments, 17);
    deliveryTerm = getArgumentValue(arguments, 18);
    product = getArgumentValue(arguments, 19);
    taxDate = getArgumentValue(arguments, 20);
    calcVendorTax = getArgumentValue(arguments, 21);


    if (!isNullOrEmpty(taxDate)) {
      // validate tax date
      try {
        SimpleDateFormat sdf = new SimpleDateFormat(HIVE_DATE_FORMAT);
        Date date = sdf.parse(taxDate);
        if (!taxDate.equals(sdf.format(date))) {
          taxDate = null;
        }
      } catch (ParseException ex) {
        return null;
      }
    }

    try {
      if (!isNullOrEmpty(extendedPrice)) {
        Double.parseDouble(extendedPrice);
      } else {
        LOGGER.warn("Extended price is null");
      }
    } catch (NumberFormatException nfe) {
      return null;
    }

//    if (company == null) {
//      return null;
//    }

//    if (currencyCode == null) {
//      currencyCode = "USD";
//    }

    if (!Objects.equals(shipToCountry, "USA") && !Objects.equals(shipToCountry, "United States")
            && !isNullOrEmpty(shipToCountry)) {
      if (isNullOrEmpty(deliveryTerm)) {
        deliveryTerm = "CUS";
      }
    }

    return buildRequest();
  }

  @Override
  protected String cleanResponse(String response) {
    // LOG.info(response);
    if (response == null)
      return null;
    try {
      String[] strs = response.split(PURCHASE_ORDER_RESPONSE);
      String tranformedResponse = "<" + PURCHASE_ORDER_RESPONSE + strs[1].substring(0, strs[1].length() - 2)
              + "</" + PURCHASE_ORDER_RESPONSE + ">";
      return tranformedResponse;
    } catch (Exception e) {

      return null;
    }
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_CALC_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    String result = "<PurchaseOrderRequest";
    result += (!isNullOrEmpty(calcVendorTax) && calcVendorTax.equalsIgnoreCase("y")) ?
            " calculateVendorTaxIndicator=\"true\"" : "";
    result += !isNullOrEmpty(taxDate) ? " documentDate=\"" + taxDate + "\"" : "";
    result += " returnAssistedParametersIndicator=\"true\"";
    result += " transactionType=\"PURCHASE\"";
    result += ">";

    result += "<Currency isoCurrencyCodeAlpha=\"" + currencyCode + "\"/>";
    result += "<LineItem";

    if (!isNullOrEmpty(deliveryTerm)) {
      result += " deliveryTerm=\"" + deliveryTerm + "\"";
    }

    if (!isNullOrEmpty(taxDate)) {
      result += " documentDate=\"" + taxDate + "\"";
    }
    result += ">";
    result += "<Buyer>";
    result += createElement("Company", company);
    result += createElement("Division", division);

    if (!isNullOrEmpty(shipToState) || !isNullOrEmpty(shipToPostalCode) || !isNullOrEmpty(shipToCountry)) {
      result += "<Destination>" +
              createElement("MainDivision", shipToState) +
              createElement("PostalCode", shipToPostalCode) +
              createElement("Country", shipToCountry) +
              "</Destination>";
    }

    if (!isNullOrEmpty(billToState) || !isNullOrEmpty(billToPostalCode) || !isNullOrEmpty(billToCountry)) {
      result += "<AdministrativeDestination>" +
              createElement("MainDivision", billToState) +
              createElement("PostalCode", billToPostalCode) +
              createElement("Country", billToCountry) +
              "</AdministrativeDestination>";
    }

    result += "</Buyer><Vendor>";
    result += createElement("VendorCode", vendorCode);
    if (!isNullOrEmpty(vendorRegistration) && shipToCountry.length() <= 3) {
      result += "<TaxRegistration isoCountryCode=\"" + shipToCountry + "\">" +
              createElement("TaxRegistrationNumber", vendorRegistration) +
              "</TaxRegistration>";
    }

    if (!isNullOrEmpty(shipFromState) || !isNullOrEmpty(shipFromPostalCode) || !isNullOrEmpty(shipFromCountry)) {
      result += "<PhysicalOrigin>" +
              createElement("MainDivision", shipFromState) +
              createElement("PostalCode", shipFromPostalCode) +
              createElement("Country", shipFromCountry) +
              "</PhysicalOrigin>";
    }

    if (!isNullOrEmpty(billFromState) || !isNullOrEmpty(billFromPostalCode) || !isNullOrEmpty(billFromCountry)) {
      result += "<AdministrativeOrigin>" +
              createElement("MainDivision", billFromState) +
              createElement("PostalCode", billFromPostalCode) +
              createElement("Country", billFromCountry) +
              "</AdministrativeOrigin>";
    }
    result += "</Vendor>";
    result += createElement("Purchase", product) +
            createElement("ExtendedPrice", Convert.toString(extendedPrice)) +
            "</LineItem>" +
            "</PurchaseOrderRequest>";

    return result;
  }
}

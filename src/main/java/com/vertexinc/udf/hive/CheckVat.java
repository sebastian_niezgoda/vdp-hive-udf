package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.io.Text;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Description(name = "CheckVat", value = "CheckVat(str,str) - Check VAT number", extended = "Example: \n"
        + " SELECT checkVat(countrycode, vatnumber) FROM hive_table; ")
public class CheckVat extends VertexUDF {
  //  private final static String CACHED_VIES_URL = "http://ip-172-31-20-128.us-west-1.compute.internal:9000/";
  //  private final static String VIES_URL = "http://localhost:9000/";
  private final static String VIES_URL = "http://ec.europa.eu/taxation_customs/vies/services/checkVatService";
  final static String PROCESSING_ERROR = "ERROR - STOPPED PROCESSING";
  final static String HOURLY_LIMIT_ERROR = "ERROR - HOURLY LIMIT REACHED";
  final static String VIES_UNAVAILABLE_ERROR = "ERROR - VIES UNAVAILABLE";

  //  public static ConcurrentHashMap<String, Text> vatMap = new ConcurrentHashMap<String, Text>();
  private static ConcurrentHashMap<String, String> responseXmlMap = new ConcurrentHashMap<>();
  private static ConcurrentHashMap<String, Integer> hourlyReqCounts = new ConcurrentHashMap<>();

  private long invalidReqCount = 0L;
  private long invalidReqLimit = 10L;
  private long invalidReqResetTimeout = 60L; // minutes
  private Integer hourlyReqLimit = 25000;
  private Boolean initialized = false;
  // this value indicates the hour of the day that the last request was made
  private static int hourOfLastReq = 0;
  private static Instant lastInvalidRequest = Instant.now();

  public void clearCounters() {
    hourlyReqCounts.clear();
    invalidReqCount = 0L;
  }

  public void resetInitializedMarker() {
    initialized = false;
  }

  private void initialize() {
    LOGGER.info("Initializing CheckVat");
    SessionState sessionState = SessionState.get();

    if (sessionState != null) {
      setLoggedInApplication(sessionService.getLoggedInApplication(sessionState));
    }
    LOGGER.debug(String.format("Logged in application: %s", getLoggedInApplication()));
    if (getLoggedInApplication() != null) {
      LOGGER.debug(String.format("Loading UDF properties for application %s", getLoggedInApplication()));
      Properties udf = propService.loadUdfThresholdConfiguration(getLoggedInApplication());
      if (udf != null) {
        setUdfConfig(udf);
      }
    }
    Object maxReq = getUdfConfig().get(UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM);
    if (maxReq != null && !maxReq.toString().isEmpty()) {
      try {
        invalidReqLimit = Long.valueOf(maxReq.toString());
      } catch (Exception e) {
        LOGGER.error(String.format("Value of consecutive invalid request limit is not set or not set properly. The default value of %s will be used.", invalidReqLimit));
        LOGGER.error(e.toString());
      }
    }
    LOGGER.info(String.format("Invalid request limit: %s", invalidReqLimit));

    Object maxHourlyReq = getUdfConfig().get(UDF_MAX_HOURLY_VIES_REQ_PARAM);
    if (maxHourlyReq != null && !maxHourlyReq.toString().isEmpty()) {
      try {
        hourlyReqLimit = Integer.valueOf(maxHourlyReq.toString());
      } catch (Exception e) {
        LOGGER.error(String.format("Value of max hourly request limit is not set or not set properly. The default value of %s will be used.", hourlyReqLimit));
        LOGGER.error(e.toString());
      }
    }
    LOGGER.info(String.format("Hourly request limit: %s", hourlyReqLimit));

    Object resetTimeout = getUdfConfig().get(UDF_VIES_INVALID_RESET_TIMEOUT_REQ_PARAM);
    if (resetTimeout != null && !resetTimeout.toString().isEmpty()) {
      try {
        invalidReqResetTimeout = Long.valueOf(resetTimeout.toString());
      } catch (Exception e) {
        LOGGER.error(String.format("Value of invalid request reset timeout is not set or not set properly. The default value of %s will be used.", invalidReqResetTimeout));
        LOGGER.error(e.toString());
      }
    }
    LOGGER.info(String.format("Invalid request reset timeout: %s", invalidReqResetTimeout));
  }

  public Text evaluate(Text countryCode, Text vatNum, List<Text> fields) {
    if (countryCode == null || vatNum == null || fields == null) {
      return null;
    }

    // force to be uppercase, API does not like lowercase letters
    countryCode = new Text(countryCode.toString().toUpperCase());
    vatNum = new Text(vatNum.toString().toUpperCase());

    // only initialize once, not every time this function is called
    if (!initialized) {
      initialize();
      initialized = Boolean.TRUE;
    }

    String response = checkVat(countryCode.toString(), vatNum.toString(), VIES_URL);

    if (isNullOrEmpty(response)) {
      return new Text("");
    }

    if (response.indexOf("<faultstring>") > 0) {
      fields.clear();
      fields.add(new Text("faultstring"));
    }

    if (response.contains(PROCESSING_ERROR) || response.contains(HOURLY_LIMIT_ERROR) || response.contains(VIES_UNAVAILABLE_ERROR)) {
      return new Text(response);
    } else {
      StringBuilder fullResponse = new StringBuilder();

      for (Iterator<Text> it = fields.iterator(); it.hasNext(); ) {
        Text resp = it.next();
        String taxResponse = getTaxResponse(response, resp.toString());

        fullResponse.append(it.hasNext() ? taxResponse + "|" : taxResponse);
      }

      return fullResponse.length() == 0 ? null : new Text(fullResponse.toString());
    }
  }

  public Text evaluate(Text countryCode, Text vatNum, Text field) {
    ArrayList<Text> fields = new ArrayList<>();
    fields.add(field);
    return evaluate(countryCode, vatNum, fields);
  }

  public Text evaluate(Text countryCode, Text vatNum) {
    ArrayList<Text> fields = new ArrayList<>();

    fields.add(new Text("requestDate"));
    fields.add(new Text("valid"));
    fields.add(new Text("name"));
    fields.add(new Text("address"));

    return evaluate(countryCode, vatNum, fields);
  }

  protected String checkVat(String countryCode, String vatNum, String url) {
    String key = countryCode + "|" + vatNum;

    if (responseXmlMap.containsKey(key)) {
      return responseXmlMap.get(key);
    }

    checkAndResetHourlyCounter();
    checkAndResetInvalidReqCounter();

    LOGGER.debug(String.format("Current invalid request count: %s", invalidReqCount));
    if (invalidReqCount >= invalidReqLimit) {
      LOGGER.error(String.format("This call has exceeded the limit of %s consecutive invalid requests.", invalidReqLimit));
      return PROCESSING_ERROR;
    }

    LOGGER.debug(String.format("Current hourly request count: %s", hourlyReqCounts.getOrDefault(countryCode, 0)));
    if (hourlyReqCounts.getOrDefault(countryCode, 0) >= hourlyReqLimit) {
      LOGGER.error(String.format("This call has exceeded the limit of %s hourly requests.", hourlyReqLimit));
      return HOURLY_LIMIT_ERROR;
    }

    String xml = getXml(countryCode, vatNum);
    HttpURLConnection conn;
    try {
      conn = (HttpURLConnection) new URL(url).openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod("POST"); // POST in case XML attachment is too
      // large for GET
      conn.setRequestProperty("Content-Type", "text/xml");

      OutputStream os = null;

      try {
        os = conn.getOutputStream();
      } catch (IOException ioe) {
        LOGGER.error(String.format("Unable to send request. Country code: %s; Vat number: %s", countryCode, vatNum));
        recordInvalidRequest();
        return VIES_UNAVAILABLE_ERROR;
      }

      Writer wout = new OutputStreamWriter(os);

      //System.out.println(xml);

      wout.write(xml); // set XML in SOAP envelope and write to
      // outputstream
      wout.flush();
      wout.close();

      // read the inputstream (the XML response)
      BufferedReader in = new BufferedReader(new InputStreamReader(
              conn.getInputStream()));

      StringBuffer returnBuff = new StringBuffer(1000);
      String inputLine;
      while ((inputLine = in.readLine()) != null)
        returnBuff.append(inputLine);

      in.close();
      conn.disconnect();

      Integer currentCount = hourlyReqCounts.getOrDefault(countryCode, 0);
      hourlyReqCounts.put(countryCode, currentCount + 1);

      responseXmlMap.putIfAbsent(key, returnBuff.toString());

      /*
      We retain the response as a string instead of converting it to a boolean because we are interested in three particular
      scenarios; "true", "false", or anything else. Anything else indicates an unexpected response, and should not be handled
      the same was a "false" response. Parsing the strings as booleans would lump the "false" and "anything else" categories
      together.
       */
      String isValidRequest = getTaxResponse(returnBuff.toString(), "valid");
      /*
      If the request is not valid, then add it to the count of consecutive invalid calls.
      If it is valid, reset the counter to zero.
       */
      if (isValidRequest == null || isValidRequest.equals("false")) {
        recordInvalidRequest();
      } else if (isValidRequest.equals("true")) {
        invalidReqCount = 0L;
      } else {
        LOGGER.warn("Unexpected checkVat response from " + url + " with country code " + countryCode + " and vatNum " + vatNum);
      }

      return responseXmlMap.get(key);
    } catch (Exception e) {
      LOGGER.error(e.toString());
      recordInvalidRequest();
      return VIES_UNAVAILABLE_ERROR;
    }
  }

  private String getXml(String countryCode, String vatNum) {
    return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" " +
            "xmlns:urn=\"urn:ec.europa.eu:taxud:vies:services:checkVat:types\">" +
            //"<soapenv:Header/>" +
            "<soapenv:Body>" +
            "<urn:checkVat>" +
            "<urn:countryCode>" + countryCode + "</urn:countryCode>" +
            "<urn:vatNumber>" + vatNum + "</urn:vatNumber>" +
            "</urn:checkVat>" +
            "</soapenv:Body>" +
            "</soapenv:Envelope>";
  }

  protected String getTaxResponse(String response, String field) {
    if (isNullOrEmpty(response))
      return null;

    try {
      String splitStr = field + ">";
      String[] strs = response.split(splitStr);
      strs = strs[1].split("<");
      return strs[0];
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Checks the hour of the last request in comparison to the current hour, and if the hour has changed, clear the
   * current request counts, because the hourly limit must be reset for the new hour.
   *
   * @return true if the current request counts have been reset because the hour is new, false if nothing happened
   */
  private Boolean checkAndResetHourlyCounter() {
    if (hourOfLastReq < Calendar.getInstance().get(Calendar.HOUR_OF_DAY)) {
      hourlyReqCounts.clear();
      hourOfLastReq = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
      return true;
    } else {
      hourOfLastReq = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
      return false;
    }
  }

  /**
   * Checks the time since the last invalid request. If that time exceeds the threshold, resets the invalid request counter.
   *
   * @return true if the invalid request counter has been reset, false if nothing happened
   */
  private Boolean checkAndResetInvalidReqCounter() {
    Duration sinceLast = Duration.between(lastInvalidRequest, Instant.now());
    if (sinceLast.toMinutes() >= invalidReqResetTimeout) {
      invalidReqCount = 0L;
      LOGGER.debug(String.format("Reset invalid request counter because it has been %s minutes since last invalid request", sinceLast.toMinutes()));
      return true;
    } else {
      LOGGER.debug(String.format("Not resetting invalid request counter because it has only been %s minutes, fewer than the threshold of %s minutes", sinceLast.toMinutes(), invalidReqResetTimeout));
      return false;
    }
  }

  private void recordInvalidRequest() {
    lastInvalidRequest = Instant.now();
    invalidReqCount++;
  }
}

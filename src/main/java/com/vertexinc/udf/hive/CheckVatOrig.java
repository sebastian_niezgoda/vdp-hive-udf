package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.apache.hadoop.io.Text;

@Description(name = "CheckVat", value = "CheckVat(str,str) - Check VAT number", extended = "Example: \n"
        + " SELECT checkVat(countrycode, vatnumber) FROM hive_table; ")
public class CheckVatOrig extends UDF {

  private static CheckVat checkVat = new CheckVat();
  private static final String DEFAULT_FIELD = "valid";
  static final String VALID_RESP = "VALID";
  static final String INVALID_RESP = "INVALID";

  public Text evaluate(Text countryCode, Text vatNum) {
    return convertResponse(evaluate(countryCode, vatNum, new Text(DEFAULT_FIELD)));
  }

  public Text evaluate(Text countryCode, Text vatNum, Text field){
    return convertResponse(checkVat.evaluate(countryCode, vatNum, field));
  }

  public CheckVat getCheckVat(){
    return checkVat;
  }

  private Text convertResponse(Text response){
    switch(response.toString()){
      case "true":
        return new Text(VALID_RESP);
      case "false":
        return new Text(INVALID_RESP);
      case "INVALID_INPUT":
        return new Text(INVALID_RESP);
      default:
        return response;
    }
  }
}

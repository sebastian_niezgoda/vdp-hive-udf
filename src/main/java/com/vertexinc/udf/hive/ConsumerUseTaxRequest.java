package com.vertexinc.udf.hive;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Map;

public class ConsumerUseTaxRequest extends VertexUDF {

  private static Document outDoc;

  public ConsumerUseTaxRequest(Document inDoc, Map<String, String> oseriesparms, String financialEventName) {
    setOutDoc(inDoc);

    // financial event attributes
    String documentNumber = oseriesparms.get("documentNumber");
    String returnAssistedParametersIndicator = oseriesparms.get("returnAssistedParametersIndicator");
    String returnGeneratedLineItemsIndicator = oseriesparms.get("returnGeneratedLineItemsIndicator");
    String documentDate = oseriesparms.get("documentDate");
    String transactionId = oseriesparms.get("transactionId");
    String calculateVendorTaxIndicator = oseriesparms.get("calculateVendorTaxIndicator");
    String postToJournal = oseriesparms.get("postToJournal");
    String isTaxOnlyAdjustmentIndicator = oseriesparms.get("isTaxOnlyAdjustmentIndicator");

    // currency
    String currencyIsoCurrencyName = oseriesparms.get("Currency-isoCurrencyName");
    String currencyIsoCurrencyCodeAlpha = oseriesparms.get("Currency-isoCurrencyCodeAlpha");
    String currencyIsoCurrencyCodeNum = oseriesparms.get("Currency-isoCurrencyCodeNum");

    // original currency
    String originalCurrencyIsoCurrencyName = oseriesparms.get("OriginalCurrency-isoCurrencyName");
    String originalCurrencyIsoCurrencyCodeAlpha = oseriesparms.get("OriginalCurrency-isoCurrencyCodeAlpha");
    String originalCurrencyIsoCurrencyCodeNum = oseriesparms.get("OriginalCurrency-isoCurrencyCodeNum");

    // line item attributes
    String lineItemNumber = oseriesparms.get("lineItemNumber");
    String taxDate = oseriesparms.get("taxDate");
    String isMulticomponent = oseriesparms.get("isMulticomponent");
    String locationCode = oseriesparms.get("locationCode");
    String deliveryTerm = oseriesparms.get("deliveryTerm");
    String postingDate = oseriesparms.get("postingDate");
    String costCenter = oseriesparms.get("costCenter");
    String departmentCode = oseriesparms.get("departmentCode");
    String generalLedgerAccount = oseriesparms.get("generalLedgerAccount");
    String materialCode = oseriesparms.get("materialCode");
    String projectNumber = oseriesparms.get("projectNumber");
    String usage = oseriesparms.get("usage");
    String usageClass = oseriesparms.get("usageClass");
    String vendorSKU = oseriesparms.get("vendorSKU");
    String countryOfOriginISOCode = oseriesparms.get("countryOfOriginISOCode");
    String modeOfTransport = oseriesparms.get("modeOfTransport");
    String natureOfTransaction = oseriesparms.get("natureOfTransaction");
    String intrastatCommodityCode = oseriesparms.get("intrastatCommodityCode");
    String netMassKilograms = oseriesparms.get("netMassKilograms");
    String lineItemId = oseriesparms.get("lineItemId");
    String taxIncludedIndicator = oseriesparms.get("taxIncludedIndicator");
    String transactionType = oseriesparms.get("transactionType");
    String simplificationCode = oseriesparms.get("simplificationCode");
    String recoverableDate = oseriesparms.get("recoverableDate");
    String recoverableOverridePercent = oseriesparms.get("recoverableOverridePercent");
    String titleTransfer = oseriesparms.get("titleTransfer");
    String chainTransactionPhase = oseriesparms.get("chainTransactionPhase");

    // buyer
    String buyerIsTaxExempt = oseriesparms.get("Buyer-isTaxExempt");
    String buyerExemptionReasonCode = oseriesparms.get("Buyer-exemptionReasonCode");
    String buyerCompany = oseriesparms.get("Buyer-Company");
    String buyerDivision = oseriesparms.get("Buyer-Division");
    String buyerDepartment = oseriesparms.get("Buyer-Department");

    // buyer recipient
    String buyerRecipientCode = oseriesparms.get("Buyer-Recipient-RecipientCode");
    String buyerRecipientClassCode = oseriesparms.get("Buyer-Recipient-RecipientCode-classCode");
    String buyerRecipientIsBusinessIndicator = oseriesparms.get("Buyer-Recipient-RecipientCode-isBusinessIndicator");

    // buyer recipient tax registration
    String buyerRecipientTaxRegistrationIsoCountryCode = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-isoCountryCode");
    String buyerRecipientTaxRegistrationMainDivision = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-mainDivision");
    String buyerRecipientTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-hasPhysicalPresenceIndicator");


    String buyerRecipientTaxRegistrationNumber = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-TaxRegistrationNumber");
    String buyerRecipientNexusOverrideLocationRole = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-NexusOverride-locationRole");
    String buyerRecipientNexusOverrideCountry = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-NexusOverride-country");
    String buyerRecipientNexusOverrideMainDivision = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-NexusOverride-mainDivision");
    String buyerRecipientNexusOverrideSubDivision = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-NexusOverride-subDivision");
    String buyerRecipientNexusOverrideCity = oseriesparms.get("Buyer-Recipient-TaxRegistration-NexusOverride-city");
    String buyerRecipientNexusOverrideDistrict = oseriesparms
            .get("Buyer-Recipient-TaxRegistration-NexusOverride-district");

    // buyer recipient physical location
    String buyerRecipientPhysicalLocationTaxAreaId = oseriesparms.get("Buyer-Recipient-PhysicalLocation-taxAreaId");
    String buyerRecipientPhysicalLocationStreetAddress1 = oseriesparms
            .get("Buyer-Recipient-PhysicalLocation-StreetAddress1");
    String buyerRecipientPhysicalLocationStreetAddress2 = oseriesparms
            .get("Buyer-Recipient-PhysicalLocation-StreetAddress2");
    String buyerRecipientPhysicalLocationCity = oseriesparms.get("Buyer-Recipient-PhysicalLocation-City");
    String buyerRecipientPhysicalLocationMainDivision = oseriesparms
            .get("Buyer-Recipient-PhysicalLocation-MainDivision");
    String buyerRecipientPhysicalLocationSubDivision = oseriesparms
            .get("Buyer-Recipient-PhysicalLocation-SubDivision");
    String buyerRecipientPhysicalLocationPostalCode = oseriesparms
            .get("Buyer-Recipient-PhysicalLocation-PostalCode");
    String buyerRecipientPhysicalLocationCountry = oseriesparms.get("Buyer-Recipient-PhysicalLocation-Country");

    // buyer destination
    String buyerDestinationTaxAreaId = oseriesparms.get("Buyer-Destination-taxAreaId");
    String buyerDestinationLocationCustomsStatus = oseriesparms.get("Buyer-Destination-locationCustomsStatus");
    String buyerDestinationLocationCode = oseriesparms.get("Buyer-Destination-locationCode");
    String buyerDestinationExternalJurisdictionCode = oseriesparms
            .get("Buyer-Destination-externalJurisdictionCode");
    String buyerDestinationStreetAddress2 = oseriesparms.get("Buyer-Destination-StreetAddress1");
    String buyerDestinationStreetAddress1 = oseriesparms.get("Buyer-Destination-StreetAddress2");
    String buyerDestinationCity = oseriesparms.get("Buyer-Destination-City");
    String buyerDestinationMainDivision = oseriesparms.get("Buyer-Destination-MainDivision");
    String buyerDestinationSubDivision = oseriesparms.get("Buyer-Destination-SubDivision");
    String buyerDestinationPostalCode = oseriesparms.get("Buyer-Destination-PostalCode");
    String buyerDestinationCountry = oseriesparms.get("Buyer-Destination-Country");
    String buyerDestinationCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Buyer-Destination-CurrencyConversion-isoCurrencyCodeName");
    String buyerDestinationCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Buyer-Destination-CurrencyConversion-isoCurrencyCodeAlpha");
    String buyerDestinationCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Buyer-Destination-CurrencyConversion-isoCurrencyCodeNum");

    // buyer administrative destination
    String buyerAdministrativeDestinationTaxAreaId = oseriesparms.get("Buyer-AdministrativeDestination-taxAreaId");
    String buyerAdministrativeDestinationLocationCustomsStatus = oseriesparms
            .get("Buyer-AdministrativeDestination-locationCustomsStatus");
    String buyerAdministrativeDestinationLocationCode = oseriesparms
            .get("Buyer-AdministrativeDestination-locationCode");
    String buyerAdministrativeDestinationExternalJurisdictionCode = oseriesparms
            .get("Buyer-AdministrativeDestination-externalJurisdictionCode");
    String buyerAdministrativeDestinationStreetAddress2 = oseriesparms
            .get("Buyer-AdministrativeDestination-StreetAddress1");
    String buyerAdministrativeDestinationStreetAddress1 = oseriesparms
            .get("Buyer-AdministrativeDestination-StreetAddress2");
    String buyerAdministrativeDestinationCity = oseriesparms.get("Buyer-AdministrativeDestination-City");
    String buyerAdministrativeDestinationMainDivision = oseriesparms
            .get("Buyer-AdministrativeDestination-MainDivision");
    String buyerAdministrativeDestinationSubDivision = oseriesparms
            .get("Buyer-AdministrativeDestination-SubDivision");
    String buyerAdministrativeDestinationPostalCode = oseriesparms
            .get("Buyer-AdministrativeDestination-PostalCode");
    String buyerAdministrativeDestinationCountry = oseriesparms.get("Buyer-AdministrativeDestination-Country");
    String buyerAdministrativeDestinationCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Buyer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeName");
    String buyerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Buyer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeAlpha");
    String buyerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Buyer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeNum");

    // buyer exemption certificate
    String buyerExemptionCertificateNumber = oseriesparms
            .get("Buyer-ExemptionCertificate-exemptionCertificateNumber");

    // buyer tax registration
    String buyerTaxRegistrationIsoCountryCode = oseriesparms.get("Buyer-TaxRegistration-isoCountryCode");
    String buyerTaxRegistrationMainDivision = oseriesparms.get("Buyer-TaxRegistration-mainDivision");
    String buyerTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms
            .get("Buyer-TaxRegistration-hasPhysicalPresenceIndicator");
    String buyerTaxRegistrationTaxRegistrationNumber = oseriesparms
            .get("Buyer-TaxRegistration-TaxRegistrationNumber");

    String buyerTaxRegistrationNexusOverrideLocationRole = oseriesparms
            .get("Buyer-TaxRegistration-NexusOverride-locationRole");
    String buyerTaxRegistrationNexusOverrideCountry = oseriesparms
            .get("Buyer-TaxRegistration-NexusOverride-country");
    String buyerTaxRegistrationNexusOverrideMainDivision = oseriesparms
            .get("Buyer-TaxRegistration-NexusOverride-mainDivision");
    String buyerTaxRegistrationNexusOverrideSubDivision = oseriesparms
            .get("Buyer-TaxRegistration-NexusOverride-subDivision");
    String buyerTaxRegistrationNexusOverrideCity = oseriesparms.get("Buyer-TaxRegistration-NexusOverride-city");
    String buyerTaxRegistrationNexusOverrideDistrict = oseriesparms
            .get("Buyer-TaxRegistration-NexusOverride-district");

    String buyerTaxRegistrationTaxAreaId = oseriesparms.get("Buyer-TaxRegistration-PhysicalLocation-taxAreaId");
    String buyerTaxRegistrationPhysicalLocationStreetAddress1 = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-StreetAddress1");
    String buyerTaxRegistrationPhysicalLocationStreetAddress2 = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-StreetAddress2");
    String buyerTaxRegistrationPhysicalLocationCity = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-City");
    String buyerTaxRegistrationPhysicalLocationMainDivision = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-MainDivision");
    String buyerTaxRegistrationPhysicalLocationSubDivision = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-SubDivision");
    String buyerTaxRegistrationPhysicalLocationPostalCode = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-PostalCode");
    String buyerTaxRegistrationPhysicalLocationCountry = oseriesparms
            .get("Buyer-TaxRegistration-PhysicalLocation-Country");

    // vendor
    String vendorVendorCode = oseriesparms.get("Vendor-VendorCode");
    String vendorVendorCodeClassCode = oseriesparms.get("Vendor-VendorCode-classCode");

    // vendor physical origin
    String vendorPhysicalOriginTaxAreaId = oseriesparms.get("Vendor-PhysicalOrigin-taxAreaId");
    String vendorPhysicalOriginLocationCustomsStatus = oseriesparms
            .get("Vendor-PhysicalOrigin-locationCustomsStatus");
    String vendorPhysicalOriginLocationCode = oseriesparms.get("Vendor-PhysicalOrigin-locationCode");
    String vendorPhysicalOriginExternalJurisdictionCode = oseriesparms
            .get("Vendor-PhysicalOrigin-externalJurisdictionCode");
    String vendorPhysicalOriginStreetAddress1 = oseriesparms.get("Vendor-PhysicalOrigin-StreetAddress1");
    String vendorPhysicalOriginStreetAddress2 = oseriesparms.get("Vendor-PhysicalOrigin-StreetAddress2");
    String vendorPhysicalOriginCity = oseriesparms.get("Vendor-PhysicalOrigin-City");
    String vendorPhysicalOriginMainDivision = oseriesparms.get("Vendor-PhysicalOrigin-MainDivision");
    String vendorPhysicalOriginSubDivision = oseriesparms.get("Vendor-PhysicalOrigin-SubDivision");
    String vendorPhysicalOriginPostalCode = oseriesparms.get("Vendor-PhysicalOrigin-PostalCode");
    String vendorPhysicalOriginCountry = oseriesparms.get("Vendor-PhysicalOrigin-Country");
    String vendorPhysicalOriginCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Vendor-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeName");
    String vendorPhysicalOriginCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Vendor-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeAlpha");
    String vendorPhysicalOriginCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Vendor-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeNum");

    // vendor administrative origin
    String vendorAdministrativeOriginTaxAreaId = oseriesparms.get("Vendor-AdministrativeOrigin-taxAreaId");
    String vendorAdministrativeOriginLocationCustomsStatus = oseriesparms
            .get("Vendor-AdministrativeOrigin-locationCustomsStatus");
    String vendorAdministrativeOriginLocationCode = oseriesparms.get("Vendor-AdministrativeOrigin-locationCode");
    String vendorAdministrativeOriginExternalJurisdictionCode = oseriesparms
            .get("Vendor-AdministrativeOrigin-externalJurisdictionCode");
    String vendorAdministrativeOriginStreetAddress1 = oseriesparms
            .get("Vendor-AdministrativeOrigin-StreetAddress1");
    String vendorAdministrativeOriginStreetAddress2 = oseriesparms
            .get("Vendor-AdministrativeOrigin-StreetAddress2");
    String vendorAdministrativeOriginCity = oseriesparms.get("Vendor-AdministrativeOrigin-City");
    String vendorAdministrativeOriginMainDivision = oseriesparms.get("Vendor-AdministrativeOrigin-MainDivision");
    String vendorAdministrativeOriginSubDivision = oseriesparms.get("Vendor-AdministrativeOrigin-SubDivision");
    String vendorAdministrativeOriginPostalCode = oseriesparms.get("Vendor-AdministrativeOrigin-PostalCode");
    String vendorAdministrativeOriginCountry = oseriesparms.get("Vendor-AdministrativeOrigin-Country");
    String vendorAdministrativeOriginCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Vendor-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeName");
    String vendorAdministrativeOriginCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Vendor-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeAlpha");
    String vendorAdministrativeOriginCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Vendor-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeNum");

    // vendor tax registration
    String vendorTaxRegistrationIsoCountryCode = oseriesparms.get("Vendor-TaxRegistration-isoCountryCode");
    String vendorTaxRegistrationMainDivision = oseriesparms.get("Vendor-TaxRegistration-mainDivision");
    String vendorTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms
            .get("Vendor-TaxRegistration-hasPhysicalPresenceIndicator");
    String vendorTaxRegistrationTaxRegistrationNumber = oseriesparms
            .get("Vendor-TaxRegistration-TaxRegistrationNumber");

    String vendorTaxRegistrationNexusOverrideLocationRole = oseriesparms
            .get("Vendor-TaxRegistration-NexusOverride-locationRole");
    String vendorTaxRegistrationNexusOverrideCountry = oseriesparms
            .get("Vendor-TaxRegistration-NexusOverride-country");
    String vendorTaxRegistrationNexusOverrideMainDivision = oseriesparms
            .get("Vendor-TaxRegistration-NexusOverride-mainDivision");
    String vendorTaxRegistrationNexusOverrideSubDivision = oseriesparms
            .get("Vendor-TaxRegistration-NexusOverride-subDivision");
    String vendorTaxRegistrationNexusOverrideCity = oseriesparms.get("Vendor-TaxRegistration-NexusOverride-city");
    String vendorTaxRegistrationNexusOverrideDistrict = oseriesparms
            .get("Vendor-TaxRegistration-NexusOverride-district");

    String vendorTaxRegistrationTaxAreaId = oseriesparms.get("Vendor-TaxRegistration-PhysicalLocation-taxAreaId");
    String vendorTaxRegistrationPhysicalLocationStreetAddress1 = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-StreetAddress1");
    String vendorTaxRegistrationPhysicalLocationStreetAddress2 = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-StreetAddress2");
    String vendorTaxRegistrationPhysicalLocationCity = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-City");
    String vendorTaxRegistrationPhysicalLocationMainDivision = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-MainDivision");
    String vendorTaxRegistrationPhysicalLocationSubDivision = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-SubDivision");
    String vendorTaxRegistrationPhysicalLocationPostalCode = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-PostalCode");
    String vendorTaxRegistrationPhysicalLocationCountry = oseriesparms
            .get("Vendor-TaxRegistration-PhysicalLocation-Country");

    // tax override
    String taxOverrideOverrideType = oseriesparms.get("TaxOverride-overrideType");
    String taxOverrideOverrideReasonCode = oseriesparms.get("TaxOverride-overrideReasonCode");

    // imposition to process
    String impositionToProcessImpositionType = oseriesparms.get("ImpositionToProcess-impositionType");

    // jurisdiction override
    String jurisdictionOverrideJurisdictionLevel = oseriesparms.get("JurisdictionOverride-jurisdictionLevel");
    String jurisdictionOverrideImpositionType = oseriesparms.get("JurisdictionOverride-impositionType");

    // jurisdiction override - deduction override - exempt override
    String jurisdictionOverrideDeductionOverrideExemptOverride = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyName = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyName");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeAlpha = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyCodeAlpha");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeNum = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyCodeNum");
    String jurisdictionOverrideDeductionOverrideExemptOverrideOverrideExemptReasonCode = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-overrideExemptReasonCode");

    // jurisdiction override - deduction override - non taxable override
    String jurisdictionOverrideDeductionOverrideNonTaxableOverride = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyName = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyName");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeAlpha = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyCodeAlpha");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeNum = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyCodeNum");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideOverrideExemptReasonCode = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-overrideNonTaxableReasonCode");

    // jurisdiction override - rate override
    String jurisdictionOverrideRateOverride = oseriesparms.get("JurisdictionOverride-RateOverride");

    // situs override
    String situsOverride = oseriesparms.get("SitusOverride");
    String situsOverrideTaxingLocation = oseriesparms.get("SitusOverride-taxingLocation");

    // purchase
    String purchase = oseriesparms.get("Purchase");
    String purchasePurchaseClass = oseriesparms.get("Purchase-purchaseClass");

    // commodity code
    String commodityCode = oseriesparms.get("CommodityCode");
    String commodityCodeCommodityCodeType = oseriesparms.get("CommodityCode-commodityCodeType");

    // quantity
    String quantity = oseriesparms.get("Quantity");
    String quantityUnitOfMeasure = oseriesparms.get("Quantity-unitOfMeasure");

    // weight
    String weight = oseriesparms.get("Weight");
    String weightUnitOfMeasure = oseriesparms.get("Weight-unitOfMeasure");

    // volume
    String volume = oseriesparms.get("Volume");
    String volumeUnitOfMeasure = oseriesparms.get("Volume-unitOfMeasure");

    // freight
    String freight = oseriesparms.get("Freight");
    String freightIsoCurrencyName = oseriesparms.get("Freight-isoCurrencyName");
    String freightIsoCurrencyCodeAlpha = oseriesparms.get("Freight-isoCurrencyCodeAlpha");
    String freightIsoCurrencyCodeNum = oseriesparms.get("Freight-isoCurrencyCodeNum");

    // unit price
    String unitPrice = oseriesparms.get("UnitPrice");
    String unitPriceIsoCurrencyName = oseriesparms.get("UnitPrice-isoCurrencyName");
    String unitPriceIsoCurrencyCodeAlpha = oseriesparms.get("UnitPrice-isoCurrencyCodeAlpha");
    String unitPriceIsoCurrencyCodeNum = oseriesparms.get("UnitPrice-isoCurrencyCodeNum");

    // extended price
    String extendedPrice = oseriesparms.get("ExtendedPrice");
    String extendedPriceIsoCurrencyName = oseriesparms.get("ExtendedPrice-isoCurrencyName");
    String extendedPriceIsoCurrencyCodeAlpha = oseriesparms.get("ExtendedPrice-isoCurrencyCodeAlpha");
    String extendedPriceIsoCurrencyCodeNum = oseriesparms.get("ExtendedPrice-isoCurrencyCodeNum");

    // charged tax
    String chargedTax = oseriesparms.get("ChargedTax");
    String chargedTaxIsoCurrencyName = oseriesparms.get("ChargedTax-isoCurrencyName");
    String chargedTaxIsoCurrencyCodeAlpha = oseriesparms.get("ChargedTax-isoCurrencyCodeAlpha");
    String chargedTaxIsoCurrencyCodeNum = oseriesparms.get("ChargedTax-isoCurrencyCodeNum");

    // landed cost
    String landedCost = oseriesparms.get("LandedCost");
    String landedCostIsoCurrencyName = oseriesparms.get("LandedCost-isoCurrencyName");
    String landedCostIsoCurrencyCodeAlpha = oseriesparms.get("LandedCost-isoCurrencyCodeAlpha");
    String landedCostIsoCurrencyCodeNum = oseriesparms.get("LandedCost-isoCurrencyCodeNum");

    // amount billed to date
    String amountBilledToDate = oseriesparms.get("AmountBilledToDate");
    String amountBilledToDateIsoCurrencyName = oseriesparms.get("AmountBilledToDate-isoCurrencyName");
    String amountBilledToDateIsoCurrencyCodeAlpha = oseriesparms.get("AmountBilledToDate-isoCurrencyCodeAlpha");
    String amountBilledToDateIsoCurrencyCodeNum = oseriesparms.get("AmountBilledToDate-isoCurrencyCodeNum");

    // flexible fields - flexible code
    String flexibleFieldsFlexibleCode1 = oseriesparms.get("FlexibleFields-FlexibleCodeField1");
    String flexibleFieldsFlexibleCode2 = oseriesparms.get("FlexibleFields-FlexibleCodeField2");
    String flexibleFieldsFlexibleCode3 = oseriesparms.get("FlexibleFields-FlexibleCodeField3");
    String flexibleFieldsFlexibleCode4 = oseriesparms.get("FlexibleFields-FlexibleCodeField4");
    String flexibleFieldsFlexibleCode5 = oseriesparms.get("FlexibleFields-FlexibleCodeField5");
    String flexibleFieldsFlexibleCode6 = oseriesparms.get("FlexibleFields-FlexibleCodeField6");
    String flexibleFieldsFlexibleCode7 = oseriesparms.get("FlexibleFields-FlexibleCodeField7");
    String flexibleFieldsFlexibleCode8 = oseriesparms.get("FlexibleFields-FlexibleCodeField8");
    String flexibleFieldsFlexibleCode9 = oseriesparms.get("FlexibleFields-FlexibleCodeField9");
    String flexibleFieldsFlexibleCode10 = oseriesparms.get("FlexibleFields-FlexibleCodeField10");
    String flexibleFieldsFlexibleCode11 = oseriesparms.get("FlexibleFields-FlexibleCodeField11");
    String flexibleFieldsFlexibleCode12 = oseriesparms.get("FlexibleFields-FlexibleCodeField12");
    String flexibleFieldsFlexibleCode13 = oseriesparms.get("FlexibleFields-FlexibleCodeField13");
    String flexibleFieldsFlexibleCode14 = oseriesparms.get("FlexibleFields-FlexibleCodeField14");
    String flexibleFieldsFlexibleCode15 = oseriesparms.get("FlexibleFields-FlexibleCodeField15");
    String flexibleFieldsFlexibleCode16 = oseriesparms.get("FlexibleFields-FlexibleCodeField16");
    String flexibleFieldsFlexibleCode17 = oseriesparms.get("FlexibleFields-FlexibleCodeField17");
    String flexibleFieldsFlexibleCode18 = oseriesparms.get("FlexibleFields-FlexibleCodeField18");
    String flexibleFieldsFlexibleCode19 = oseriesparms.get("FlexibleFields-FlexibleCodeField19");
    String flexibleFieldsFlexibleCode20 = oseriesparms.get("FlexibleFields-FlexibleCodeField20");
    String flexibleFieldsFlexibleCode21 = oseriesparms.get("FlexibleFields-FlexibleCodeField21");
    String flexibleFieldsFlexibleCode22 = oseriesparms.get("FlexibleFields-FlexibleCodeField22");
    String flexibleFieldsFlexibleCode23 = oseriesparms.get("FlexibleFields-FlexibleCodeField23");
    String flexibleFieldsFlexibleCode24 = oseriesparms.get("FlexibleFields-FlexibleCodeField24");
    String flexibleFieldsFlexibleCode25 = oseriesparms.get("FlexibleFields-FlexibleCodeField25");

    // flexible fields - flexible numeric
    String flexibleFieldsFlexibleNumeric1 = oseriesparms.get("FlexibleFields-FlexibleNumericField1");
    String flexibleFieldsFlexibleNumeric2 = oseriesparms.get("FlexibleFields-FlexibleNumericField2");
    String flexibleFieldsFlexibleNumeric3 = oseriesparms.get("FlexibleFields-FlexibleNumericField3");
    String flexibleFieldsFlexibleNumeric4 = oseriesparms.get("FlexibleFields-FlexibleNumericField4");
    String flexibleFieldsFlexibleNumeric5 = oseriesparms.get("FlexibleFields-FlexibleNumericField5");

    // flexible fields - flexible date
    String flexibleFieldsFlexibleDate1 = oseriesparms.get("FlexibleFields-FlexibleDateField1");
    String flexibleFieldsFlexibleDate2 = oseriesparms.get("FlexibleFields-FlexibleDateField2");
    String flexibleFieldsFlexibleDate3 = oseriesparms.get("FlexibleFields-FlexibleDateField3");
    String flexibleFieldsFlexibleDate4 = oseriesparms.get("FlexibleFields-FlexibleDateField4");
    String flexibleFieldsFlexibleDate5 = oseriesparms.get("FlexibleFields-FlexibleDateField5");

    // input total tax
    String inputTotalTax = oseriesparms.get("InputTotalTax");
    String inputTotalTaxIsoCurrencyName = oseriesparms.get("InputTotalTax-isoCurrencyName");
    String inputTotalTaxIsoCurrencyCodeAlpha = oseriesparms.get("InputTotalTax-isoCurrencyCodeAlpha");
    String inputTotalTaxIsoCurrencyCodeNum = oseriesparms.get("InputTotalTax-isoCurrencyCodeNum");

    // reportedSelfAccrualRecoverableAmount
    String reportedSelfAccrualRecoverableAmount = oseriesparms.get("ReportedSelfAccrualRecoverableAmount");
    String reportedSelfAccrualRecoverableAmountIsoCurrencyName = oseriesparms.get("ReportedSelfAccrualRecoverableAmount-isoCurrencyName");
    String reportedSelfAccrualRecoverableAmountIsoCurrencyCodeAlpha = oseriesparms.get("ReportedSelfAccrualRecoverableAmount-isoCurrencyCodeAlpha");
    String reportedSelfAccrualRecoverableAmountIsoCurrencyCodeNum = oseriesparms.get("ReportedSelfAccrualRecoverableAmount-isoCurrencyCodeNum");

    // set defaults
    if (isNullOrEmpty(transactionType)) {
      transactionType = "PURCHASE";
    }

    Element financialEventElement = (Element) outDoc.getElementsByTagName(financialEventName).item(0);
    // transaction type
    addAttributeToElement(financialEventElement, "transactionType", transactionType);
    addAttributeToElement(financialEventElement, "documentNumber", documentNumber);
    addAttributeToElement(financialEventElement, "returnAssistedParametersIndicator",
            returnAssistedParametersIndicator);
    addAttributeToElement(financialEventElement, "returnGeneratedLineItemsIndicator",
            returnGeneratedLineItemsIndicator);
    addAttributeToElement(financialEventElement, "documentDate", documentDate);
    addAttributeToElement(financialEventElement, "transactionId", transactionId);
    addAttributeToElement(financialEventElement, "calculateVendorTaxIndicator", calculateVendorTaxIndicator);

    // Currency
    if (!isNullOrEmpty(currencyIsoCurrencyName) || !isNullOrEmpty(currencyIsoCurrencyCodeAlpha)
            || !isNullOrEmpty(currencyIsoCurrencyCodeNum)) {

      Element currencyCodeElement = createStubElement(financialEventElement, "Currency");
      addAttributeToElement(currencyCodeElement, "isoCurrencyName", currencyIsoCurrencyName);
      addAttributeToElement(currencyCodeElement, "isoCurrencyCodeAlpha", currencyIsoCurrencyCodeAlpha);
      addAttributeToElement(currencyCodeElement, "isoCurrencyCodeNum", currencyIsoCurrencyCodeNum);
    }
    // Original Currency
    if (!isNullOrEmpty(originalCurrencyIsoCurrencyName)
            || !isNullOrEmpty(originalCurrencyIsoCurrencyCodeAlpha)
            || !isNullOrEmpty(originalCurrencyIsoCurrencyCodeNum)) {

      Element originalCurrencyCodeElement = createStubElement(financialEventElement, "OriginalCurrency");
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyName", originalCurrencyIsoCurrencyName);
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyCodeAlpha",
              originalCurrencyIsoCurrencyCodeAlpha);
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyCodeNum",
              originalCurrencyIsoCurrencyCodeNum);
    }
    // Line Item Element
    Element lineItemElement = createStubElement(financialEventElement, "LineItem");

    // Line Item Delivery Term
    addAttributeToElement(lineItemElement, "lineItemNumber", lineItemNumber);
    addAttributeToElement(lineItemElement, "taxDate", taxDate);
    addAttributeToElement(lineItemElement, "isMulticomponent", isMulticomponent);
    addAttributeToElement(lineItemElement, "deliveryTerm", deliveryTerm);
    addAttributeToElement(lineItemElement, "locationCode", locationCode);
    addAttributeToElement(lineItemElement, "postingDate", postingDate);
    addAttributeToElement(lineItemElement, "costCenter", costCenter);
    addAttributeToElement(lineItemElement, "departmentCode", departmentCode);
    addAttributeToElement(lineItemElement, "generalLedgerAccount", generalLedgerAccount);
    addAttributeToElement(lineItemElement, "materialCode", materialCode);
    addAttributeToElement(lineItemElement, "projectNumber", projectNumber);
    addAttributeToElement(lineItemElement, "usage", usage);
    addAttributeToElement(lineItemElement, "usageClass", usageClass);
    addAttributeToElement(lineItemElement, "vendorSKU", vendorSKU);
    addAttributeToElement(lineItemElement, "countryOfOriginISOCode", countryOfOriginISOCode);
    addAttributeToElement(lineItemElement, "modeOfTransport", modeOfTransport);
    addAttributeToElement(lineItemElement, "natureOfTransaction", natureOfTransaction);
    addAttributeToElement(lineItemElement, "intrastatCommodityCode", intrastatCommodityCode);
    addAttributeToElement(lineItemElement, "netMassKilograms", netMassKilograms);
    addAttributeToElement(lineItemElement, "lineItemId", lineItemId);
    addAttributeToElement(lineItemElement, "taxIncludedIndicator", taxIncludedIndicator);
    addAttributeToElement(lineItemElement, "transactionType", transactionType);
    addAttributeToElement(lineItemElement, "simplificationCode", simplificationCode);
    addAttributeToElement(lineItemElement, "recoverableDate", recoverableDate);
    addAttributeToElement(lineItemElement, "recoverableOverridePercent", recoverableOverridePercent);
    addAttributeToElement(lineItemElement, "titleTransfer", titleTransfer);
    addAttributeToElement(lineItemElement, "chainTransactionPhase", chainTransactionPhase);

    // Line Item Buyer
    Element lineItemBuyerElement = createStubElement(lineItemElement, "Buyer");

    // Line Item Buyer Company/Division/Department
    Element lineItemBuyerCompanyElement = addElementToDoc(lineItemBuyerElement, "Company", buyerCompany);
    Element lineItemBuyerDivisionElement = addElementToDoc(lineItemBuyerElement, "Division", buyerDivision);
    Element lineItemBuyerDepartmentElement = addElementToDoc(lineItemBuyerElement, "Department", buyerDepartment);
    addAttributeToElement(lineItemBuyerElement, "isTaxExempt", buyerIsTaxExempt);
    addAttributeToElement(lineItemBuyerElement, "exemptionReasonCode", buyerExemptionReasonCode);

    // Line Item Buyer Recipient
    Element lineItemBuyerRecipient = createStubElement(lineItemBuyerElement, "Recipient");
    addAttributeToElement(lineItemBuyerRecipient, "classCode", buyerRecipientClassCode);
    addAttributeToElement(lineItemBuyerRecipient, "isBusinessIndicator", buyerRecipientIsBusinessIndicator);
    Element lineItemBuyerRecipientRecipientCode = addElementToDoc(lineItemBuyerRecipient, "RecipientCode",
            buyerRecipientCode);


    // buyer recipient tax registration
    Element lineItemBuyerRecipientTaxRegistration = createStubElement(lineItemBuyerRecipient, "TaxRegistration");
    addAttributeToElement(lineItemBuyerRecipientTaxRegistration, "isoCountryCode", buyerRecipientTaxRegistrationIsoCountryCode);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistration, "mainDivision", buyerRecipientTaxRegistrationMainDivision);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistration, "hasPhysicalPresenceIndicator", buyerRecipientTaxRegistrationHasPhysicalPresenceIndicator);

    Element lineItemBuyerRecipientTaxRegistrationTaxRegistrationNumber = addElementToDoc(lineItemBuyerRecipientTaxRegistration, "TaxRegistrationNumber", buyerRecipientTaxRegistrationNumber);
    Element lineItemBuyerRecipientTaxRegistrationNexusOverride = createStubElement(
            lineItemBuyerRecipientTaxRegistration, "NexusOverride");
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "locationRole",
            buyerRecipientNexusOverrideLocationRole);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "country",
            buyerRecipientNexusOverrideCountry);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "mainDivision",
            buyerRecipientNexusOverrideMainDivision);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "subDivision",
            buyerRecipientNexusOverrideSubDivision);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "city",
            buyerRecipientNexusOverrideCity);
    addAttributeToElement(lineItemBuyerRecipientTaxRegistrationNexusOverride, "district",
            buyerRecipientNexusOverrideDistrict);

    Element lineItemBuyerRecipientPhysicalLocation = createStubElement(lineItemBuyerRecipient, "PhysicalLocation");
    addAttributeToElement(lineItemBuyerRecipientPhysicalLocation, "taxAreaId",
            buyerRecipientPhysicalLocationTaxAreaId);

    Element lineItemBuyerRecipientPhysicalLocationStreetAddress1 = addElementToDoc(
            lineItemBuyerRecipientPhysicalLocation, "StreetAddress1", buyerRecipientPhysicalLocationStreetAddress1);
    Element lineItemBuyerRecipientPhysicalLocationStreetAddress2 = addElementToDoc(
            lineItemBuyerRecipientPhysicalLocation, "StreetAddress2", buyerRecipientPhysicalLocationStreetAddress2);
    Element lineItemBuyerRecipientPhysicalLocationCity = addElementToDoc(lineItemBuyerRecipientPhysicalLocation,
            "City", buyerRecipientPhysicalLocationCity);
    Element lineItemBuyerRecipientPhysicalLocationMainDivision = addElementToDoc(
            lineItemBuyerRecipientPhysicalLocation, "MainDivision", buyerRecipientPhysicalLocationMainDivision);
    Element lineItemBuyerRecipientPhysicalLocationSubDivision = addElementToDoc(
            lineItemBuyerRecipientPhysicalLocation, "SubDivision", buyerRecipientPhysicalLocationSubDivision);
    Element lineItemBuyerRecipientPhysicalLocationPostalCode = addElementToDoc(
            lineItemBuyerRecipientPhysicalLocation, "PostalCode", buyerRecipientPhysicalLocationPostalCode);
    Element lineItemBuyerRecipientPhysicalLocationCountry = addElementToDoc(lineItemBuyerRecipientPhysicalLocation,
            "Country", buyerRecipientPhysicalLocationCountry);

    // Line Item Buyer Destination - Ship To
    Element lineItemBuyerDestinationElement = createStubElement(lineItemBuyerElement, "Destination");

    addAttributeToElement(lineItemBuyerDestinationElement, "taxAreaId", buyerDestinationTaxAreaId);
    addAttributeToElement(lineItemBuyerDestinationElement, "locationCustomsStatus",
            buyerDestinationLocationCustomsStatus);
    addAttributeToElement(lineItemBuyerDestinationElement, "locationCode", buyerDestinationLocationCode);
    addAttributeToElement(lineItemBuyerDestinationElement, "externalJurisdictionCode",
            buyerDestinationExternalJurisdictionCode);

    Element lineItemDestinationStreetAddress1Element = addElementToDoc(lineItemBuyerDestinationElement,
            "StreetAddress1", buyerDestinationStreetAddress1);
    Element lineItemDestinationStreetAddress2Element = addElementToDoc(lineItemBuyerDestinationElement,
            "StreetAddress2", buyerDestinationStreetAddress2);
    Element lineItemDestinationStreetCityElement = addElementToDoc(lineItemBuyerDestinationElement, "City",
            buyerDestinationCity);
    Element lineItemDestinationMainDivisionElement = addElementToDoc(lineItemBuyerDestinationElement,
            "MainDivision", buyerDestinationMainDivision);
    Element lineItemDestinationSubDivisionElement = addElementToDoc(lineItemBuyerDestinationElement, "SubDivision",
            buyerDestinationSubDivision);
    Element lineItemDestinationPostalElement = addElementToDoc(lineItemBuyerDestinationElement, "PostalCode",
            buyerDestinationPostalCode);
    Element lineItemDestinationCountryElement = addElementToDoc(lineItemBuyerDestinationElement, "Country",
            buyerDestinationCountry);

    Element lineItemBuyerDestinationCurrencyConversion = createStubElement(lineItemBuyerDestinationElement,
            "CurrencyConversion");
    addAttributeToElement(lineItemBuyerDestinationCurrencyConversion, "isoCurrencyName",
            buyerDestinationCurrencyConversionIsoCurrencyName);
    addAttributeToElement(lineItemBuyerDestinationCurrencyConversion, "isoCurrencyCodeAlpha",
            buyerDestinationCurrencyConversionIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemBuyerDestinationCurrencyConversion, "isoCurrencyCodeNum",
            buyerDestinationCurrencyConversionIsoCurrencyCodeNum);

    // Line Item Buyer Administrative Destination - Bill To
    if (!isNullOrEmpty(buyerAdministrativeDestinationMainDivision)
            || !isNullOrEmpty(buyerAdministrativeDestinationPostalCode)
            || !isNullOrEmpty(buyerAdministrativeDestinationCountry)
            || !isNullOrEmpty(buyerAdministrativeDestinationTaxAreaId)) {

      Element lineItemBuyerAdministrativeDestinationElement = createStubElement(lineItemBuyerElement,
              "AdministrativeDestination");

      addAttributeToElement(lineItemBuyerAdministrativeDestinationElement, "taxAreaId",
              buyerAdministrativeDestinationTaxAreaId);
      addAttributeToElement(lineItemBuyerAdministrativeDestinationElement, "locationCustomsStatus",
              buyerAdministrativeDestinationLocationCustomsStatus);
      addAttributeToElement(lineItemBuyerAdministrativeDestinationElement, "locationCode",
              buyerAdministrativeDestinationLocationCode);
      addAttributeToElement(lineItemBuyerAdministrativeDestinationElement, "externalJurisdictionCode",
              buyerAdministrativeDestinationExternalJurisdictionCode);

      Element lineItemBuyerAdministrativeDestinationStreetAddress1Element = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "StreetAddress1",
              buyerAdministrativeDestinationStreetAddress1);
      Element lineItemBuyerAdministrativeDestinationStreetAddress2Element = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "StreetAddress2",
              buyerAdministrativeDestinationStreetAddress2);
      Element lineItemBuyerAdministrativeDestinationStreetCityElement = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "City", buyerAdministrativeDestinationCity);
      Element lineItemBuyerAdministrativeDestinationMainDivisionElement = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "MainDivision",
              buyerAdministrativeDestinationMainDivision);
      Element lineItemBuyerAdministrativeDestinationSubDivisionElement = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "SubDivision",
              buyerAdministrativeDestinationSubDivision);
      Element lineItemBuyerAdministrativeDestinationPostalElement = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "PostalCode",
              buyerAdministrativeDestinationPostalCode);
      Element lineItemBuyerAdministrativeDestinationCountryElement = addElementToDoc(
              lineItemBuyerAdministrativeDestinationElement, "Country", buyerAdministrativeDestinationCountry);

      Element lineItemBuyerAdministrativeDestinationCurrencyConversion = createStubElement(
              lineItemBuyerAdministrativeDestinationElement, "CurrencyConversion");
      addAttributeToElement(lineItemBuyerAdministrativeDestinationCurrencyConversion, "isoCurrencyName",
              buyerAdministrativeDestinationCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemBuyerAdministrativeDestinationCurrencyConversion, "isoCurrencyCodeAlpha",
              buyerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemBuyerAdministrativeDestinationCurrencyConversion, "isoCurrencyCodeNum",
              buyerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeNum);
    }

    // Line Item Vendor
    Element lineItemVendorElement = createStubElement(lineItemElement, "Vendor");

    // Line Item Vendor Code
    Element lineItemVendorCodeElement = addElementToDoc(lineItemVendorElement, "VendorCode", vendorVendorCode);
    addAttributeToElement(lineItemVendorCodeElement, "classCode", vendorVendorCodeClassCode);

    // Line Item Vendor Tax Registration Number
    if (!isNullOrEmpty(vendorTaxRegistrationTaxRegistrationNumber)
            && buyerDestinationCountry.length() <= 3) {
      Element lineItemVendorTaxRegistrationElement = createStubElement(lineItemVendorElement, "TaxRegistration");
      if (buyerDestinationCountry.length() == 3) {
        addAttributeToElement(lineItemVendorTaxRegistrationElement, "isoCountryCode", buyerDestinationCountry);
      }
      Element lineItemVendorTaxRegistrationNumberElement = addElementToDoc(lineItemVendorTaxRegistrationElement,
              "TaxRegistrationNumber", vendorTaxRegistrationTaxRegistrationNumber);
    }

    // Line Item Vendor Physical Origin - Ship From
    if (!isNullOrEmpty(vendorPhysicalOriginMainDivision)
            || !isNullOrEmpty(vendorPhysicalOriginPostalCode)
            || !isNullOrEmpty(vendorPhysicalOriginCountry)
            || !isNullOrEmpty(vendorPhysicalOriginTaxAreaId)) {

      Element lineItemVendorPhysicalOriginElement = createStubElement(lineItemVendorElement, "PhysicalOrigin");

      addAttributeToElement(lineItemVendorPhysicalOriginElement, "taxAreaId", vendorPhysicalOriginTaxAreaId);
      addAttributeToElement(lineItemVendorPhysicalOriginElement, "locationCustomsStatus",
              vendorPhysicalOriginLocationCustomsStatus);
      addAttributeToElement(lineItemVendorPhysicalOriginElement, "locationCode",
              vendorPhysicalOriginLocationCode);
      addAttributeToElement(lineItemVendorPhysicalOriginElement, "externalJurisdictionCode",
              vendorPhysicalOriginExternalJurisdictionCode);

      Element lineItemVendorPhysicalOriginStreetAddress1Element = addElementToDoc(
              lineItemVendorPhysicalOriginElement, "StreetAddress1", vendorPhysicalOriginStreetAddress1);
      Element lineItemVendorPhysicalOriginStreetAddress2Element = addElementToDoc(
              lineItemVendorPhysicalOriginElement, "StreetAddress2", vendorPhysicalOriginStreetAddress2);
      Element lineItemVendorPhysicalOriginStreetCityElement = addElementToDoc(lineItemVendorPhysicalOriginElement,
              "City", vendorPhysicalOriginCity);
      Element lineItemVendorPhysicalOriginMainDivisionElement = addElementToDoc(
              lineItemVendorPhysicalOriginElement, "MainDivision", vendorPhysicalOriginMainDivision);
      Element lineItemVendorPhysicalOriginSubDivisionElement = addElementToDoc(
              lineItemVendorPhysicalOriginElement, "SubDivision", vendorPhysicalOriginSubDivision);
      Element lineItemVendorPhysicalOriginPostalElement = addElementToDoc(lineItemVendorPhysicalOriginElement,
              "PostalCode", vendorPhysicalOriginPostalCode);
      Element lineItemVendorPhysicalOriginCountryElement = addElementToDoc(lineItemVendorPhysicalOriginElement,
              "Country", vendorPhysicalOriginCountry);

      Element lineItemVendorPhysicalOriginCurrencyConversion = createStubElement(
              lineItemVendorPhysicalOriginElement, "CurrencyConversion");
      addAttributeToElement(lineItemVendorPhysicalOriginCurrencyConversion, "isoCurrencyName",
              vendorPhysicalOriginCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemVendorPhysicalOriginCurrencyConversion, "isoCurrencyCodeAlpha",
              vendorPhysicalOriginCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemVendorPhysicalOriginCurrencyConversion, "isoCurrencyCodeNum",
              vendorPhysicalOriginCurrencyConversionIsoCurrencyCodeNum);
    }

    // Line Item Vendor Administrative Origin - Bill From
    if (!isNullOrEmpty(vendorAdministrativeOriginMainDivision)
            || !isNullOrEmpty(vendorAdministrativeOriginPostalCode)
            || !isNullOrEmpty(vendorAdministrativeOriginCountry)
            || !isNullOrEmpty(vendorAdministrativeOriginTaxAreaId)) {

      Element lineItemVendorAdministrativeOriginElement = createStubElement(lineItemVendorElement,
              "AdministrativeOrigin");

      addAttributeToElement(lineItemVendorAdministrativeOriginElement, "taxAreaId",
              vendorAdministrativeOriginTaxAreaId);
      addAttributeToElement(lineItemVendorAdministrativeOriginElement, "locationCustomsStatus",
              vendorAdministrativeOriginLocationCustomsStatus);
      addAttributeToElement(lineItemVendorAdministrativeOriginElement, "locationCode",
              vendorAdministrativeOriginLocationCode);
      addAttributeToElement(lineItemVendorAdministrativeOriginElement, "externalJurisdictionCode",
              vendorAdministrativeOriginExternalJurisdictionCode);

      Element lineItemVendorAdministrativeOriginStreetAddress1Element = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "StreetAddress1",
              vendorAdministrativeOriginStreetAddress1);
      Element lineItemVendorAdministrativeOriginStreetAddress2Element = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "StreetAddress2",
              vendorAdministrativeOriginStreetAddress2);
      Element lineItemVendorAdministrativeOriginStreetCityElement = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "City", vendorAdministrativeOriginCity);
      Element lineItemVendorAdministrativeOriginMainDivisionElement = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "MainDivision", vendorAdministrativeOriginMainDivision);
      Element lineItemVendorAdministrativeOriginSubDivisionElement = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "SubDivision", vendorAdministrativeOriginSubDivision);
      Element lineItemVendorAdministrativeOriginPostalElement = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "PostalCode", vendorAdministrativeOriginPostalCode);
      Element lineItemVendorAdministrativeOriginCountryElement = addElementToDoc(
              lineItemVendorAdministrativeOriginElement, "Country", vendorAdministrativeOriginCountry);

      Element lineItemVendorAdministrativeOriginCurrencyConversion = createStubElement(
              lineItemVendorAdministrativeOriginElement, "CurrencyConversion");
      addAttributeToElement(lineItemVendorAdministrativeOriginCurrencyConversion, "isoCurrencyName",
              vendorAdministrativeOriginCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemVendorAdministrativeOriginCurrencyConversion, "isoCurrencyCodeAlpha",
              vendorAdministrativeOriginCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemVendorAdministrativeOriginCurrencyConversion, "isoCurrencyCodeNum",
              vendorAdministrativeOriginCurrencyConversionIsoCurrencyCodeNum);
    }

    if (!isNullOrEmpty(taxOverrideOverrideType)) {
      Element lineItemTaxOverride = createStubElement(lineItemElement, "TaxOverride");
      addAttributeToElement(lineItemTaxOverride, "overrideType", taxOverrideOverrideType);
      addAttributeToElement(lineItemTaxOverride, "overrideReasonCode", taxOverrideOverrideReasonCode);
    }

    if (!isNullOrEmpty(impositionToProcessImpositionType)) {
      Element lineItemImpositionToProcess = createStubElement(lineItemElement, "ImpositionToProcess");
      addAttributeToElement(lineItemImpositionToProcess, "impositionType", impositionToProcessImpositionType);
    }

    // JURISDICTION OVERRIDE
    if (!isNullOrEmpty(jurisdictionOverrideJurisdictionLevel)) {
      Element lineItemJurisdictionOverride = createStubElement(lineItemElement, "JurisdictionOverride");
      addAttributeToElement(lineItemJurisdictionOverride, "jurisdictionLevel",
              jurisdictionOverrideJurisdictionLevel);
      addAttributeToElement(lineItemJurisdictionOverride, "impositionType", jurisdictionOverrideImpositionType);

      Element lineItemJurisdictionOverrideDeductionOverride = createStubElement(lineItemJurisdictionOverride,
              "DeductionOverride");
      Element lineItemJurisdictionOverrideDeductionOverrideExemptOverride = addElementToDoc(lineItemElement,
              "ExemptOverride", jurisdictionOverrideDeductionOverrideExemptOverride);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyName",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyName);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyCodeAlpha",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyCodeNum",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeNum);

      Element lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride = addElementToDoc(lineItemElement,
              "NonTaxableOverride", jurisdictionOverrideDeductionOverrideNonTaxableOverride);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride, "isoCurrencyName",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyName);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride,
              "isoCurrencyCodeAlpha",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride, "isoCurrencyCodeNum",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeNum);

      if (!isNullOrEmpty(jurisdictionOverrideRateOverride)) {
        Element lineItemJurisdictionOverrideRateOverride = addElementToDoc(lineItemJurisdictionOverride,
                "RateOverride", jurisdictionOverrideRateOverride);
      }

    } // end JURISDICTION OVERRIDE

    // LINE ITEM SITUS OVERRIDE
    if (!isNullOrEmpty(situsOverrideTaxingLocation)) {
      Element lineItemSitusOverride = createStubElement(lineItemElement, "SitusOverride");
      addAttributeToElement(lineItemSitusOverride, "taxingLocation", situsOverrideTaxingLocation);
    }

    // Line Item Purchase
    Element lineItemPurchaseElement = addElementToDoc(lineItemElement, "Purchase", purchase);
    addAttributeToElement(lineItemPurchaseElement, "purchaseClass", purchasePurchaseClass);

    // LINE ITEM COMMODITY CODE
    Element lineItemCommodityCode = addElementToDoc(lineItemElement, "CommodityCode", commodityCode);
    addAttributeToElement(lineItemCommodityCode, "commodityCodeType", commodityCodeCommodityCodeType);

    // LINE ITEM QUANTITY
    Element lineItemQuantity = addElementToDoc(lineItemElement, "Quantity", quantity);
    addAttributeToElement(lineItemQuantity, "unitOfMeasure", quantityUnitOfMeasure);

    // LINE ITEM QUANTITY
    Element lineItemWeight = addElementToDoc(lineItemElement, "Weight", weight);
    addAttributeToElement(lineItemWeight, "unitOfMeasure", weightUnitOfMeasure);

    // LINE ITEM VOLUME
    Element lineItemVolume = addElementToDoc(lineItemElement, "Volume", volume);
    addAttributeToElement(lineItemVolume, "unitOfMeasure", volumeUnitOfMeasure);

    Element lineItemFreight = addElementToDoc(lineItemElement, "Freight", freight);
    addAttributeToElement(lineItemFreight, "isoCurrencyName", freightIsoCurrencyName);
    addAttributeToElement(lineItemFreight, "isoCurrencyCodeAlpha", freightIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemFreight, "isoCurrencyCodeNum", freightIsoCurrencyCodeNum);

    Element lineItemUnitPrice = addElementToDoc(lineItemElement, "UnitPrice", unitPrice);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyName", unitPriceIsoCurrencyName);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyCodeAlpha", unitPriceIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyCodeNum", unitPriceIsoCurrencyCodeNum);

    Element lineItemExtendedPrice = addElementToDoc(lineItemElement, "ExtendedPrice", extendedPrice);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyName", extendedPriceIsoCurrencyName);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyCodeAlpha", extendedPriceIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyCodeNum", extendedPriceIsoCurrencyCodeNum);

    Element lineItemLandedCost = addElementToDoc(lineItemElement, "LandedCost", landedCost);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyName", landedCostIsoCurrencyName);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyCodeAlpha", landedCostIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyCodeNum", landedCostIsoCurrencyCodeNum);

    Element lineItemAmountBilledToDate = addElementToDoc(lineItemElement, "AmountBilledToDate", amountBilledToDate);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyName", amountBilledToDateIsoCurrencyName);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyCodeAlpha",
            amountBilledToDateIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyCodeNum", amountBilledToDateIsoCurrencyCodeNum);


    //Flexfields 
    Element lineItemFlexibleFields = createStubElement(lineItemElement, "FlexibleFields");
    if (!isNullOrEmpty(flexibleFieldsFlexibleCode1)) {
      Element lineItemFlexibleCodeField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode1);
      addAttributeToElement(lineItemFlexibleCodeField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode2)) {
      Element lineItemFlexibleCodeField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode2);
      addAttributeToElement(lineItemFlexibleCodeField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode3)) {
      Element lineItemFlexibleCodeField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode3);
      addAttributeToElement(lineItemFlexibleCodeField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode4)) {
      Element lineItemFlexibleCodeField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode4);
      addAttributeToElement(lineItemFlexibleCodeField4, "fieldId", "4");
    }
    if (!isNullOrEmpty(flexibleFieldsFlexibleCode5)) {
      Element lineItemFlexibleCodeField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode5);
      addAttributeToElement(lineItemFlexibleCodeField5, "fieldId", "5");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode6)) {
      Element lineItemFlexibleCodeField6 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode6);
      addAttributeToElement(lineItemFlexibleCodeField6, "fieldId", "6");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode7)) {
      Element lineItemFlexibleCodeField7 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode7);
      addAttributeToElement(lineItemFlexibleCodeField7, "fieldId", "7");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode8)) {
      Element lineItemFlexibleCodeField8 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode8);
      addAttributeToElement(lineItemFlexibleCodeField8, "fieldId", "8");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode9)) {
      Element lineItemFlexibleCodeField9 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode9);
      addAttributeToElement(lineItemFlexibleCodeField9, "fieldId", "9");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode10)) {
      Element lineItemFlexibleCodeField10 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode10);
      addAttributeToElement(lineItemFlexibleCodeField10, "fieldId", "10");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode11)) {
      Element lineItemFlexibleCodeField11 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode11);
      addAttributeToElement(lineItemFlexibleCodeField11, "fieldId", "11");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode12)) {
      Element lineItemFlexibleCodeField12 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode12);
      addAttributeToElement(lineItemFlexibleCodeField12, "fieldId", "12");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode13)) {
      Element lineItemFlexibleCodeField13 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode13);
      addAttributeToElement(lineItemFlexibleCodeField13, "fieldId", "13");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode14)) {
      Element lineItemFlexibleCodeField14 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode14);
      addAttributeToElement(lineItemFlexibleCodeField14, "fieldId", "14");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode15)) {
      Element lineItemFlexibleCodeField15 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode15);
      addAttributeToElement(lineItemFlexibleCodeField15, "fieldId", "15");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode16)) {
      Element lineItemFlexibleCodeField16 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode16);
      addAttributeToElement(lineItemFlexibleCodeField16, "fieldId", "16");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode17)) {
      Element lineItemFlexibleCodeField17 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode17);
      addAttributeToElement(lineItemFlexibleCodeField17, "fieldId", "17");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode18)) {
      Element lineItemFlexibleCodeField18 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode18);
      addAttributeToElement(lineItemFlexibleCodeField18, "fieldId", "18");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode19)) {
      Element lineItemFlexibleCodeField19 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode19);
      addAttributeToElement(lineItemFlexibleCodeField19, "fieldId", "19");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode20)) {
      Element lineItemFlexibleCodeField20 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode20);
      addAttributeToElement(lineItemFlexibleCodeField20, "fieldId", "20");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode21)) {
      Element lineItemFlexibleCodeField21 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode21);
      addAttributeToElement(lineItemFlexibleCodeField21, "fieldId", "21");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode22)) {
      Element lineItemFlexibleCodeField22 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode22);
      addAttributeToElement(lineItemFlexibleCodeField22, "fieldId", "22");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode23)) {
      Element lineItemFlexibleCodeField23 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode23);
      addAttributeToElement(lineItemFlexibleCodeField23, "fieldId", "23");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode24)) {
      Element lineItemFlexibleCodeField24 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode24);
      addAttributeToElement(lineItemFlexibleCodeField24, "fieldId", "24");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode25)) {
      Element lineItemFlexibleCodeField25 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode25);
      addAttributeToElement(lineItemFlexibleCodeField25, "fieldId", "25");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric1)) {
      Element lineItemFlexibleNumericField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric1);
      addAttributeToElement(lineItemFlexibleNumericField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric2)) {
      Element lineItemFlexibleNumericField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric2);
      addAttributeToElement(lineItemFlexibleNumericField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric3)) {
      Element lineItemFlexibleNumericField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric3);
      addAttributeToElement(lineItemFlexibleNumericField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric4)) {
      Element lineItemFlexibleNumericField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric4);
      addAttributeToElement(lineItemFlexibleNumericField4, "fieldId", "4");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric5)) {
      Element lineItemFlexibleNumericField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric5);
      addAttributeToElement(lineItemFlexibleNumericField5, "fieldId", "5");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate1)) {
      Element lineItemFlexibleDateField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate1);
      addAttributeToElement(lineItemFlexibleDateField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate2)) {
      Element lineItemFlexibleDateField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate2);
      addAttributeToElement(lineItemFlexibleDateField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate3)) {
      Element lineItemFlexibleDateField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate3);
      addAttributeToElement(lineItemFlexibleDateField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate4)) {
      Element lineItemFlexibleDateField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate4);
      addAttributeToElement(lineItemFlexibleDateField4, "fieldId", "4");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate5)) {
      Element lineItemFlexibleDateField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate5);
      addAttributeToElement(lineItemFlexibleDateField5, "fieldId", "5");
    }


    //added fields for invoice verification request only	
    if (!financialEventName.toLowerCase().contains("verification")) {
      Element lineItemChargedTax = addElementToDoc(lineItemElement, "ChargedTax", chargedTax);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyName", chargedTaxIsoCurrencyName);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeAlpha", chargedTaxIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeNum", chargedTaxIsoCurrencyCodeNum);
    }


    //added fields for accrual request only		
    if (financialEventName.toLowerCase().contains("accrual")) {
      //charged tax
      Element lineItemChargedTax = addElementToDoc(lineItemElement, "ChargedTax", chargedTax);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyName", chargedTaxIsoCurrencyName);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeAlpha", chargedTaxIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeNum", chargedTaxIsoCurrencyCodeNum);

    }

    //added fields for distribute tax only
    if (financialEventName.toLowerCase().contains("distribute")) {
      //add header level attributes
      addAttributeToElement(financialEventElement, "postToJournal", postToJournal);
      addAttributeToElement(financialEventElement, "isTaxOnlyAdjustmentIndicator", isTaxOnlyAdjustmentIndicator);


      //add charged tax
      Element lineItemChargedTax = addElementToDoc(lineItemElement, "ChargedTax", chargedTax);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyName", chargedTaxIsoCurrencyName);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeAlpha", chargedTaxIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemChargedTax, "isoCurrencyCodeNum", chargedTaxIsoCurrencyCodeNum);

      //add input total tax
      Element lineItemInputTotalTax = addElementToDoc(lineItemElement, "InputTotalTax", inputTotalTax);
      addAttributeToElement(lineItemInputTotalTax, "isoCurrencyName", inputTotalTaxIsoCurrencyName);
      addAttributeToElement(lineItemInputTotalTax, "isoCurrencyCodeAlpha", inputTotalTaxIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemInputTotalTax, "isoCurrencyCodeNum", inputTotalTaxIsoCurrencyCodeNum);

      //add ReportedSelfAccrualRecoverableAmount
      Element lineItemReportedSelfAccrualRecoverableAmount = addElementToDoc(lineItemElement, "ReportedSelfAccrualRecoverableAmount", reportedSelfAccrualRecoverableAmount);
      addAttributeToElement(lineItemReportedSelfAccrualRecoverableAmount, "isoCurrencyName", reportedSelfAccrualRecoverableAmountIsoCurrencyName);
      addAttributeToElement(lineItemReportedSelfAccrualRecoverableAmount, "isoCurrencyCodeAlpha", reportedSelfAccrualRecoverableAmountIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemReportedSelfAccrualRecoverableAmount, "isoCurrencyCodeNum", reportedSelfAccrualRecoverableAmountIsoCurrencyCodeNum);
    }

    //add input tax element and sub elements for distribute tax and accrual request
    //if(financialEventName.toLowerCase().contains("distribute") || financialEventName.toLowerCase().contains("accrual") ){

    //CANNOT DO INPUT TAX CHILD ELEMENTS because of flat design, no elements with multiple children under line item element allowed
    //Element lineItemInputTax = createStubElement(lineItemElement, "InputTax");
    //addAttributeToElement(lineItemInputTax, "isImport", inputTaxIsImport);


    //}


  }

  public Document getOutDoc() {
    return outDoc;
  }

  public void setOutDoc(Document doc) {
    outDoc = doc;
  }

  private static Element createStubElement(Element parentElement, String elementName) {
    Element element = outDoc.createElement(elementName);
    parentElement.appendChild(element);
    return element;
  }

  private static Element addElementToDoc(Element parentElement, String elementName, String elementValue) {
    Element element = outDoc.createElement(elementName);
    if (!isNullOrEmpty(elementValue)) {
      element.setTextContent(elementValue);
      parentElement.appendChild(element);
    }
    return element;
  }

  private static void addAttributeToElement(Element parentElement, String attributeName, String attributeValue) {
    if (!isNullOrEmpty(attributeValue)) {
      parentElement.setAttribute(attributeName, attributeValue);
    }
  }
}
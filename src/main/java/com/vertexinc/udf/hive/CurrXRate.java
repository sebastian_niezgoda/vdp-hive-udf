package com.vertexinc.udf.hive;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;

@Description(
        name = "Lookup",
        value = "Lookup(str,str) - Look up reference value using file table",
        extended = "Example: \nSELECT lookup(column, table) FROM hive_table; "
)
public class CurrXRate extends VertexUDF {
  private enum PROVIDER_LOCATIONS {DEFAULT, POLAND, EUROPE}

  private enum PROVIDER {FIXER, PNB, ECB}

  private enum CURRENCY_ABBRV {PLN, EUR}

  private static DateFormat dateFormat = new SimpleDateFormat(HIVE_DATE_FORMAT);

  private final static String delimiter = "|";
  private static ConcurrentHashMap<PROVIDER, ConcurrentHashMap<String, Double>> providerRateMap = new ConcurrentHashMap<>();

  public Double evaluate(Text date, Text from, Text to, Text provider) {
    if (from == null || to == null) {
      return 0.0;
    }

    // make upper case because the API does not like lower case
    from = new Text(from.toString().toUpperCase());
    to = new Text(to.toString().toUpperCase());

    // if converting from a currency to itself the exchange rate is always 1.0
    if (from.toString().equals(to.toString())) {
      return 1.0;
    }

    if (PROVIDER_LOCATIONS.DEFAULT.toString().equalsIgnoreCase(provider.toString())) {
      if (date == null || date.toString().isEmpty()) {
        return getFixerRate(from.toString(), to.toString());
      } else {
        return getFixerRate(date.toString(), from.toString(), to.toString());
      }
    } else if (PROVIDER_LOCATIONS.POLAND.toString().equalsIgnoreCase(provider.toString())) {
      if (date == null || date.toString().isEmpty()) {
        return getPolandNationalBankRate(from.toString(), to.toString());
      }
      return getPolandNationalBankRate(date.toString(), from.toString(), to.toString());
    } else if (PROVIDER_LOCATIONS.EUROPE.toString().equalsIgnoreCase(provider.toString())) {
      if (date == null || date.toString().isEmpty()) {
        return getEuropeanNationalBankRate(from.toString(), to.toString());
      }
      return getEuropeanNationalBankRate(date.toString(), from.toString(), to.toString());
    }
    return 0.0;
  }

  public Double evaluate(Text date, Text from, Text to) {
    return evaluate(date, from, to, new Text(PROVIDER_LOCATIONS.DEFAULT.toString()));
  }

  public Double evaluate(Text from, Text to) {
    return evaluate(new Text(dateFormat.format(new Date())), from, to, new Text(PROVIDER_LOCATIONS.DEFAULT.toString()));
  }

  private Double getFixerRate(String date, String from, String to, Boolean isRetry) {
    String dateNow = getFormattedDate(date);

    try {
      Double val = providerRateMap.getOrDefault(PROVIDER.FIXER, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
      if (val == null) {
        cacheFixerRates(date, from, to);
        val = providerRateMap.getOrDefault(PROVIDER.FIXER, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
      }
      return val == null ? 0.00 : val;
    } catch (Exception e) {
      LOGGER.error(e.toString());

      if (isRetry)
        return 0.0;

      try {
        Thread.sleep(5000);
        return getFixerRate(dateNow, from, to, true);
      } catch (InterruptedException e1) {
        LOGGER.error(e1.toString());
      }
      return 0.0;
    }
  }

  private Double getFixerRate(String date, String from, String to) {
    return getFixerRate(date, from, to, false);
  }

  private Double getFixerRate(String from, String to) {
    return getFixerRate(getFormattedDate(new Date()), from, to, false);
  }

  private Double getPolandNationalBankRate(String from, String to) {
    return getPolandNationalBankRate(getFormattedDate(new Date()), from, to);
  }

  private Double getPolandNationalBankRate(String date, String from, String to) {
    if(!from.equals(CURRENCY_ABBRV.PLN.toString())){
      return 0.0;
    }

    String dateNow = getFormattedDate(date);

    try {
      Double val = providerRateMap.getOrDefault(PROVIDER.PNB, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
      if (val == null) {
        cachePNBRates(dateNow);
        val = providerRateMap.getOrDefault(PROVIDER.PNB, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
        return val == null ? 0.00 : val;
      } else {
        return val;
      }
    } catch (Exception e) {
      LOGGER.error(e.toString());
      return 0.00;
    }
  }

  private Double getEuropeanNationalBankRate(String date, String from, String to) {
    Date d = parseDate(date);

    String dateNow = getFormattedDate(date);

    Double val = providerRateMap.getOrDefault(PROVIDER.ECB, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
    if (val == null) {
      cacheECBRates(d, to);
      val = providerRateMap.getOrDefault(PROVIDER.ECB, new ConcurrentHashMap<>()).get(getKey(from, to, dateNow));
    }
    return val == null ? 0.00 : val;
  }

  private Double getEuropeanNationalBankRate(String from, String to) {
    return getEuropeanNationalBankRate(getFormattedDate(new Date()), from, to);
  }

  private void cacheFixerRates(String date, String from, String to) {
    ConcurrentHashMap<String, Double> fixer = providerRateMap.get(PROVIDER.FIXER);
    if (fixer == null) {
      fixer = new ConcurrentHashMap<>();
      providerRateMap.put(PROVIDER.FIXER, fixer);
    }

    String dateNow = getFormattedDate(date);

    if (fixer.containsKey(getKey(from, to, dateNow))) {
      return;
    }

    try {
      String jsonStr = IOUtils.toString(getFixerUrl(dateNow, from), StandardCharsets.UTF_8);
      if (jsonStr.isEmpty() || jsonStr.contains("\"success\":false")) {
        return;
      }

      JSONObject ratesArray = new JSONObject(jsonStr).getJSONObject("rates");

      Iterator<?> keys = ratesArray.keys();
      while (keys.hasNext()) {
        String curTo = (String) keys.next();
        Double amt = ratesArray.getDouble(curTo);

        Double inv = (double) Math.round((1 / amt) * 10000d) / 10000d;
        fixer.put(getKey(from, curTo, dateNow), inv);
      }
      providerRateMap.put(PROVIDER.FIXER, fixer);
    } catch (Exception e) {
      LOGGER.error(e.toString());
    }
  }

  private void cachePNBRates(String date) {
    ConcurrentHashMap<String, Double> pnb = providerRateMap.get(PROVIDER.PNB);
    if (pnb == null) {
      pnb = new ConcurrentHashMap<>();
      providerRateMap.put(PROVIDER.PNB, pnb);
    }
    try {
      String json = IOUtils.toString(getNBPUrl(date), StandardCharsets.UTF_8);
      if (json.isEmpty())
        return;

      String from = CURRENCY_ABBRV.PLN.toString();

      JSONArray jsonArr = new JSONArray(json);

      for (int i = 0; i < jsonArr.length(); i++) {
        JSONObject rec = jsonArr.getJSONObject(i);
        JSONArray ratesArray = rec.getJSONArray("rates");

        for (int rateIndex = 0; rateIndex < ratesArray.length(); rateIndex++) {
          JSONObject rateRec = ratesArray.getJSONObject(rateIndex);

          // String from = rateRec.getString("code");
          String to = rateRec.getString("code");
          Double amt = rateRec.getDouble("mid");

          Double inv = (double) Math.round((1 / amt) * 10000d) / 10000d;
          pnb.put(getKey(from, to, date), inv);
        }
      }
    } catch (Exception e) {
      LOGGER.error(e.toString());
    }
  }

  private void cacheECBRates(Date date, String to) {
    String from = CURRENCY_ABBRV.EUR.toString();

    ConcurrentHashMap<String, Double> ecb = providerRateMap.get(PROVIDER.ECB);
    if (ecb == null) {
      ecb = new ConcurrentHashMap<>();
      providerRateMap.put(PROVIDER.ECB, ecb);
    }

    if (ecb.containsKey(getKey(from, to, date)))
      return;

    int year = getCurrentYear(date);

    try {
      String json = getJson(getECBUrl(to, year));

      if (json.isEmpty() || json.contains("No results found."))
        return;

      Map<String, String> dateIndexMap = new HashMap<String, String>();
      JSONObject theData = new JSONObject(json);

      JSONObject struct = theData.getJSONObject("structure");
      JSONObject dims = struct.getJSONObject("dimensions");
      JSONArray obs = dims.getJSONArray("observation");
      for (int obsIndex = 0; obsIndex < obs.length(); obsIndex++) {
        JSONObject thisObs = obs.getJSONObject(obsIndex);
        JSONArray obsVals = thisObs.getJSONArray("values");
        for (int valIndex = 0; valIndex < obsVals.length(); valIndex++) {
          JSONObject val = obsVals.getJSONObject(valIndex);
          String dt = val.getString("name");
          dateIndexMap.put(Integer.toString(valIndex), dt);
        }
      }

      JSONArray jsonArr = theData.getJSONArray("dataSets");
      for (int index = 0; index < jsonArr.length(); index++) {
        JSONObject rec = jsonArr.getJSONObject(index);

        JSONObject observations = rec.getJSONObject("series")
                .getJSONObject("0:0:0:0:0")
                .getJSONObject("observations");

        for (Map.Entry<String, String> entry : dateIndexMap.entrySet()) {
          Double rt = observations.getJSONArray(entry.getKey()).getDouble(0);
          String dt = entry.getValue();

          ecb.put(from + delimiter + to + delimiter + dt, rt);
        }
      }

    } catch (Exception e) {
      LOGGER.error(e.toString());
    }
  }

  private Date getWorkingDay(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);

    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

    while (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
      cal.add(Calendar.DAY_OF_MONTH, -1);
      dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
    }

    return cal.getTime();
  }

  /**
   * Returns a correctly formatted date string which represents the nearest weekday (looking back in time) from the supplied date.
   */
  private String getFormattedDate(String date) {
    try {
      return dateFormat.format(getWorkingDay(dateFormat.parse(date))).replaceAll("/", "-");
    } catch (ParseException e) {
      LOGGER.error(e.toString());
      return null;
    }
  }

  /**
   * Returns a correctly formatted date string which represents the nearest weekday (looking back in time) from the supplied date.
   */
  private String getFormattedDate(Date date) {
    return dateFormat.format(getWorkingDay(date)).replaceAll("/", "-");
  }

  private Date parseDate(String date) {
    try {
      return dateFormat.parse(date);
    } catch (ParseException e) {
      LOGGER.error(e.toString());
      return null;
    }
  }

  /**
   * Builds a key to get the parameters from the providerRateMap
   */
  private String getKey(String from, String to, String dateNow) {
    return from + delimiter + to + delimiter + dateNow;
  }

  private String getKey(String from, String to, Date dateNow) {
    return from + delimiter + to + delimiter + dateNow;
  }

  private URL getFixerUrl(String dateNow, String from) {
    try {
      return new URL("http://data.fixer.io/api/" + dateNow + "?base=" + from + "&access_key=" + getFixerApiToken());
    } catch (MalformedURLException e) {
      LOGGER.error(e.toString());
      return null;
    }
  }

  private URL getNBPUrl(String date) {
    try {
      return new URL("http://api.nbp.pl/api/exchangerates/tables/a/" + date + "/?format=json");
    } catch (MalformedURLException e) {
      LOGGER.error(e.toString());
      return null;
    }
  }

  private URL getECBUrl(String to, int year) {
    try {
      return new URL("https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D." + to + ".EUR.SP00.A?startPeriod=" + year + "&endPeriod=" + year);
    } catch (MalformedURLException e) {
      LOGGER.error(e.toString());
      return null;
    }
  }

  private int getCurrentYear(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);

    return cal.get(Calendar.YEAR);
  }

  private String getJson(URL url) {
    BufferedReader rd = null;
    String line;
    String result = "";
    try {
      HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
      conn.setRequestMethod("GET");
      conn.setRequestProperty("Accept-Encoding", "gzip,deflate");
      conn.setRequestProperty("Accept-Charset", "UTF-8");
      conn.setRequestProperty("Accept", "application/vnd.sdmx.data+json;version=1.0.0-wd");
      String response = conn.getResponseMessage();

      if (response.isEmpty())
        return result;

      if (conn.getContentEncoding().equals("gzip")) {
        Reader decoder = new InputStreamReader(new GZIPInputStream(conn.getInputStream()));
        rd = new BufferedReader(decoder);
      } else {
        rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
      }
      while ((line = rd.readLine()) != null) {
        result += line;
      }
      rd.close();
    } catch (Exception e) {
      LOGGER.error(e.toString());
    }

    return result;
  }

}

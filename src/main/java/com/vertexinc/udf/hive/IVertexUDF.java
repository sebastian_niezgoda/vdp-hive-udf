package com.vertexinc.udf.hive;

import java.text.SimpleDateFormat;

public interface IVertexUDF {

  String HIVE_DATE_FORMAT = "yyyy-MM-dd";

  static SimpleDateFormat getHiveDataFormatter() {
    return new SimpleDateFormat(HIVE_DATE_FORMAT);
  }
}

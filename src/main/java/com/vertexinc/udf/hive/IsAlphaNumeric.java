package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

@Description(
        name = "IsAlphaNumeric",
        value = "IsAlphaNumeric(Text) - return true if text is only characters or numbers",
        extended = "Example: SELECT isAlphaNumeric('11235') FROM hive_table; "
)
public class IsAlphaNumeric extends VertexUDF{
  public Boolean evaluate(Text alphaText) {
    return alphaText != null && alphaText.toString().matches("[a-zA-Z0-9 ]+");
  }
}

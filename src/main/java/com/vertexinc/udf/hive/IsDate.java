package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Description(
        name = "IsDate",
        value = "IsDate(string, optional) - return true if string can be a date using list of most likely date formats",
        extended = "Example: \nSELECT isDate('1-OCT-15') FROM hive_table; "
)
public class IsDate extends VertexUDF {
  // list of date formats used for for parsing if format not specified
  private static final String[] DATE_FORMAT_LIST = new String[]{"yy-MM-dd", // 2015-12-31,
          // 15-12-31
          "MM/dd/yy", // 12/31/15, 12/31/2015
          "MM-dd-yy", // 12-31-15, 12-31-2015
          "dd-MMM-yy", // 31-DEC-15, 31-DEC-2015
          "MMM-dd-yy", // DEC-31-15, DEC-31-2015
          "yyyyMMdd", // 20151231
          "MM/dd/yy hh:mm", // 12/31/15 12:34
          "MM-dd-yy hh:ss", // 12-31-15 12:34
          "dd-MM-yyyy", "yyyy-MM-dd", "MM/dd/yyyy", "yyyy/MM/dd", "dd MMM yyyy", "dd MMMM yyyy", "yyyyMMddHHmm",
          "yyyyMMdd HHmm", "dd-MM-yyyy HH:mm", "yyyy-MM-dd HH:mm", "MM/dd/yyyy HH:mm", "yyyy/MM/dd HH:mm",
          "dd MMM yyyy HH:mm", "dd MMMM yyyy HH:mm", "yyyyMMddHHmmss", "yyyyMMdd HHmmss", "dd-MM-yyyy HH:mm:ss",
          "yyyy-MM-dd HH:mm:ss", "MM/dd/yyyy HH:mm:ss", "yyyy/MM/dd HH:mm:ss", "dd MMM yyyy HH:mm:ss",
          "dd MMMM yyyy HH:mm:ss",};

  /**
   * If no dateFormat is supplied, try every common date format until a match is found. If no match is found, reject the input.
   * @return true if the dateString can be successfully parsed as a date, false otherwise
   */
  public Boolean evaluate(Text dateString) {
    for (String dateFormat : DATE_FORMAT_LIST) {
      Boolean evaluation = evaluate(dateString, new Text(dateFormat));
      if (evaluation) {
        return evaluation;
      }
    }
    return false;
  }

  public Boolean evaluate(Text dateString, Text dateFormat) {
    if (dateString == null || dateFormat == null) {
      return false;
    }

    try {
      Date date = new SimpleDateFormat(dateFormat.toString()).parse(dateString.toString());
      if (date != null) {
        return true;
      }
    } catch (ParseException e) {
      return false;
    }
    return false;
  }
}

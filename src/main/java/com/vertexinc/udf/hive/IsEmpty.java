package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;
import org.apache.hadoop.io.BooleanWritable;

@Description(
        name = "isempty",
        value = "_FUNC_ a - Returns true if a is empty or NULL and false otherwise"
)
public class IsEmpty extends VertexGenericUDF {

  @Override
  public ObjectInspector initialize(ObjectInspector[] objectInspectors) throws UDFArgumentException {
    if(objectInspectors.length != 1){
      throw new UDFArgumentLengthException("The operator IsEmpty only accepts 1 argument.");
    }

    if(!(objectInspectors[0] instanceof StringObjectInspector)){
      throw new UDFArgumentException("First argument must be a string.");
    }

    return PrimitiveObjectInspectorFactory.writableBooleanObjectInspector;
  }

  @Override
  public Object evaluate(DeferredObject[] deferredObjects) throws HiveException {
    BooleanWritable result = new BooleanWritable();
    if(deferredObjects == null){
      return null;
    }
    result.set(deferredObjects[0].get() == null || "".equals(deferredObjects[0].get().toString().trim()));
    return result;
  }

  @Override
  public String getDisplayString(String[] strings) {
    assert (strings.length == 1);
    return strings[0] + " is empty";
  }
}

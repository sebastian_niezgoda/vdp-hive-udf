package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

@Description(
        name = "IsInteger",
        value = "IsInteger(string) - return true if string is an integer",
        extended = "Example: \nSELECT IsInteger('11235') FROM hive_table; "
)
public class IsInteger extends VertexUDF {
  public Boolean evaluate(Text numberString) {
    if (numberString == null) {
      return false;
    }

    return parseInteger(numberString);
  }

  private Boolean parseInteger(Text numberString) {
    try {
      Integer.parseInt(numberString.toString());
      return true;
    } catch (NumberFormatException e) {
      return false;
    } catch (NullPointerException e) {
      return false;
    }
  }
}

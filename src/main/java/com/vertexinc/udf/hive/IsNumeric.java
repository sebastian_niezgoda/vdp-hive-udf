package com.vertexinc.udf.hive;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

@Description(
        name = "IsNumeric",
        value = "IsNumeric(Text) - return true if text is only numbers",
        extended = "Example: \nSELECT isNumeric('11235') FROM hive_table; "
)
public class IsNumeric extends VertexUDF {
  public Boolean evaluate(Text numberText){
    if(numberText == null){
      return false;
    }

    // the field dev code applied the following, which I believe is unnecessary, and thus I am commenting it out
//    String newString = numberText.toString().replaceAll("[^a-zA-Z0-9]","");
    return NumberUtils.isNumber(numberText.toString());
  }
}

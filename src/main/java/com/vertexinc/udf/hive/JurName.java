package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.io.Text;

import java.util.concurrent.ConcurrentHashMap;

@Description(name = "MainDivision", value = "MainDivision(str) - Retrieve main division from TAID", extended = "Example: \n"
        + " SELECT MainDivision(column) FROM hive_table; ")
public class JurName extends OSeriesUDF {

  private static final String SOAP_ACTION = "LookupTaxAreas60";
  private static ConcurrentHashMap<String, Text> jurNameMap = new ConcurrentHashMap<String, Text>();

  private String taid;
  private String jur;

  @Override
  protected String getXmlRequest(DeferredObject[] arguments) throws HiveException {
    taid = getArgumentValue(arguments, 0).trim().toUpperCase();
    jur = getArgumentValue(arguments, 1).trim().toUpperCase();

    return buildRequest();
  }

  @Override
  public Object evaluate(DeferredObject[] arguments) throws HiveException {
    String xmlRequest = getXmlRequest(arguments);
    if (xmlRequest == null) return null;

    if(isNullOrEmpty(taid) || isNullOrEmpty(jur)){
      return null;
    }

    if ("|STATE|TERRITORY|PROVINCE|".contains(jur)) {
      jur = "(STATE|TERRITORY|PROVINCE)";
    }

    String key = taid + "|" + jur;

    if (jurNameMap.containsKey(key)) {
      Text txt = jurNameMap.get(key);
      return (txt.getLength()==0 ? null : txt);
    }

    String response = submitRequest(xmlRequest);

    if (response==null) {
      jurNameMap.putIfAbsent(key, new Text());
      return null;
    }
    String taxResponse = getTaxResponse(response, jur);

    jurNameMap.putIfAbsent(key, taxResponse == null ? new Text() : new Text(taxResponse));

    //System.out.println(taxResponse);
    Text txt = jurNameMap.get(key);
    return (txt.getLength()==0 ? null : txt);
  }

  @Override
  protected String cleanResponse(String response) {
    return null;
  }

  @Override
  protected String getSoapAction() {
    return SOAP_ACTION;
  }

  @Override
  protected String getOseriesURL() {
    return oSeriesConfig.getProperty(OSERIES_URL_LOOKUP_PARAM);
  }

  @Override
  protected String buildRequestBodyXML() {
    return "<TaxAreaRequest>"
            + "<TaxAreaLookup>"
            + "<TaxAreaId>" + taid + "</TaxAreaId>"
            + "</TaxAreaLookup></TaxAreaRequest>";
  }

  private String getTaxResponse(String response, String jur) {
    if (response == null) return null;
    try {
      //String splitStr = "jurisdictionLevel=\"" + jur + "\"";
      String splitStr = "jurisdictionLevel=\"" + jur + "\"";
      String[] strs = response.split(splitStr);
      String jurName = strs[1];
      int idx1 = jurName.indexOf('>');
      int idx2 = jurName.indexOf('<');
      return jurName.substring(idx1+1, idx2);
    } catch (Exception e) {
      return null;
    }
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDFUtils;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorConverter;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.datanucleus.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Description(name = "lookup", value = "Retrieves the output value for mapped input values.")
public class Lookup extends VertexGenericUDF {

  private static final String DELIMITER = "|";

  private static final long MAX_LOOKUP_LIFE = 60000L; // 1 minute

  private Map<String, VELookup> lookups;

  private transient GenericUDFUtils.StringHelper returnHelper;

  private transient PrimitiveObjectInspectorConverter.StringConverter[] stringConverter;

  public ObjectInspector initialize(ObjectInspector[] arguments) throws UDFArgumentException {
    if (arguments.length < 2) {
      throw new UDFArgumentLengthException("The lookup function requires two or more parameters.");
    }

    stringConverter = new PrimitiveObjectInspectorConverter.StringConverter[arguments.length];

    for (int i = 0; i < arguments.length; i++) {
      // TODO: why is this only checking for the first argument to be primitive but not the rest?
//      if (arguments[0].getCategory() != ObjectInspector.Category.PRIMITIVE) {
//        throw new UDFArgumentException("The lookup function name parameter must be a primitive type.");
//      }
      stringConverter[i] = new PrimitiveObjectInspectorConverter.StringConverter((PrimitiveObjectInspector) arguments[i]);
    }

    super.init();

    returnHelper = new GenericUDFUtils.StringHelper(PrimitiveObjectInspector.PrimitiveCategory.STRING);

    return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
  }

  public Object evaluate(GenericUDF.DeferredObject[] arguments) throws HiveException {
    String inputValue = buildInputValueForArguments(arguments);

    String lookupName = (String) stringConverter[arguments.length - 1].convert(arguments[arguments.length - 1].get());

    String outputValue = getOutputValueFromLookup(inputValue, lookupName.toLowerCase());

    return returnHelper.setReturnValue(null == outputValue ? "" : outputValue);
  }

  private String buildInputValueForArguments(DeferredObject[] arguments) throws HiveException {
    StringBuilder sb = new StringBuilder();
    String prefix = "";
    for (int i = 0; i < (arguments.length - 1); i++) {
      sb.append(prefix);
      prefix = DELIMITER;
      sb.append((String) stringConverter[i].convert(arguments[i].get()));
    }
    return sb.toString();
  }

  public String getOutputValueFromLookup(String inputValue, String lookupName) {
    VELookup lookup = getLookup(lookupName);
    return null == lookup ? "" : lookup.getOutputValue(inputValue);
  }

  private VELookup getLookup(String lookupName) {
    VELookup lookup = null;

    if (null == lookups) {
      lookups = new HashMap<>();
    }

    lookup = lookups.get(lookupName);
    if (null == lookup || lookup.isExpired()) {
      lookups.remove(lookupName);
      lookup = readLookup(lookupName);
      lookups.put(lookupName, lookup);
    }

    return lookup;
  }

  private VELookup readLookup(String lookupName) {
    VELookup lookup = new VELookup(lookupName);

    if (StringUtils.isEmpty(loggedInApplication)) {
      throw new NullPointerException("Unable to retrieve application name from user.");
    }

    String hdfsPath = "/lookup-tables/" + loggedInApplication + "/" + lookupName + ".txt";

    try (
        FSDataInputStream is = FileSystem.get(new Configuration()).open(new Path(hdfsPath));
        BufferedReader br = new BufferedReader(new InputStreamReader(is))) {

      String line = br.readLine();
      while(null != line) {
        // skip comment lines
        if (!line.startsWith("#")) {
          String[] split = line.split("\t");
          lookup.addLookup(buildInputValue(Arrays.copyOfRange(split, 0, split.length - 2)), split[split.length - 1]);
        }
        line = br.readLine();
      }
    } catch (IOException ioe) {
      // do nothing
    }

    return lookup;
  }

  private String buildInputValue(String[] input) {
    StringBuilder sb = new StringBuilder();

    String prefix = "";
    for (int i = 0; i < input.length; i++) {
      sb.append(prefix);
      prefix = DELIMITER;
      sb.append(input[i].toUpperCase().trim());
    }

    return sb.toString();
  }

  public String getDisplayString(String[] children) {
    return "lookup()";
  }

  static class VELookup {
    private String lookupName;
    private Map<String, String> lookups;
    private long created;

    public VELookup(String lookupName) {
      this.lookupName = lookupName;
      this.created = new Date().getTime();
      lookups = new HashMap<>();
    }

    public boolean isExpired() {
      return MAX_LOOKUP_LIFE < (new Date().getTime() - this.created);
    }

    public void addLookup(String inputValue, String outputValue) {
      lookups.remove(inputValue);
      lookups.put(inputValue, outputValue);
    }

    public String getOutputValue(String inputValue) {
      return lookups.get(inputValue.toUpperCase().trim());
    }
  }

  /* FOR TESTS ONLY */
  protected void setLookups(Map<String, VELookup> lookups) {
    this.lookups = lookups;
  }
}

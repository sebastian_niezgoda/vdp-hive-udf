package com.vertexinc.udf.hive;

import com.vertexinc.udf.hive.service.ConfigurationPropertiesService;
import com.vertexinc.udf.hive.service.SessionService;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDFUtils;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector.Category;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector.PrimitiveCategory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorConverter.StringConverter;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import javax.ws.rs.HttpMethod;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


@Description(name = "OSeriesUDF", value = "_FUNC_(...) - ...")
public abstract class OSeriesUDF extends GenericUDF {

  static final Logger LOGGER = LoggerFactory.getLogger(OSeriesUDF.class.getName());

  public static final String OSERIES_TRUSTEDID_PARAM = "oseries.trustedid";
  public static final String OSERIES_USERNAME_PARAM = "oseries.username";
  public static final String OSERIES_PASSWORD_PARAM = "oseries.password";
  public static final String OSERIES_URL_CALC_PARAM = "oseries.url.calculatetax";
  public static final String OSERIES_URL_LOOKUP_PARAM = "oseries.url.lookuptaxareas";
  public static final String DEFAULT_APP = "oseriesdefault";
  public static final String HIVE_DATE_FORMAT = "yyyy-MM-dd";


  protected String loggedInUser;
  protected String loggedInApplication;
  protected Properties oSeriesConfig;

  protected ObjectInspector[] argumentOIs;
  protected List<StringConverter> stringConverters;
  protected PrimitiveCategory returnType = PrimitiveCategory.STRING;
  protected BytesWritable[] bw;
  protected GenericUDFUtils.StringHelper returnHelper;

  private ConfigurationPropertiesService propService = new ConfigurationPropertiesService();
  private SessionService sessionService = new SessionService();

  @Override
  public ObjectInspector initialize(ObjectInspector[] arguments)
      throws UDFArgumentException {

    if (arguments.length < 2) {
      throw new UDFArgumentLengthException("OSeries UDFs expect 2 or more arguments.");
    }

    stringConverters = new ArrayList<>(arguments.length);

    for (ObjectInspector objectInspector : arguments) {
      if (objectInspector.getCategory() != Category.PRIMITIVE) {
        throw new UDFArgumentException("OSeries UDFs require primitive types.");
      }
      stringConverters.add(new StringConverter((PrimitiveObjectInspector) objectInspector));
    }

    SessionState sessionState = SessionState.get();

    if (sessionState != null) {
      setLoggedInUser(sessionState.getUserName());
      setLoggedInApplication(sessionState);
    }

    LOGGER.info("logged in app:" + loggedInApplication);

    Properties prop = propService.loadOSeriesConfiguration(loggedInApplication);

    if (prop == null || prop.isEmpty()) {
      prop = propService.loadOSeriesConfiguration(DEFAULT_APP);
    }

    setOSeriesConfig(prop);

    trustAllCerts();

    return PrimitiveObjectInspectorFactory.writableStringObjectInspector;
  }

  public void setLoggedInUser(String u) {
    if (loggedInUser == null) {
      loggedInUser = u;
    }
  }

  public void setLoggedInApplication(SessionState sessionState){
    loggedInApplication = sessionService.getLoggedInApplication(sessionState);
  }

  /**
   * Set the logged in application.
   * @param value
   * @param parseAppFromUser If true, the logged in application is parsed from the provided "value"
   *                         parameter (this is useful if you are passing it something like "open.vedemo").
   *                         If false, the logged in application is set to the value of "value".
   */
  public void setLoggedInApplication(String value, Boolean parseAppFromUser){
    loggedInApplication = parseAppFromUser ? sessionService.getLoggedInApplication(value) : value;
  }

  private void trustAllCerts() {
    TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
      public X509Certificate[] getAcceptedIssuers() {
        return null;
      }

      public void checkClientTrusted(X509Certificate[] certs, String authType) {
      }

      public void checkServerTrusted(X509Certificate[] certs, String authType) {
      }
    }};

    try {
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
    }

    // Create all-trusting host name verifier
    HostnameVerifier allHostsValid = new HostnameVerifier() {
      public boolean verify(String hostname, SSLSession session) {
        return true;
      }
    };
    // Install the all-trusting host verifier
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
  }

  @Override
  public Object evaluate(DeferredObject[] arguments) throws HiveException {

    String xmlRequest = getXmlRequest(arguments);

    if (xmlRequest == null) {
      return null;
    }

    String response = submitRequest(xmlRequest);

    String tranformedResponse = null == response ? null : cleanResponse(response);
    LOGGER.info(tranformedResponse);

    return tranformedResponse == null ? null : new Text(tranformedResponse);
  }

  protected abstract String getXmlRequest(DeferredObject[] arguments) throws HiveException;

  protected abstract String cleanResponse(String response);

  protected abstract String getSoapAction();

  protected abstract String getOseriesURL();

  protected abstract String buildRequestBodyXML();

  protected String buildRequest() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
    stringBuilder.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">");
    stringBuilder.append("<soapenv:Header/>");
    stringBuilder.append("<soapenv:Body>");
    stringBuilder.append("<VertexEnvelope xmlns=\"urn:vertexinc:o-series:tps:6:0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");

    stringBuilder.append(buildRequestLogin());

    stringBuilder.append(buildRequestBodyXML());

    stringBuilder.append("</VertexEnvelope></soapenv:Body></soapenv:Envelope>");

    return stringBuilder.toString();
  }

  protected String createElement(String tagType, String value) {
    return (value == null || value.isEmpty()) ?
            "<" + tagType.trim() + "/>" :
            "<" + tagType.trim() + ">" + value.trim() + "</" + tagType.trim() + ">";
  }

  /**
   * Creates an XML tag of the format <tagType tagValueType="tagValue">value</tagType>
   */
  protected String createElementWithTagValue(String tagType, String tagValueType, String tagValue, String value) {
    return (value == null || value.isEmpty()) ?
            "<" + tagType.trim() + " " + tagValueType.trim() + "=\"" + tagValue.trim() + "\"/>" :
            "<" + tagType.trim() + " " + tagValueType.trim() + "=\"" + tagValue.trim() + "\">" + value.trim() + "</" + tagType.trim() + ">";
  }

  public static Boolean isNullOrEmpty(String string){
    return string == null || string.trim().length() == 0;
  }

  private String buildRequestLogin() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<Login>");
    if (oSeriesConfig.getProperty(OSERIES_TRUSTEDID_PARAM) != null && !oSeriesConfig.getProperty(OSERIES_TRUSTEDID_PARAM).isEmpty()) {
      stringBuilder.append(createElement("TrustedId", oSeriesConfig.getProperty(OSERIES_TRUSTEDID_PARAM)));
    } else {
      stringBuilder.append(createElement("UserName", oSeriesConfig.getProperty(OSERIES_USERNAME_PARAM)));
      stringBuilder.append(createElement("Password", oSeriesConfig.getProperty(OSERIES_PASSWORD_PARAM)));
    }
    stringBuilder.append("</Login>");

    return stringBuilder.toString();
  }

  @Override
  public String getDisplayString(String[] arg0) {
    return "OSeriesUDF";
  }

  public String getArgumentValue(DeferredObject[] arguments, int index) throws HiveException {
    try {
      if (arguments[index] != null) {
        return (String) stringConverters.get(index).convert(arguments[index].get());
      } else {
        return null;
      }
    } catch (HiveException e) {
      throw new HiveException("Unable to convert argument at index " + index);
    } catch (IndexOutOfBoundsException e){
      LOGGER.error("Index " + index + " is out of bounds of the argument array.");
      return null;
    }
  }

  public Double toDouble(String s) {
    try {
      return Double.parseDouble(s);
    } catch (Exception e) {
      return 0.0;
    }
  }

  public String submitRequest(String xml) {
    String response = null;

    long start = System.currentTimeMillis();

    HttpURLConnection conn = null;
    try {
      conn = (HttpURLConnection) new URL(getOseriesURL()).openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod(HttpMethod.POST);

      String soapAction = getSoapAction();

      if (soapAction != null) {
        conn.setRequestProperty("SOAPAction", "\"" + soapAction + "\"");
      }
      conn.setRequestProperty("Content-Type", "text/xml");

      StringBuffer returnBuff = new StringBuffer(1000);

      try (OutputStream os = conn.getOutputStream()) {
        Writer writer = new OutputStreamWriter(os);

        writer.write(xml);

        writer.flush();
        writer.close();

        try (BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
          String inputLine;
          while ((inputLine = in.readLine()) != null) {
            returnBuff.append(inputLine);
          }
        } catch (Exception e) {
//          throw new HiveException("Unable to read response for request: " + e.getMessage());
          LOGGER.error(e.toString());
          return null;
        }
      } catch (IOException ioe) {
        ioe.printStackTrace();
        throw new HiveException("Unable to send request.  Please check web server (" + conn.getURL() + ")");
      }

      long threadId = Thread.currentThread().getId();
      LOGGER.info("Thread # " + threadId + " is doing this task");
      long end = System.currentTimeMillis();
      LOGGER.info("POST time: " + (end - start) + " ms");

      response = returnBuff.toString();
    } catch (Exception e) {
      e.printStackTrace();
      LOGGER.error(e.getMessage());
      LOGGER.error(getSoapErrorMessage(conn));
    } finally {
      if (null != conn) {
        conn.disconnect();
      }
    }

    return response;
  }

  private String getSoapErrorMessage(HttpURLConnection connection) {
    String result = null;
    try (InputStream is = connection.getErrorStream(); BufferedReader in = new BufferedReader(new InputStreamReader(is))) {
      StringBuilder buff = new StringBuilder(1000);

      String inputLine;
      while ((inputLine = in.readLine()) != null) {
        buff.append(inputLine);
      }

      result = buff.toString();
    } catch (Exception e) {
      LOGGER.error("SoapClient.getSoapErrorMessage() error " + e);
    }
    return result;
  }

  /**
   * TEST ONLY
   **/
  protected void setOSeriesConfig(Properties properties) {
    if (null == oSeriesConfig && null != properties) {
      oSeriesConfig = properties;
    }
  }
}

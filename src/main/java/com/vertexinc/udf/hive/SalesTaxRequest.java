package com.vertexinc.udf.hive;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Map;

public class SalesTaxRequest extends VertexUDF {

  private static Document outDoc;
  
  public SalesTaxRequest(Document inDoc, Map<String, String> oseriesparms, String financialEventName) {

    setOutDoc(inDoc);

    // financial event attributes
    String documentNumber = oseriesparms.get("documentNumber");
    String returnAssistedParametersIndicator = oseriesparms.get("returnAssistedParametersIndicator");
    String returnGeneratedLineItemsIndicator = oseriesparms.get("returnGeneratedLineItemsIndicator");
    String documentDate = oseriesparms.get("documentDate");
    String transactionId = oseriesparms.get("transactionId");
    String postToJournal = oseriesparms.get("postToJournal");
    String isTaxOnlyAdjustmentIndicator = oseriesparms.get("isTaxOnlyAdjustmentIndicator");

    // currency
    String currencyIsoCurrencyName = oseriesparms.get("Currency-isoCurrencyName");
    String currencyIsoCurrencyCodeAlpha = oseriesparms.get("Currency-isoCurrencyCodeAlpha");
    String currencyIsoCurrencyCodeNum = oseriesparms.get("Currency-isoCurrencyCodeNum");

    // original currency
    String originalCurrencyIsoCurrencyName = oseriesparms.get("OriginalCurrency-isoCurrencyName");
    String originalCurrencyIsoCurrencyCodeAlpha = oseriesparms.get("OriginalCurrency-isoCurrencyCodeAlpha");
    String originalCurrencyIsoCurrencyCodeNum = oseriesparms.get("OriginalCurrency-isoCurrencyCodeNum");

    // line item attributes
    String lineItemNumber = oseriesparms.get("lineItemNumber");
    String taxDate = oseriesparms.get("taxDate");
    String isMulticomponent = oseriesparms.get("isMulticomponent");
    String deliveryTerm = oseriesparms.get("deliveryTerm");
    String locationCode = oseriesparms.get("locationCode");
    String postingDate = oseriesparms.get("postingDate");
    String costCenter = oseriesparms.get("costCenter");
    String departmentCode = oseriesparms.get("departmentCode");
    String generalLedgerAccount = oseriesparms.get("generalLedgerAccount");
    String materialCode = oseriesparms.get("materialCode");
    String projectNumber = oseriesparms.get("projectNumber");
    String usage = oseriesparms.get("usage");
    String usageClass = oseriesparms.get("usageClass");
    String vendorSKU = oseriesparms.get("vendorSKU");
    String countryOfOriginISOCode = oseriesparms.get("countryOfOriginISOCode");
    String modeOfTransport = oseriesparms.get("modeOfTransport");
    String natureOfTransaction = oseriesparms.get("natureOfTransaction");
    String intrastatCommodityCode = oseriesparms.get("intrastatCommodityCode");
    String netMassKilograms = oseriesparms.get("netMassKilograms");
    String lineItemId = oseriesparms.get("lineItemId");
    String taxIncludedIndicator = oseriesparms.get("taxIncludedIndicator");
    String transactionType = oseriesparms.get("transactionType");
    String simplificationCode = oseriesparms.get("simplificationCode");
    String titleTransfer = oseriesparms.get("titleTransfer");
    String chainTransactionPhase = oseriesparms.get("chainTransactionPhase");

    // seller
    String sellerCompany = oseriesparms.get("Seller-Company");
    String sellerDivision = oseriesparms.get("Seller-Division");
    String sellerDepartment = oseriesparms.get("Seller-Department");
    String sellerNexusIndicator = oseriesparms.get("Seller-nexusIndicator");
    String sellerNexusReasonCode = oseriesparms.get("Seller-nexusReasonCode");

    // seller dispatcher
    String sellerDispatcherCode = oseriesparms.get("Seller-Dispatcher-DispatcherCode");
    String sellerDispatcherClassCode = oseriesparms.get("Seller-Dispatcher-classCode");

    // seller dispatcher tax registration
    String sellerDispatcherTaxRegistrationIsoCountryCode = oseriesparms.get("Seller-Dispatcher-TaxRegistration-isoCountryCode");
    String sellerDispatcherTaxRegistrationMainDivision = oseriesparms.get("Seller-Dispatcher-TaxRegistration-mainDivision");
    String sellerDispatcherTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms.get("Seller-Dispatcher-TaxRegistration-hasPhysicalPresenceIndicator");

    String sellerDispatcherTaxRegistrationNumber = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-TaxRegistrationNumber");
    String sellerDispatcherNexusOverrideLocationRole = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-NexusOverride-locationRole");
    String sellerDispatcherNexusOverrideCountry = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-NexusOverride-country");
    String sellerDispatcherNexusOverrideMainDivision = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-NexusOverride-mainDivision");
    String sellerDispatcherNexusOverrideSubDivision = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-NexusOverride-subDivision");
    String sellerDispatcherNexusOverrideCity = oseriesparms.get("Seller-Dispatcher-TaxRegistration-NexusOverride-city");
    String sellerDispatcherNexusOverrideDistrict = oseriesparms
            .get("Seller-Dispatcher-TaxRegistration-NexusOverride-district");

    // seller dispatcher physical location
    String sellerDispatcherPhysicalLocationTaxAreaId = oseriesparms.get("Seller-Dispatcher-PhysicalLocation-taxAreaId");
    String sellerDispatcherPhysicalLocationStreetAddress1 = oseriesparms
            .get("Seller-Dispatcher-PhysicalLocation-StreetAddress1");
    String sellerDispatcherPhysicalLocationStreetAddress2 = oseriesparms
            .get("Seller-Dispatcher-PhysicalLocation-StreetAddress2");
    String sellerDispatcherPhysicalLocationCity = oseriesparms.get("Seller-Dispatcher-PhysicalLocation-City");
    String sellerDispatcherPhysicalLocationMainDivision = oseriesparms
            .get("Seller-Dispatcher-PhysicalLocation-MainDivision");
    String sellerDispatcherPhysicalLocationSubDivision = oseriesparms
            .get("Seller-Dispatcher-PhysicalLocation-SubDivision");
    String sellerDispatcherPhysicalLocationPostalCode = oseriesparms
            .get("Seller-Dispatcher-PhysicalLocation-PostalCode");
    String sellerDispatcherPhysicalLocationCountry = oseriesparms.get("Seller-Dispatcher-PhysicalLocation-Country");

    // seller physical origin
    String sellerPhysicalOriginTaxAreaId = oseriesparms.get("Seller-PhysicalOrigin-taxAreaId");
    String sellerPhysicalOriginLocationCustomsStatus = oseriesparms
            .get("Seller-PhysicalOrigin-locationCustomsStatus");
    String sellerPhysicalOriginLocationCode = oseriesparms.get("Seller-PhysicalOrigin-locationCode");
    String sellerPhysicalOriginExternalJurisdictionCode = oseriesparms
            .get("Seller-PhysicalOrigin-externalJurisdictionCode");
    String sellerPhysicalOriginStreetAddress1 = oseriesparms.get("Seller-PhysicalOrigin-StreetAddress1");
    String sellerPhysicalOriginStreetAddress2 = oseriesparms.get("Seller-PhysicalOrigin-StreetAddress2");
    String sellerPhysicalOriginCity = oseriesparms.get("Seller-PhysicalOrigin-City");
    String sellerPhysicalOriginMainDivision = oseriesparms.get("Seller-PhysicalOrigin-MainDivision");
    String sellerPhysicalOriginSubDivision = oseriesparms.get("Seller-PhysicalOrigin-SubDivision");
    String sellerPhysicalOriginPostalCode = oseriesparms.get("Seller-PhysicalOrigin-PostalCode");
    String sellerPhysicalOriginCountry = oseriesparms.get("Seller-PhysicalOrigin-Country");
    String sellerPhysicalOriginCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Seller-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeName");
    String sellerPhysicalOriginCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Seller-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeAlpha");
    String sellerPhysicalOriginCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Seller-PhysicalOrigin-CurrencyConversion-isoCurrencyCodeNum");

    // seller administrative origin
    String sellerAdministrativeOriginTaxAreaId = oseriesparms.get("Seller-AdministrativeOrigin-taxAreaId");
    String sellerAdministrativeOriginLocationCustomsStatus = oseriesparms
            .get("Seller-AdministrativeOrigin-locationCustomsStatus");
    String sellerAdministrativeOriginLocationCode = oseriesparms.get("Seller-AdministrativeOrigin-locationCode");
    String sellerAdministrativeOriginExternalJurisdictionCode = oseriesparms
            .get("Seller-AdministrativeOrigin-externalJurisdictionCode");
    String sellerAdministrativeOriginStreetAddress1 = oseriesparms
            .get("Seller-AdministrativeOrigin-StreetAddress1");
    String sellerAdministrativeOriginStreetAddress2 = oseriesparms
            .get("Seller-AdministrativeOrigin-StreetAddress2");
    String sellerAdministrativeOriginCity = oseriesparms.get("Seller-AdministrativeOrigin-City");
    String sellerAdministrativeOriginMainDivision = oseriesparms.get("Seller-AdministrativeOrigin-MainDivision");
    String sellerAdministrativeOriginSubDivision = oseriesparms.get("Seller-AdministrativeOrigin-SubDivision");
    String sellerAdministrativeOriginPostalCode = oseriesparms.get("Seller-AdministrativeOrigin-PostalCode");
    String sellerAdministrativeOriginCountry = oseriesparms.get("Seller-AdministrativeOrigin-Country");
    String sellerAdministrativeOriginCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Seller-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeName");
    String sellerAdministrativeOriginCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Seller-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeAlpha");
    String sellerAdministrativeOriginCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Seller-AdministrativeOrigin-CurrencyConversion-isoCurrencyCodeNum");

    String sellerTaxRegistrationIsoCountryCode = oseriesparms.get("Seller-TaxRegistration-isoCountryCode");
    String sellerTaxRegistrationMainDivision = oseriesparms.get("Seller-TaxRegistration-mainDivision");
    String sellerTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms
            .get("Seller-TaxRegistration-hasPhysicalPresenceIndicator");
    String sellerTaxRegistrationTaxRegistrationNumber = oseriesparms
            .get("Seller-TaxRegistration-TaxRegistrationNumber");
    String sellerTaxRegistrationNexusOverrideLocationRole = oseriesparms
            .get("Seller-TaxRegistration-NexusOverride-locationRole");
    String sellerTaxRegistrationNexusOverrideCountry = oseriesparms
            .get("Seller-TaxRegistration-NexusOverride-country");
    String sellerTaxRegistrationNexusOverrideMainDivision = oseriesparms
            .get("Seller-TaxRegistration-NexusOverride-mainDivision");
    String sellerTaxRegistrationNexusOverrideSubDivision = oseriesparms
            .get("Seller-TaxRegistration-NexusOverride-subDivision");
    String sellerTaxRegistrationNexusOverrideCity = oseriesparms.get("Seller-TaxRegistration-NexusOverride-city");
    String sellerTaxRegistrationNexusOverrideDistrict = oseriesparms
            .get("Seller-TaxRegistration-NexusOverride-district");
    String sellerTaxRegistrationTaxAreaId = oseriesparms.get("Seller-TaxRegistration-PhysicalLocation-taxAreaId");
    String sellerTaxRegistrationPhysicalLocationStreetAddress1 = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-StreetAddress1");
    String sellerTaxRegistrationPhysicalLocationStreetAddress2 = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-StreetAddress2");
    String sellerTaxRegistrationPhysicalLocationCity = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-City");
    String sellerTaxRegistrationPhysicalLocationMainDivision = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-MainDivision");
    String sellerTaxRegistrationPhysicalLocationSubDivision = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-SubDivision");
    String sellerTaxRegistrationPhysicalLocationPostalCode = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-PostalCode");
    String sellerTaxRegistrationPhysicalLocationCountry = oseriesparms
            .get("Seller-TaxRegistration-PhysicalLocation-Country");

    // customer
    String customerCustomerCode = oseriesparms.get("Customer-CustomerCode");
    String customerCustomerCodeClassCode = oseriesparms.get("Customer-CustomerCode-classCode");
    String customerCustomerCodeIsBusinessIndicator = oseriesparms.get("Customer-CustomerCode-isBusinessIndicator");
    String customerIsTaxExempt = oseriesparms.get("Customer-isTaxExempt");
    String customerExemptionReasonCode = oseriesparms.get("Customer-exemptionReasonCode");

    //customer destination
    String customerDestinationTaxAreaId = oseriesparms.get("Customer-Destination-taxAreaId");
    String customerDestinationLocationCustomsStatus = oseriesparms.get("Customer-Destination-locationCustomsStatus");
    String customerDestinationLocationCode = oseriesparms.get("Customer-Destination-locationCode");
    String customerDestinationExternalJurisdictionCode = oseriesparms
            .get("Customer-Destination-externalJurisdictionCode");
    String customerDestinationStreetAddress2 = oseriesparms.get("Customer-Destination-StreetAddress1");
    String customerDestinationStreetAddress1 = oseriesparms.get("Customer-Destination-StreetAddress2");
    String customerDestinationCity = oseriesparms.get("Customer-Destination-City");
    String customerDestinationMainDivision = oseriesparms.get("Customer-Destination-MainDivision");
    String customerDestinationSubDivision = oseriesparms.get("Customer-Destination-SubDivision");
    String customerDestinationPostalCode = oseriesparms.get("Customer-Destination-PostalCode");
    String customerDestinationCountry = oseriesparms.get("Customer-Destination-Country");
    String customerDestinationCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Customer-Destination-CurrencyConversion-isoCurrencyCodeName");
    String customerDestinationCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Customer-Destination-CurrencyConversion-isoCurrencyCodeAlpha");
    String customerDestinationCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Customer-Destination-CurrencyConversion-isoCurrencyCodeNum");

    // customer administrative destination
    String customerAdministrativeDestinationTaxAreaId = oseriesparms.get("Customer-AdministrativeDestination-taxAreaId");
    String customerAdministrativeDestinationLocationCustomsStatus = oseriesparms
            .get("Customer-AdministrativeDestination-locationCustomsStatus");
    String customerAdministrativeDestinationLocationCode = oseriesparms
            .get("Customer-AdministrativeDestination-locationCode");
    String customerAdministrativeDestinationExternalJurisdictionCode = oseriesparms
            .get("Customer-AdministrativeDestination-externalJurisdictionCode");
    String customerAdministrativeDestinationStreetAddress2 = oseriesparms
            .get("Customer-AdministrativeDestination-StreetAddress1");
    String customerAdministrativeDestinationStreetAddress1 = oseriesparms
            .get("Customer-AdministrativeDestination-StreetAddress2");
    String customerAdministrativeDestinationCity = oseriesparms.get("Customer-AdministrativeDestination-City");
    String customerAdministrativeDestinationMainDivision = oseriesparms
            .get("Customer-AdministrativeDestination-MainDivision");
    String customerAdministrativeDestinationSubDivision = oseriesparms
            .get("Customer-AdministrativeDestination-SubDivision");
    String customerAdministrativeDestinationPostalCode = oseriesparms
            .get("Customer-AdministrativeDestination-PostalCode");
    String customerAdministrativeDestinationCountry = oseriesparms.get("Customer-AdministrativeDestination-Country");
    String customerAdministrativeDestinationCurrencyConversionIsoCurrencyName = oseriesparms
            .get("Customer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeName");
    String customerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeAlpha = oseriesparms
            .get("Customer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeAlpha");
    String customerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeNum = oseriesparms
            .get("Customer-AdministrativeDestination-CurrencyConversion-isoCurrencyCodeNum");

    // customer exemption certificate
    String customerExemptionCertificateNumber = oseriesparms
            .get("Customer-ExemptionCertificate-exemptionCertificateNumber");

    // customer tax registration
    String customerTaxRegistrationIsoCountryCode = oseriesparms.get("Customer-TaxRegistration-isoCountryCode");
    String customerTaxRegistrationMainDivision = oseriesparms.get("Customer-TaxRegistration-mainDivision");
    String customerTaxRegistrationHasPhysicalPresenceIndicator = oseriesparms
            .get("Customer-TaxRegistration-hasPhysicalPresenceIndicator");
    String customerTaxRegistrationTaxRegistrationNumber = oseriesparms
            .get("Customer-TaxRegistration-TaxRegistrationNumber");

    String customerTaxRegistrationNexusOverrideLocationRole = oseriesparms
            .get("Customer-TaxRegistration-NexusOverride-locationRole");
    String customerTaxRegistrationNexusOverrideCountry = oseriesparms
            .get("Customer-TaxRegistration-NexusOverride-country");
    String customerTaxRegistrationNexusOverrideMainDivision = oseriesparms
            .get("Customer-TaxRegistration-NexusOverride-mainDivision");
    String customerTaxRegistrationNexusOverrideSubDivision = oseriesparms
            .get("Customer-TaxRegistration-NexusOverride-subDivision");
    String customerTaxRegistrationNexusOverrideCity = oseriesparms.get("Customer-TaxRegistration-NexusOverride-city");
    String customerTaxRegistrationNexusOverrideDistrict = oseriesparms
            .get("Customer-TaxRegistration-NexusOverride-district");

    String customerTaxRegistrationTaxAreaId = oseriesparms.get("Customer-TaxRegistration-PhysicalLocation-taxAreaId");
    String customerTaxRegistrationPhysicalLocationStreetAddress1 = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-StreetAddress1");
    String customerTaxRegistrationPhysicalLocationStreetAddress2 = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-StreetAddress2");
    String customerTaxRegistrationPhysicalLocationCity = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-City");
    String customerTaxRegistrationPhysicalLocationMainDivision = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-MainDivision");
    String customerTaxRegistrationPhysicalLocationSubDivision = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-SubDivision");
    String customerTaxRegistrationPhysicalLocationPostalCode = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-PostalCode");
    String customerTaxRegistrationPhysicalLocationCountry = oseriesparms
            .get("Customer-TaxRegistration-PhysicalLocation-Country");


    // tax override
    String taxOverrideOverrideType = oseriesparms.get("TaxOverride-overrideType");
    String taxOverrideOverrideReasonCode = oseriesparms.get("TaxOverride-overrideReasonCode");

    // imposition to process
    String impositionToProcessImpositionType = oseriesparms.get("ImpositionToProcess-impositionType");

    // jurisdiction override
    String jurisdictionOverrideJurisdictionLevel = oseriesparms.get("JurisdictionOverride-jurisdictionLevel");
    String jurisdictionOverrideImpositionType = oseriesparms.get("JurisdictionOverride-impositionType");

    // jurisdiction override - deduction override - exempt override
    String jurisdictionOverrideDeductionOverrideExemptOverride = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyName = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyName");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeAlpha = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyCodeAlpha");
    String jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeNum = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-isoCurrencyCodeNum");
    String jurisdictionOverrideDeductionOverrideExemptOverrideOverrideExemptReasonCode = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-ExemptOverride-overrideExemptReasonCode");

    // jurisdiction override - deduction override - non taxable override
    String jurisdictionOverrideDeductionOverrideNonTaxableOverride = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyName = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyName");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeAlpha = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyCodeAlpha");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeNum = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-isoCurrencyCodeNum");
    String jurisdictionOverrideDeductionOverrideNonTaxableOverrideOverrideExemptReasonCode = oseriesparms
            .get("JurisdictionOverride-DeductionOverride-NonTaxableOverride-overrideNonTaxableReasonCode");

    // jurisdiction override - rate override
    String jurisdictionOverrideRateOverride = oseriesparms.get("JurisdictionOverride-RateOverride");

    // situs override
    String situsOverride = oseriesparms.get("SitusOverride");
    String situsOverrideTaxingLocation = oseriesparms.get("SitusOverride-taxingLocation");

    // purchase
    String product = oseriesparms.get("Product");
    String productProductClass = oseriesparms.get("Product-productClass");

    // commodity code
    String commodityCode = oseriesparms.get("CommodityCode");
    String commodityCodeCommodityCodeType = oseriesparms.get("CommodityCode-commodityCodeType");

    // quantity
    String quantity = oseriesparms.get("Quantity");
    String quantityUnitOfMeasure = oseriesparms.get("Quantity-unitOfMeasure");

    // weight
    String weight = oseriesparms.get("Weight");
    String weightUnitOfMeasure = oseriesparms.get("Weight-unitOfMeasure");

    // volume
    String volume = oseriesparms.get("Volume");
    String volumeUnitOfMeasure = oseriesparms.get("Volume-unitOfMeasure");

    // freight
    String freight = oseriesparms.get("Freight");
    String freightIsoCurrencyName = oseriesparms.get("Freight-isoCurrencyName");
    String freightIsoCurrencyCodeAlpha = oseriesparms.get("Freight-isoCurrencyCodeAlpha");
    String freightIsoCurrencyCodeNum = oseriesparms.get("Freight-isoCurrencyCodeNum");

    // fair market value
    String fairMarketValue = oseriesparms.get("FairMarketValue");
    String fairMarketValueIsoCurrencyName = oseriesparms.get("FairMarketValue-isoCurrencyName");
    String fairMarketValueIsoCurrencyCodeAlpha = oseriesparms.get("FairMarketValue-isoCurrencyCodeAlpha");
    String fairMarketValueIsoCurrencyCodeNum = oseriesparms.get("FairMarketValue-isoCurrencyCodeNum");

    // unit price
    String cost = oseriesparms.get("Cost");

    // unit price
    String unitPrice = oseriesparms.get("UnitPrice");
    String unitPriceIsoCurrencyName = oseriesparms.get("UnitPrice-isoCurrencyName");
    String unitPriceIsoCurrencyCodeAlpha = oseriesparms.get("UnitPrice-isoCurrencyCodeAlpha");
    String unitPriceIsoCurrencyCodeNum = oseriesparms.get("UnitPrice-isoCurrencyCodeNum");

    // extended price
    String extendedPrice = oseriesparms.get("ExtendedPrice");
    String extendedPriceIsoCurrencyName = oseriesparms.get("ExtendedPrice-isoCurrencyName");
    String extendedPriceIsoCurrencyCodeAlpha = oseriesparms.get("ExtendedPrice-isoCurrencyCodeAlpha");
    String extendedPriceIsoCurrencyCodeNum = oseriesparms.get("ExtendedPrice-isoCurrencyCodeNum");

    // landed cost
    String landedCost = oseriesparms.get("LandedCost");
    String landedCostIsoCurrencyName = oseriesparms.get("LandedCost-isoCurrencyName");
    String landedCostIsoCurrencyCodeAlpha = oseriesparms.get("LandedCost-isoCurrencyCodeAlpha");
    String landedCostIsoCurrencyCodeNum = oseriesparms.get("LandedCost-isoCurrencyCodeNum");

    // discount
    String discountUserDefinedDiscountCode = oseriesparms.get("Discount-userDefinedDiscountCode");
    //discount percentage
    String discountDiscountPercent = oseriesparms.get("Discount-DiscountPercent");
    // discount amount
    String discountDiscountAmount = oseriesparms.get("Discount-DiscountAmount");
    String discountDiscountAmountIsoCurrencyName = oseriesparms.get("Discount-DiscountAmount-isoCurrencyName");
    String discountDiscountAmountIsoCurrencyCodeAlpha = oseriesparms.get("Discount-DiscountAmount-isoCurrencyCodeAlpha");
    String discountDiscountAmountIsoCurrencyCodeNum = oseriesparms.get("Discount-DiscountAmount-isoCurrencyCodeNum");

    // amount billed to date
    String amountBilledToDate = oseriesparms.get("AmountBilledToDate");
    String amountBilledToDateIsoCurrencyName = oseriesparms.get("AmountBilledToDate-isoCurrencyName");
    String amountBilledToDateIsoCurrencyCodeAlpha = oseriesparms.get("AmountBilledToDate-isoCurrencyCodeAlpha");
    String amountBilledToDateIsoCurrencyCodeNum = oseriesparms.get("AmountBilledToDate-isoCurrencyCodeNum");

    // flexible fields - flexible code
    String flexibleFieldsFlexibleCode1 = oseriesparms.get("FlexibleFields-FlexibleCodeField1");
    String flexibleFieldsFlexibleCode2 = oseriesparms.get("FlexibleFields-FlexibleCodeField2");
    String flexibleFieldsFlexibleCode3 = oseriesparms.get("FlexibleFields-FlexibleCodeField3");
    String flexibleFieldsFlexibleCode4 = oseriesparms.get("FlexibleFields-FlexibleCodeField4");
    String flexibleFieldsFlexibleCode5 = oseriesparms.get("FlexibleFields-FlexibleCodeField5");
    String flexibleFieldsFlexibleCode6 = oseriesparms.get("FlexibleFields-FlexibleCodeField6");
    String flexibleFieldsFlexibleCode7 = oseriesparms.get("FlexibleFields-FlexibleCodeField7");
    String flexibleFieldsFlexibleCode8 = oseriesparms.get("FlexibleFields-FlexibleCodeField8");
    String flexibleFieldsFlexibleCode9 = oseriesparms.get("FlexibleFields-FlexibleCodeField9");
    String flexibleFieldsFlexibleCode10 = oseriesparms.get("FlexibleFields-FlexibleCodeField10");
    String flexibleFieldsFlexibleCode11 = oseriesparms.get("FlexibleFields-FlexibleCodeField11");
    String flexibleFieldsFlexibleCode12 = oseriesparms.get("FlexibleFields-FlexibleCodeField12");
    String flexibleFieldsFlexibleCode13 = oseriesparms.get("FlexibleFields-FlexibleCodeField13");
    String flexibleFieldsFlexibleCode14 = oseriesparms.get("FlexibleFields-FlexibleCodeField14");
    String flexibleFieldsFlexibleCode15 = oseriesparms.get("FlexibleFields-FlexibleCodeField15");
    String flexibleFieldsFlexibleCode16 = oseriesparms.get("FlexibleFields-FlexibleCodeField16");
    String flexibleFieldsFlexibleCode17 = oseriesparms.get("FlexibleFields-FlexibleCodeField17");
    String flexibleFieldsFlexibleCode18 = oseriesparms.get("FlexibleFields-FlexibleCodeField18");
    String flexibleFieldsFlexibleCode19 = oseriesparms.get("FlexibleFields-FlexibleCodeField19");
    String flexibleFieldsFlexibleCode20 = oseriesparms.get("FlexibleFields-FlexibleCodeField20");
    String flexibleFieldsFlexibleCode21 = oseriesparms.get("FlexibleFields-FlexibleCodeField21");
    String flexibleFieldsFlexibleCode22 = oseriesparms.get("FlexibleFields-FlexibleCodeField22");
    String flexibleFieldsFlexibleCode23 = oseriesparms.get("FlexibleFields-FlexibleCodeField23");
    String flexibleFieldsFlexibleCode24 = oseriesparms.get("FlexibleFields-FlexibleCodeField24");
    String flexibleFieldsFlexibleCode25 = oseriesparms.get("FlexibleFields-FlexibleCodeField25");

    // flexible fields - flexible numeric
    String flexibleFieldsFlexibleNumeric1 = oseriesparms.get("FlexibleFields-FlexibleNumericField1");
    String flexibleFieldsFlexibleNumeric2 = oseriesparms.get("FlexibleFields-FlexibleNumericField2");
    String flexibleFieldsFlexibleNumeric3 = oseriesparms.get("FlexibleFields-FlexibleNumericField3");
    String flexibleFieldsFlexibleNumeric4 = oseriesparms.get("FlexibleFields-FlexibleNumericField4");
    String flexibleFieldsFlexibleNumeric5 = oseriesparms.get("FlexibleFields-FlexibleNumericField5");

    // flexible fields - flexible date
    String flexibleFieldsFlexibleDate1 = oseriesparms.get("FlexibleFields-FlexibleDateField1");
    String flexibleFieldsFlexibleDate2 = oseriesparms.get("FlexibleFields-FlexibleDateField2");
    String flexibleFieldsFlexibleDate3 = oseriesparms.get("FlexibleFields-FlexibleDateField3");
    String flexibleFieldsFlexibleDate4 = oseriesparms.get("FlexibleFields-FlexibleDateField4");
    String flexibleFieldsFlexibleDate5 = oseriesparms.get("FlexibleFields-FlexibleDateField5");

    // set defaults
    if (isNullOrEmpty(transactionType)) {
      transactionType = "SALE";
    }

    Element financialEventElement = (Element) outDoc.getElementsByTagName(financialEventName).item(0);
    // transaction type
    addAttributeToElement(financialEventElement, "transactionType", transactionType);
    addAttributeToElement(financialEventElement, "documentNumber", documentNumber);
    addAttributeToElement(financialEventElement, "returnAssistedParametersIndicator",
            returnAssistedParametersIndicator);
    addAttributeToElement(financialEventElement, "returnGeneratedLineItemsIndicator",
            returnGeneratedLineItemsIndicator);
    addAttributeToElement(financialEventElement, "documentDate", documentDate);
    addAttributeToElement(financialEventElement, "transactionId", transactionId);

    // Currency
    if (!isNullOrEmpty(currencyIsoCurrencyName) || !isNullOrEmpty(currencyIsoCurrencyCodeAlpha)
            || !isNullOrEmpty(currencyIsoCurrencyCodeNum)) {

      Element currencyCodeElement = createStubElement(financialEventElement, "Currency");
      addAttributeToElement(currencyCodeElement, "isoCurrencyName", currencyIsoCurrencyName);
      addAttributeToElement(currencyCodeElement, "isoCurrencyCodeAlpha", currencyIsoCurrencyCodeAlpha);
      addAttributeToElement(currencyCodeElement, "isoCurrencyCodeNum", currencyIsoCurrencyCodeNum);
    }
    // Original Currency
    if (!isNullOrEmpty(originalCurrencyIsoCurrencyName)
            || !isNullOrEmpty(originalCurrencyIsoCurrencyCodeAlpha)
            || !isNullOrEmpty(originalCurrencyIsoCurrencyCodeNum)) {

      Element originalCurrencyCodeElement = createStubElement(financialEventElement, "OriginalCurrency");
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyName", originalCurrencyIsoCurrencyName);
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyCodeAlpha",
              originalCurrencyIsoCurrencyCodeAlpha);
      addAttributeToElement(originalCurrencyCodeElement, "isoCurrencyCodeNum",
              originalCurrencyIsoCurrencyCodeNum);
    }
    // Line Item Element
    Element lineItemElement = createStubElement(financialEventElement, "LineItem");

    // Line Item Delivery Term
    addAttributeToElement(lineItemElement, "lineItemNumber", lineItemNumber);
    addAttributeToElement(lineItemElement, "taxDate", taxDate);
    addAttributeToElement(lineItemElement, "isMulticomponent", isMulticomponent);
    addAttributeToElement(lineItemElement, "deliveryTerm", deliveryTerm);
    addAttributeToElement(lineItemElement, "locationCode", locationCode);
    addAttributeToElement(lineItemElement, "postingDate", postingDate);
    addAttributeToElement(lineItemElement, "costCenter", costCenter);
    addAttributeToElement(lineItemElement, "departmentCode", departmentCode);
    addAttributeToElement(lineItemElement, "generalLedgerAccount", generalLedgerAccount);
    addAttributeToElement(lineItemElement, "materialCode", materialCode);
    addAttributeToElement(lineItemElement, "projectNumber", projectNumber);
    addAttributeToElement(lineItemElement, "usage", usage);
    addAttributeToElement(lineItemElement, "usageClass", usageClass);
    addAttributeToElement(lineItemElement, "vendorSKU", vendorSKU);
    addAttributeToElement(lineItemElement, "countryOfOriginISOCode", countryOfOriginISOCode);
    addAttributeToElement(lineItemElement, "modeOfTransport", modeOfTransport);
    addAttributeToElement(lineItemElement, "natureOfTransaction", natureOfTransaction);
    addAttributeToElement(lineItemElement, "intrastatCommodityCode", intrastatCommodityCode);
    addAttributeToElement(lineItemElement, "netMassKilograms", netMassKilograms);
    addAttributeToElement(lineItemElement, "lineItemId", lineItemId);
    addAttributeToElement(lineItemElement, "taxIncludedIndicator", taxIncludedIndicator);
    addAttributeToElement(lineItemElement, "transactionType", transactionType);
    addAttributeToElement(lineItemElement, "simplificationCode", simplificationCode);
    //addAttributeToElement(lineItemElement, "recoverableDate", recoverableDate);
    //addAttributeToElement(lineItemElement, "recoverableOverridePercent", recoverableOverridePercent);
    addAttributeToElement(lineItemElement, "titleTransfer", titleTransfer);
    addAttributeToElement(lineItemElement, "chainTransactionPhase", chainTransactionPhase);

    // Line Item Seller
    Element lineItemSellerElement = createStubElement(lineItemElement, "Seller");

    // Line Item Seller Company/Division/Department
    Element lineItemSellerCompanyElement = addElementToDoc(lineItemSellerElement, "Company", sellerCompany);
    Element lineItemSellerDivisionElement = addElementToDoc(lineItemSellerElement, "Division", sellerDivision);
    Element lineItemSellerDepartmentElement = addElementToDoc(lineItemSellerElement, "Department", sellerDepartment);
    addAttributeToElement(lineItemSellerElement, "nexusIndicator", sellerNexusIndicator);
    addAttributeToElement(lineItemSellerElement, "nexusReasonCode", sellerNexusReasonCode);

    // Line Item Seller Dispatcher
    Element lineItemSellerDispatcher = createStubElement(lineItemSellerElement, "Dispatcher");
    addAttributeToElement(lineItemSellerDispatcher, "classCode", sellerDispatcherClassCode);

    Element lineItemSellerDispatcherDispatcherCode = addElementToDoc(lineItemSellerDispatcher, "DispatcherCode",
            sellerDispatcherCode);

    // seller tax registration
    Element lineItemSellerDispatcherTaxRegistration = createStubElement(lineItemSellerDispatcher, "TaxRegistration");
    Element lineItemSellerDispatcherTaxRegistrationTaxRegistrationNumber = addElementToDoc(
            lineItemSellerDispatcherTaxRegistration, "TaxRegistrationNumber", sellerDispatcherTaxRegistrationNumber);
    Element lineItemSellerDispatcherTaxRegistrationNexusOverride = createStubElement(
            lineItemSellerDispatcherTaxRegistration, "NexusOverride");
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "locationRole",
            sellerDispatcherNexusOverrideLocationRole);
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "country",
            sellerDispatcherNexusOverrideCountry);
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "mainDivision",
            sellerDispatcherNexusOverrideMainDivision);
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "subDivision",
            sellerDispatcherNexusOverrideSubDivision);
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "city",
            sellerDispatcherNexusOverrideCity);
    addAttributeToElement(lineItemSellerDispatcherTaxRegistrationNexusOverride, "district",
            sellerDispatcherNexusOverrideDistrict);

    Element lineItemSellerDispatcherPhysicalLocation = createStubElement(lineItemSellerDispatcher, "PhysicalLocation");
    addAttributeToElement(lineItemSellerDispatcherPhysicalLocation, "taxAreaId",
            sellerDispatcherPhysicalLocationTaxAreaId);

    Element lineItemSellerDispatcherPhysicalLocationStreetAddress1 = addElementToDoc(
            lineItemSellerDispatcherPhysicalLocation, "StreetAddress1", sellerDispatcherPhysicalLocationStreetAddress1);
    Element lineItemSellerDispatcherPhysicalLocationStreetAddress2 = addElementToDoc(
            lineItemSellerDispatcherPhysicalLocation, "StreetAddress2", sellerDispatcherPhysicalLocationStreetAddress2);
    Element lineItemSellerDispatcherPhysicalLocationCity = addElementToDoc(lineItemSellerDispatcherPhysicalLocation,
            "City", sellerDispatcherPhysicalLocationCity);
    Element lineItemSellerDispatcherPhysicalLocationMainDivision = addElementToDoc(
            lineItemSellerDispatcherPhysicalLocation, "MainDivision", sellerDispatcherPhysicalLocationMainDivision);
    Element lineItemSellerDispatcherPhysicalLocationSubDivision = addElementToDoc(
            lineItemSellerDispatcherPhysicalLocation, "SubDivision", sellerDispatcherPhysicalLocationSubDivision);
    Element lineItemSellerDispatcherPhysicalLocationPostalCode = addElementToDoc(
            lineItemSellerDispatcherPhysicalLocation, "PostalCode", sellerDispatcherPhysicalLocationPostalCode);
    Element lineItemSellerDispatcherPhysicalLocationCountry = addElementToDoc(lineItemSellerDispatcherPhysicalLocation,
            "Country", sellerDispatcherPhysicalLocationCountry);


    // Line Item Seller Physical Origin - Ship From
    if (!isNullOrEmpty(sellerPhysicalOriginMainDivision)
            || !isNullOrEmpty(sellerPhysicalOriginPostalCode)
            || !isNullOrEmpty(sellerPhysicalOriginCountry)
            || !isNullOrEmpty(sellerPhysicalOriginTaxAreaId)) {

      Element lineItemSellerPhysicalOriginElement = createStubElement(lineItemSellerElement, "PhysicalOrigin");

      addAttributeToElement(lineItemSellerPhysicalOriginElement, "taxAreaId", sellerPhysicalOriginTaxAreaId);
      addAttributeToElement(lineItemSellerPhysicalOriginElement, "locationCustomsStatus",
              sellerPhysicalOriginLocationCustomsStatus);
      addAttributeToElement(lineItemSellerPhysicalOriginElement, "locationCode",
              sellerPhysicalOriginLocationCode);
      addAttributeToElement(lineItemSellerPhysicalOriginElement, "externalJurisdictionCode",
              sellerPhysicalOriginExternalJurisdictionCode);

      Element lineItemVendorPhysicalOriginStreetAddress1Element = addElementToDoc(
              lineItemSellerPhysicalOriginElement, "StreetAddress1", sellerPhysicalOriginStreetAddress1);
      Element lineItemVendorPhysicalOriginStreetAddress2Element = addElementToDoc(
              lineItemSellerPhysicalOriginElement, "StreetAddress2", sellerPhysicalOriginStreetAddress2);
      Element lineItemVendorPhysicalOriginStreetCityElement = addElementToDoc(lineItemSellerPhysicalOriginElement,
              "City", sellerPhysicalOriginCity);
      Element lineItemVendorPhysicalOriginMainDivisionElement = addElementToDoc(
              lineItemSellerPhysicalOriginElement, "MainDivision", sellerPhysicalOriginMainDivision);
      Element lineItemVendorPhysicalOriginSubDivisionElement = addElementToDoc(
              lineItemSellerPhysicalOriginElement, "SubDivision", sellerPhysicalOriginSubDivision);
      Element lineItemVendorPhysicalOriginPostalElement = addElementToDoc(lineItemSellerPhysicalOriginElement,
              "PostalCode", sellerPhysicalOriginPostalCode);
      Element lineItemVendorPhysicalOriginCountryElement = addElementToDoc(lineItemSellerPhysicalOriginElement,
              "Country", sellerPhysicalOriginCountry);

      Element lineItemSellerPhysicalOriginCurrencyConversion = createStubElement(
              lineItemSellerPhysicalOriginElement, "CurrencyConversion");
      addAttributeToElement(lineItemSellerPhysicalOriginCurrencyConversion, "isoCurrencyName",
              sellerPhysicalOriginCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemSellerPhysicalOriginCurrencyConversion, "isoCurrencyCodeAlpha",
              sellerPhysicalOriginCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemSellerPhysicalOriginCurrencyConversion, "isoCurrencyCodeNum",
              sellerPhysicalOriginCurrencyConversionIsoCurrencyCodeNum);
    }

    // Line Item Seller Administrative Origin - Bill From
    if (!isNullOrEmpty(sellerAdministrativeOriginMainDivision)
            || !isNullOrEmpty(sellerAdministrativeOriginPostalCode)
            || !isNullOrEmpty(sellerAdministrativeOriginCountry)
            || !isNullOrEmpty(sellerAdministrativeOriginTaxAreaId)) {

      Element lineItemSellerAdministrativeOriginElement = createStubElement(lineItemSellerElement,
              "AdministrativeOrigin");

      addAttributeToElement(lineItemSellerAdministrativeOriginElement, "taxAreaId",
              sellerAdministrativeOriginTaxAreaId);
      addAttributeToElement(lineItemSellerAdministrativeOriginElement, "locationCustomsStatus",
              sellerAdministrativeOriginLocationCustomsStatus);
      addAttributeToElement(lineItemSellerAdministrativeOriginElement, "locationCode",
              sellerAdministrativeOriginLocationCode);
      addAttributeToElement(lineItemSellerAdministrativeOriginElement, "externalJurisdictionCode",
              sellerAdministrativeOriginExternalJurisdictionCode);

      Element lineItemVendorAdministrativeOriginStreetAddress1Element = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "StreetAddress1",
              sellerAdministrativeOriginStreetAddress1);
      Element lineItemVendorAdministrativeOriginStreetAddress2Element = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "StreetAddress2",
              sellerAdministrativeOriginStreetAddress2);
      Element lineItemVendorAdministrativeOriginStreetCityElement = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "City", sellerAdministrativeOriginCity);
      Element lineItemVendorAdministrativeOriginMainDivisionElement = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "MainDivision", sellerAdministrativeOriginMainDivision);
      Element lineItemVendorAdministrativeOriginSubDivisionElement = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "SubDivision", sellerAdministrativeOriginSubDivision);
      Element lineItemVendorAdministrativeOriginPostalElement = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "PostalCode", sellerAdministrativeOriginPostalCode);
      Element lineItemVendorAdministrativeOriginCountryElement = addElementToDoc(
              lineItemSellerAdministrativeOriginElement, "Country", sellerAdministrativeOriginCountry);

      Element lineItemSellerAdministrativeOriginCurrencyConversion = createStubElement(
              lineItemSellerAdministrativeOriginElement, "CurrencyConversion");
      addAttributeToElement(lineItemSellerAdministrativeOriginCurrencyConversion, "isoCurrencyName",
              sellerAdministrativeOriginCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemSellerAdministrativeOriginCurrencyConversion, "isoCurrencyCodeAlpha",
              sellerAdministrativeOriginCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemSellerAdministrativeOriginCurrencyConversion, "isoCurrencyCodeNum",
              sellerAdministrativeOriginCurrencyConversionIsoCurrencyCodeNum);
    }

    // Line Item Customer
    Element lineItemCustomerElement = createStubElement(lineItemElement, "Customer");
    addAttributeToElement(lineItemCustomerElement, "isTaxExempt", customerIsTaxExempt);
    addAttributeToElement(lineItemCustomerElement, "exemptionReasonCode", customerExemptionReasonCode);
    // Line Item Customer Code
    Element lineItemCustomerCodeElement = addElementToDoc(lineItemCustomerElement, "CustomerCode", customerCustomerCode);
    addAttributeToElement(lineItemCustomerCodeElement, "classCode", customerCustomerCodeClassCode);
    addAttributeToElement(lineItemCustomerCodeElement, "isBusinessIndicator", customerCustomerCodeIsBusinessIndicator);

    // Line Item Customer Destination - Ship To
    Element lineItemCustomerDestinationElement = createStubElement(lineItemCustomerElement, "Destination");
    addAttributeToElement(lineItemCustomerDestinationElement, "taxAreaId", customerDestinationTaxAreaId);
    addAttributeToElement(lineItemCustomerDestinationElement, "locationCustomsStatus",
            customerDestinationLocationCustomsStatus);
    addAttributeToElement(lineItemCustomerDestinationElement, "locationCode", customerDestinationLocationCode);
    addAttributeToElement(lineItemCustomerDestinationElement, "externalJurisdictionCode",
            customerDestinationExternalJurisdictionCode);

    Element lineItemDestinationStreetAddress1Element = addElementToDoc(lineItemCustomerDestinationElement,
            "StreetAddress1", customerDestinationStreetAddress1);
    Element lineItemDestinationStreetAddress2Element = addElementToDoc(lineItemCustomerDestinationElement,
            "StreetAddress2", customerDestinationStreetAddress2);
    Element lineItemDestinationStreetCityElement = addElementToDoc(lineItemCustomerDestinationElement, "City",
            customerDestinationCity);
    Element lineItemDestinationMainDivisionElement = addElementToDoc(lineItemCustomerDestinationElement,
            "MainDivision", customerDestinationMainDivision);
    Element lineItemDestinationSubDivisionElement = addElementToDoc(lineItemCustomerDestinationElement, "SubDivision",
            customerDestinationSubDivision);
    Element lineItemDestinationPostalElement = addElementToDoc(lineItemCustomerDestinationElement, "PostalCode",
            customerDestinationPostalCode);
    Element lineItemDestinationCountryElement = addElementToDoc(lineItemCustomerDestinationElement, "Country",
            customerDestinationCountry);

    Element lineItemCustomerDestinationCurrencyConversion = createStubElement(lineItemCustomerDestinationElement,
            "CurrencyConversion");
    addAttributeToElement(lineItemCustomerDestinationCurrencyConversion, "isoCurrencyName",
            customerDestinationCurrencyConversionIsoCurrencyName);
    addAttributeToElement(lineItemCustomerDestinationCurrencyConversion, "isoCurrencyCodeAlpha",
            customerDestinationCurrencyConversionIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemCustomerDestinationCurrencyConversion, "isoCurrencyCodeNum",
            customerDestinationCurrencyConversionIsoCurrencyCodeNum);

    // Line Item Customer Administrative Destination - Bill To
    if (!isNullOrEmpty(customerAdministrativeDestinationMainDivision)
            || !isNullOrEmpty(customerAdministrativeDestinationPostalCode)
            || !isNullOrEmpty(customerAdministrativeDestinationCountry)
            || !isNullOrEmpty(customerAdministrativeDestinationTaxAreaId)) {

      Element lineItemCustomerAdministrativeDestinationElement = createStubElement(lineItemCustomerElement,
              "AdministrativeDestination");

      addAttributeToElement(lineItemCustomerAdministrativeDestinationElement, "taxAreaId",
              customerAdministrativeDestinationTaxAreaId);
      addAttributeToElement(lineItemCustomerAdministrativeDestinationElement, "locationCustomsStatus",
              customerAdministrativeDestinationLocationCustomsStatus);
      addAttributeToElement(lineItemCustomerAdministrativeDestinationElement, "locationCode",
              customerAdministrativeDestinationLocationCode);
      addAttributeToElement(lineItemCustomerAdministrativeDestinationElement, "externalJurisdictionCode",
              customerAdministrativeDestinationExternalJurisdictionCode);

      Element lineItemBuyerAdministrativeDestinationStreetAddress1Element = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "StreetAddress1",
              customerAdministrativeDestinationStreetAddress1);
      Element lineItemBuyerAdministrativeDestinationStreetAddress2Element = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "StreetAddress2",
              customerAdministrativeDestinationStreetAddress2);
      Element lineItemBuyerAdministrativeDestinationStreetCityElement = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "City", customerAdministrativeDestinationCity);
      Element lineItemBuyerAdministrativeDestinationMainDivisionElement = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "MainDivision",
              customerAdministrativeDestinationMainDivision);
      Element lineItemBuyerAdministrativeDestinationSubDivisionElement = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "SubDivision",
              customerAdministrativeDestinationSubDivision);
      Element lineItemBuyerAdministrativeDestinationPostalElement = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "PostalCode",
              customerAdministrativeDestinationPostalCode);
      Element lineItemBuyerAdministrativeDestinationCountryElement = addElementToDoc(
              lineItemCustomerAdministrativeDestinationElement, "Country", customerAdministrativeDestinationCountry);

      Element lineItemCustomerAdministrativeDestinationCurrencyConversion = createStubElement(
              lineItemCustomerAdministrativeDestinationElement, "CurrencyConversion");
      addAttributeToElement(lineItemCustomerAdministrativeDestinationCurrencyConversion, "isoCurrencyName",
              customerAdministrativeDestinationCurrencyConversionIsoCurrencyName);
      addAttributeToElement(lineItemCustomerAdministrativeDestinationCurrencyConversion, "isoCurrencyCodeAlpha",
              customerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemCustomerAdministrativeDestinationCurrencyConversion, "isoCurrencyCodeNum",
              customerAdministrativeDestinationCurrencyConversionIsoCurrencyCodeNum);
    }

    //customer exemption certificate
    Element lineItemCustomerExemptionCertificateElement = createStubElement(lineItemCustomerElement, "ExemptionCertificate");
    addAttributeToElement(lineItemCustomerExemptionCertificateElement, "exemptionCertificateNumber", customerExemptionCertificateNumber);

    // Line Item Customer Tax Registration Number
    if (!isNullOrEmpty(customerTaxRegistrationTaxRegistrationNumber)
            && customerDestinationCountry.length() <= 3) {
      Element lineItemCustomerTaxRegistrationElement = createStubElement(lineItemCustomerElement, "TaxRegistration");
      if (customerDestinationCountry.length() == 3) {
        addAttributeToElement(lineItemCustomerTaxRegistrationElement, "isoCountryCode", customerDestinationCountry);
      }
      Element lineItemVendorTaxRegistrationNumberElement = addElementToDoc(lineItemCustomerTaxRegistrationElement,
              "TaxRegistrationNumber", customerTaxRegistrationTaxRegistrationNumber);
    }


    if (!isNullOrEmpty(taxOverrideOverrideType)) {
      Element lineItemTaxOverride = createStubElement(lineItemElement, "TaxOverride");
      addAttributeToElement(lineItemTaxOverride, "overrideType", taxOverrideOverrideType);
      addAttributeToElement(lineItemTaxOverride, "overrideReasonCode", taxOverrideOverrideReasonCode);
    }

    if (!isNullOrEmpty(impositionToProcessImpositionType)) {
      Element lineItemImpositionToProcess = createStubElement(lineItemElement, "ImpositionToProcess");
      addAttributeToElement(lineItemImpositionToProcess, "impositionType", impositionToProcessImpositionType);
    }

    // JURISDICTION OVERRIDE
    if (!isNullOrEmpty(jurisdictionOverrideJurisdictionLevel)) {
      Element lineItemJurisdictionOverride = createStubElement(lineItemElement, "JurisdictionOverride");
      addAttributeToElement(lineItemJurisdictionOverride, "jurisdictionLevel",
              jurisdictionOverrideJurisdictionLevel);
      addAttributeToElement(lineItemJurisdictionOverride, "impositionType", jurisdictionOverrideImpositionType);

      Element lineItemJurisdictionOverrideDeductionOverride = createStubElement(lineItemJurisdictionOverride,
              "DeductionOverride");
      Element lineItemJurisdictionOverrideDeductionOverrideExemptOverride = addElementToDoc(lineItemElement,
              "ExemptOverride", jurisdictionOverrideDeductionOverrideExemptOverride);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyName",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyName);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyCodeAlpha",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideExemptOverride, "isoCurrencyCodeNum",
              jurisdictionOverrideDeductionOverrideExemptOverrideIsoCurrencyCodeNum);

      Element lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride = addElementToDoc(lineItemElement,
              "NonTaxableOverride", jurisdictionOverrideDeductionOverrideNonTaxableOverride);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride, "isoCurrencyName",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyName);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride,
              "isoCurrencyCodeAlpha",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeAlpha);
      addAttributeToElement(lineItemJurisdictionOverrideDeductionOverrideNonTaxableOverride, "isoCurrencyCodeNum",
              jurisdictionOverrideDeductionOverrideNonTaxableOverrideIsoCurrencyCodeNum);

      if (!isNullOrEmpty(jurisdictionOverrideRateOverride)) {
        Element lineItemJurisdictionOverrideRateOverride = addElementToDoc(lineItemJurisdictionOverride,
                "RateOverride", jurisdictionOverrideRateOverride);
      }

    } // end JURISDICTION OVERRIDE

    // LINE ITEM SITUS OVERRIDE
    if (!isNullOrEmpty(situsOverrideTaxingLocation)) {
      Element lineItemSitusOverride = createStubElement(lineItemElement, "SitusOverride");
      addAttributeToElement(lineItemSitusOverride, "taxingLocation", situsOverrideTaxingLocation);
    }

    // Line Item Purchase
    Element lineItemProductElement = addElementToDoc(lineItemElement, "Product", product);
    addAttributeToElement(lineItemProductElement, "productClass", productProductClass);

    // LINE ITEM COMMODITY CODE
    Element lineItemCommodityCode = addElementToDoc(lineItemElement, "CommodityCode", commodityCode);
    addAttributeToElement(lineItemCommodityCode, "commodityCodeType", commodityCodeCommodityCodeType);

    // LINE ITEM QUANTITY
    Element lineItemQuantity = addElementToDoc(lineItemElement, "Quantity", quantity);
    addAttributeToElement(lineItemQuantity, "unitOfMeasure", quantityUnitOfMeasure);

    // LINE ITEM QUANTITY
    Element lineItemWeight = addElementToDoc(lineItemElement, "Weight", weight);
    addAttributeToElement(lineItemWeight, "unitOfMeasure", weightUnitOfMeasure);

    // LINE ITEM VOLUME
    Element lineItemVolume = addElementToDoc(lineItemElement, "Volume", volume);
    addAttributeToElement(lineItemVolume, "unitOfMeasure", volumeUnitOfMeasure);

    Element lineItemFreight = addElementToDoc(lineItemElement, "Freight", freight);
    addAttributeToElement(lineItemFreight, "isoCurrencyName", freightIsoCurrencyName);
    addAttributeToElement(lineItemFreight, "isoCurrencyCodeAlpha", freightIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemFreight, "isoCurrencyCodeNum", freightIsoCurrencyCodeNum);

    Element lineItemFairMarketValue = addElementToDoc(lineItemElement, "FairMarketValue", fairMarketValue);
    addAttributeToElement(lineItemFairMarketValue, "isoCurrencyName", fairMarketValueIsoCurrencyName);
    addAttributeToElement(lineItemFairMarketValue, "isoCurrencyCodeAlpha", fairMarketValueIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemFairMarketValue, "isoCurrencyCodeNum", fairMarketValueIsoCurrencyCodeNum);

    Element lineItemCost = addElementToDoc(lineItemElement, "Cost", cost);

    Element lineItemUnitPrice = addElementToDoc(lineItemElement, "UnitPrice", unitPrice);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyName", unitPriceIsoCurrencyName);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyCodeAlpha", unitPriceIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemUnitPrice, "isoCurrencyCodeNum", unitPriceIsoCurrencyCodeNum);

    Element lineItemExtendedPrice = addElementToDoc(lineItemElement, "ExtendedPrice", extendedPrice);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyName", extendedPriceIsoCurrencyName);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyCodeAlpha", extendedPriceIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemExtendedPrice, "isoCurrencyCodeNum", extendedPriceIsoCurrencyCodeNum);

    Element lineItemLandedCost = addElementToDoc(lineItemElement, "LandedCost", landedCost);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyName", landedCostIsoCurrencyName);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyCodeAlpha", landedCostIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemLandedCost, "isoCurrencyCodeNum", landedCostIsoCurrencyCodeNum);

    Element lineItemDiscount = createStubElement(lineItemElement, "Discount");
    addAttributeToElement(lineItemDiscount, "userDefinedDiscountCode", discountUserDefinedDiscountCode);

    Element lineItemDiscountPercent = addElementToDoc(lineItemDiscount, "DiscountPercent", discountDiscountPercent);

    Element lineItemDiscountAmount = addElementToDoc(lineItemDiscount, "DiscountAmount", discountDiscountAmount);
    addAttributeToElement(lineItemDiscountAmount, "isoCurrencyName", discountDiscountAmountIsoCurrencyName);
    addAttributeToElement(lineItemDiscountAmount, "isoCurrencyCodeAlpha", discountDiscountAmountIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemDiscountAmount, "isoCurrencyCodeNum", discountDiscountAmountIsoCurrencyCodeNum);

    Element lineItemAmountBilledToDate = addElementToDoc(lineItemElement, "AmountBilledToDate", amountBilledToDate);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyName", amountBilledToDateIsoCurrencyName);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyCodeAlpha",
            amountBilledToDateIsoCurrencyCodeAlpha);
    addAttributeToElement(lineItemAmountBilledToDate, "isoCurrencyCodeNum", amountBilledToDateIsoCurrencyCodeNum);

    //Flexfields
    Element lineItemFlexibleFields = createStubElement(lineItemElement, "FlexibleFields");
    if (!isNullOrEmpty(flexibleFieldsFlexibleCode1)) {
      Element lineItemFlexibleCodeField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode1);
      addAttributeToElement(lineItemFlexibleCodeField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode2)) {
      Element lineItemFlexibleCodeField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode2);
      addAttributeToElement(lineItemFlexibleCodeField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode3)) {
      Element lineItemFlexibleCodeField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode3);
      addAttributeToElement(lineItemFlexibleCodeField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode4)) {
      Element lineItemFlexibleCodeField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode4);
      addAttributeToElement(lineItemFlexibleCodeField4, "fieldId", "4");
    }
    if (!isNullOrEmpty(flexibleFieldsFlexibleCode5)) {
      Element lineItemFlexibleCodeField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode5);
      addAttributeToElement(lineItemFlexibleCodeField5, "fieldId", "5");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode6)) {
      Element lineItemFlexibleCodeField6 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode6);
      addAttributeToElement(lineItemFlexibleCodeField6, "fieldId", "6");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode7)) {
      Element lineItemFlexibleCodeField7 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode7);
      addAttributeToElement(lineItemFlexibleCodeField7, "fieldId", "7");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode8)) {
      Element lineItemFlexibleCodeField8 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode8);
      addAttributeToElement(lineItemFlexibleCodeField8, "fieldId", "8");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode9)) {
      Element lineItemFlexibleCodeField9 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode9);
      addAttributeToElement(lineItemFlexibleCodeField9, "fieldId", "9");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode10)) {
      Element lineItemFlexibleCodeField10 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode10);
      addAttributeToElement(lineItemFlexibleCodeField10, "fieldId", "10");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode11)) {
      Element lineItemFlexibleCodeField11 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode11);
      addAttributeToElement(lineItemFlexibleCodeField11, "fieldId", "11");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode12)) {
      Element lineItemFlexibleCodeField12 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode12);
      addAttributeToElement(lineItemFlexibleCodeField12, "fieldId", "12");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode13)) {
      Element lineItemFlexibleCodeField13 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode13);
      addAttributeToElement(lineItemFlexibleCodeField13, "fieldId", "13");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode14)) {
      Element lineItemFlexibleCodeField14 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode14);
      addAttributeToElement(lineItemFlexibleCodeField14, "fieldId", "14");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode15)) {
      Element lineItemFlexibleCodeField15 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode15);
      addAttributeToElement(lineItemFlexibleCodeField15, "fieldId", "15");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode16)) {
      Element lineItemFlexibleCodeField16 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode16);
      addAttributeToElement(lineItemFlexibleCodeField16, "fieldId", "16");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode17)) {
      Element lineItemFlexibleCodeField17 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode17);
      addAttributeToElement(lineItemFlexibleCodeField17, "fieldId", "17");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode18)) {
      Element lineItemFlexibleCodeField18 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode18);
      addAttributeToElement(lineItemFlexibleCodeField18, "fieldId", "18");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode19)) {
      Element lineItemFlexibleCodeField19 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode19);
      addAttributeToElement(lineItemFlexibleCodeField19, "fieldId", "19");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode20)) {
      Element lineItemFlexibleCodeField20 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode20);
      addAttributeToElement(lineItemFlexibleCodeField20, "fieldId", "20");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode21)) {
      Element lineItemFlexibleCodeField21 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode21);
      addAttributeToElement(lineItemFlexibleCodeField21, "fieldId", "21");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode22)) {
      Element lineItemFlexibleCodeField22 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode22);
      addAttributeToElement(lineItemFlexibleCodeField22, "fieldId", "22");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode23)) {
      Element lineItemFlexibleCodeField23 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode23);
      addAttributeToElement(lineItemFlexibleCodeField23, "fieldId", "23");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode24)) {
      Element lineItemFlexibleCodeField24 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode24);
      addAttributeToElement(lineItemFlexibleCodeField24, "fieldId", "24");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleCode25)) {
      Element lineItemFlexibleCodeField25 = addElementToDoc(lineItemFlexibleFields, "FlexibleCodeField",
              flexibleFieldsFlexibleCode25);
      addAttributeToElement(lineItemFlexibleCodeField25, "fieldId", "25");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric1)) {
      Element lineItemFlexibleNumericField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric1);
      addAttributeToElement(lineItemFlexibleNumericField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric2)) {
      Element lineItemFlexibleNumericField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric2);
      addAttributeToElement(lineItemFlexibleNumericField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric3)) {
      Element lineItemFlexibleNumericField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric3);
      addAttributeToElement(lineItemFlexibleNumericField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric4)) {
      Element lineItemFlexibleNumericField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric4);
      addAttributeToElement(lineItemFlexibleNumericField4, "fieldId", "4");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleNumeric5)) {
      Element lineItemFlexibleNumericField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleNumericField",
              flexibleFieldsFlexibleNumeric5);
      addAttributeToElement(lineItemFlexibleNumericField5, "fieldId", "5");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate1)) {
      Element lineItemFlexibleDateField1 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate1);
      addAttributeToElement(lineItemFlexibleDateField1, "fieldId", "1");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate2)) {
      Element lineItemFlexibleDateField2 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate2);
      addAttributeToElement(lineItemFlexibleDateField2, "fieldId", "2");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate3)) {
      Element lineItemFlexibleDateField3 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate3);
      addAttributeToElement(lineItemFlexibleDateField3, "fieldId", "3");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate4)) {
      Element lineItemFlexibleDateField4 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate4);
      addAttributeToElement(lineItemFlexibleDateField4, "fieldId", "4");
    }

    if (!isNullOrEmpty(flexibleFieldsFlexibleDate5)) {
      Element lineItemFlexibleDateField5 = addElementToDoc(lineItemFlexibleFields, "FlexibleDateField",
              flexibleFieldsFlexibleDate5);
      addAttributeToElement(lineItemFlexibleDateField5, "fieldId", "5");
    }


    //added fields for distribute tax only
    if (financialEventName.toLowerCase().contains("distribute")) {
      //add header level attributes
      addAttributeToElement(financialEventElement, "postToJournal", postToJournal);
      addAttributeToElement(financialEventElement, "isTaxOnlyAdjustmentIndicator", isTaxOnlyAdjustmentIndicator);
    }

  }

  public Document getOutDoc() {
    return outDoc;
  }

  public void setOutDoc(Document doc) {
    outDoc = doc;
  }

  private static Element createStubElement(Element parentElement, String elementName) {
    Element element = outDoc.createElement(elementName);
    parentElement.appendChild(element);
    return element;
  }

  private static Element addElementToDoc(Element parentElement, String elementName, String elementValue) {
    Element element = outDoc.createElement(elementName);
    if (!isNullOrEmpty(elementValue)) {
      element.setTextContent(elementValue);
      parentElement.appendChild(element);
    }
    return element;
  }

  private static void addAttributeToElement(Element parentElement, String attributeName, String attributeValue) {
    if (!isNullOrEmpty(attributeValue)) {
      parentElement.setAttribute(attributeName, attributeValue);
    }
  }

}

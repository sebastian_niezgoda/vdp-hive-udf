package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Description(name = "SumCols", value = "Calculates sum for given inputs.", extended = "Example: SELECT SumCols('(1,234.56)', '12') FROM hive_table;")
public class SumCols extends VertexUDF {

  private static final ThreadLocal<NumberFormat> formatters = new ThreadLocal<NumberFormat>() {
    @Override
    protected NumberFormat initialValue() {
      return new DecimalFormat("#.#######");
    }
  };

  public Text evaluate(Text... inputs) {
    double result = 0d;
    for (Text input : inputs) {
      Object obj = new ToDouble().evaluate(input);
      if (obj != null)
        result += Double.parseDouble(obj.toString());
    }
    return new Text(formatters.get().format(result));
  }
}

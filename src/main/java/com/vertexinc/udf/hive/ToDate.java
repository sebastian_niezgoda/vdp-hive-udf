package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Description(
        name = "ToDate",
        value = "ToDate(date,format) - Convert to Hive date format",
        extended = "Example: \nSELECT toDate('1-OCT-15', 'dd-MMM-yy') FROM hive_table; ")
public class ToDate extends VertexUDF {
  //list of date formats used for for parsing if format not specified
  private static final String[] DATE_FORMAT_LIST = new String[] {
          "yy-MM-dd",			//2015-12-31, 15-12-31
          "MM/dd/yy",			//12/31/15, 12/31/2015
          "MM-dd-yy",			//12-31-15, 12-31-2015
          "dd-MMM-yy",		//31-DEC-15, 31-DEC-2015
          "MMM-dd-yy",		//DEC-31-15, DEC-31-2015
          "yyyyMMdd",			//20151231
          "MM/dd/yy hh:mm",	//12/31/15 12:34
          "MM-dd-yy hh:ss"	//12-31-15 12:34
  };

  public Text evaluate(Text dateString) {
    Text result = new Text();
    for(String dateFormat : DATE_FORMAT_LIST){
      result = evaluate(dateString, new Text(dateFormat));
      if(result != null && !result.toString().equals("")){
        return result;
      }
    }
    return null;
  }

  public Text evaluate(Text dateString, Text sourceDateFormat) {
    return evaluate(dateString,sourceDateFormat,new Text(HIVE_DATE_FORMAT));
  }

  public Text evaluate(Text dateString, Text sourceDateFormat, Text toDateFormat) {
    if (dateString == null || sourceDateFormat == null) {
      return null;
    }

    try {
      SimpleDateFormat sdf = new SimpleDateFormat(sourceDateFormat.toString());
      Date sourceDate = sdf.parse(dateString.toString());

      if (toDateFormat != null && !toDateFormat.toString().equals("")) {
        return new Text(new SimpleDateFormat(toDateFormat.toString()).format(sourceDate));
      } else {
        return new Text(new SimpleDateFormat(HIVE_DATE_FORMAT).format(sourceDate));
      }
    } catch (ParseException e) {
      return null;
    }
  }
}

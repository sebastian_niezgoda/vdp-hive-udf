package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;

public class ToDouble extends VertexUDF {
  public Text evaluate(Text doubleString){
    return new ToNumber().evaluate(doubleString, new Text("DOUBLE"));
  }
}

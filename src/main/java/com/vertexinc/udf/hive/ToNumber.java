package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

import java.text.DecimalFormat;
import java.text.NumberFormat;

@Description(
        name = "ToNumber",
        value = "ToNumber() - Convert string to number",
        extended = "Example: SELECT ToNumber('(9,388.18)', NumericType) FROM hive_table; "
)
public class ToNumber extends VertexUDF {
  private enum NumericType { TINYINT, SMALLINT, INT, BIGINT, FLOAT, DOUBLE, DECIMAL }

  private static final ThreadLocal<NumberFormat> formatters = new ThreadLocal<NumberFormat>() {
    @Override
    protected NumberFormat initialValue() {
      return new DecimalFormat("#.#######");
    }
  };

  public Text evaluate(Text doubleString, Text type){
    if(doubleString == null || type == null || doubleString.toString().equals("") || type.toString().equals("")){
      return new Text("0");
    }

    String inputString = doubleString.toString().trim();
    Boolean hasParens = inputString.contains("(") && inputString.contains(")");
    inputString = inputString.replaceAll("[()]", "").trim();
    Boolean isPercent = inputString.endsWith("%");
    Boolean isDecimal = inputString.contains(".");

    inputString = inputString.replaceAll("[,$%]", "");

    if(hasParens)
      inputString = "-" + inputString;

    NumericType numericType = NumericType.valueOf(type.toString().trim().toUpperCase());

    try{
      switch (numericType) {
        case INT:
        case SMALLINT:
        case TINYINT:
        case BIGINT:
          if(isDecimal){
            return new Text(String.valueOf(Math.round(Double.parseDouble(inputString))));
          }
          else {
            return new Text(String.valueOf(Long.parseLong(inputString)));
          }
        default:
          Double d = Double.parseDouble(inputString);
          if(isPercent)
            d = d / 100.00;
          return new Text(formatters.get().format(d));
      }
    }
    catch(Exception e){
      return new Text("0");
    }
  }
}

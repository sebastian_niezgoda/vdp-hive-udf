package com.vertexinc.udf.hive;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;

public abstract class VertexGenericUDF extends GenericUDF implements IVertexUDF {
  protected String loggedInUser;
  protected String loggedInApplication;

  protected void init() {
    setLoggedInUser();
  }

  private void setLoggedInUser() {
    if (loggedInUser == null) {
      loggedInUser = SessionState.get().getUserName();
      System.out.println("Logged in user is: " + loggedInUser);
      if (StringUtils.isNotEmpty(loggedInUser)) {
        if (loggedInUser.contains(".")) {
          String[] split = loggedInUser.split("\\.");
          if (2 == split.length) {
            System.out.println("Logged in application is: " + loggedInApplication);
            loggedInApplication = split[0].toLowerCase();
          }
        }
      }
    }
  }

  /* FOR TESTS ONLY */
  protected void setLoggedInUser(String user) {
    loggedInUser = user;
  }

  protected void setLoggedInApplication(String app) {
    loggedInApplication = app;
  }
}

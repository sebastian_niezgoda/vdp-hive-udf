package com.vertexinc.udf.hive;

import com.vertexinc.udf.hive.service.ConfigurationPropertiesService;
import com.vertexinc.udf.hive.service.SessionService;
import org.apache.hadoop.hive.ql.exec.UDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public abstract class VertexUDF extends UDF implements IVertexUDF {
  public static final Logger LOGGER = LoggerFactory.getLogger(VertexUDF.class.getName());
  // free token:
  // private static final String FIXER_API_TOKEN = "7d6b84e7c47a7c5b2d8029b4dff5156b";
  // professional token:
  private static final String FIXER_API_TOKEN = "c6a69454feaee9a7ca8865e14cd65cc1";
  private String loggedInApplication;
  private Properties udfConfig = new Properties();

  public static final String UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM = "udf.vies.max.invalid";
  public static final String UDF_MAX_HOURLY_VIES_REQ_PARAM = "udf.vies.max.hourly";
  public static final String UDF_VIES_INVALID_RESET_TIMEOUT_REQ_PARAM = "udf.vies.invalid.resetTimeout";

  protected ConfigurationPropertiesService propService = new ConfigurationPropertiesService();
  protected SessionService sessionService = new SessionService();

  public static Boolean isNullOrEmpty(String string) {
    return string == null || string.trim().length() == 0;
  }

  public void setUdfConfig(Properties properties) {
    this.udfConfig = properties;
  }

  public Properties getUdfConfig() {
    return udfConfig;
  }

  public String getLoggedInApplication() {
    return loggedInApplication;
  }

  public void setLoggedInApplication(String loggedInApplication) {
    this.loggedInApplication = loggedInApplication;
    LOGGER.info(String.format("Logged in application: %s", loggedInApplication));
  }

  public static String getFixerApiToken() {
    return FIXER_API_TOKEN;
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.io.Text;

@Description(name = "vrtxCurrentDB",
        value = "vrtxCurrentDB() - Retrieves the current database",
        extended = "Example: \nSELECT vrtxCurrentDB() FROM hive_table; "
)
public class VrtxCurrentDB extends VertexUDF {

  public Text evaluate(){
    return new Text(SessionState.get().getCurrentDatabase());
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.io.Text;

@Description(
        name = "vrtxCurrentUser",
        value = "vrtxCurrentUser() - Retrieves the current user",
        extended = "Example: \nSELECT vrtxCurrentUser() FROM hive_table; "
)
public class VrtxCurrentUser extends VertexUDF {

  public Text evaluate(){
    String sessUserFromAuth = SessionState.getUserFromAuthenticator();
    if (sessUserFromAuth != null) {
      return new Text(sessUserFromAuth);
    }
    return null;
  }
}

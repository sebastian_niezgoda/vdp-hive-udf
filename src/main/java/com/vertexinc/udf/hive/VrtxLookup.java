package com.vertexinc.udf.hive;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.exec.UDFArgumentLengthException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.session.SessionState;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDFUtils;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorConverter;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

@Description(name = "vrtxLookup", value = "vrtxLookup() - Retrieves the current user", extended = "Example: \n"
        + " SELECT vrtxLookup() FROM hive_table3; ")
public class VrtxLookup extends VertexGenericUDF {
  private static String delimiter = "|";
  private static ConcurrentHashMap<String, HashMap<String, String>> fileMap = new ConcurrentHashMap<String, HashMap<String, String>>();

  private transient GenericUDFUtils.StringHelper returnHelper;

  private transient PrimitiveObjectInspectorConverter.StringConverter[] stringConverter;

  private BufferedReader br;

  public void setBr(BufferedReader br) {
    this.br = br;
  }

  @Override
  public ObjectInspector initialize(ObjectInspector[] arguments)
          throws UDFArgumentException {
    if (arguments.length < 2) {
      throw new UDFArgumentLengthException(
              "vrtxLookup takes 2 or more arguments");
    }

    stringConverter = new PrimitiveObjectInspectorConverter.StringConverter[arguments.length];

    for (int i = 0; i < arguments.length; i++) {
      if (arguments[0].getCategory() != ObjectInspector.Category.PRIMITIVE) {
        throw new UDFArgumentException(
                "vrtxLookup only takes primitive types");
      }
      stringConverter[i] = new PrimitiveObjectInspectorConverter.StringConverter(
              (PrimitiveObjectInspector) arguments[i]);
    }

    setLoggedInUser();

    returnHelper = new GenericUDFUtils.StringHelper(
            PrimitiveObjectInspector.PrimitiveCategory.STRING);

    return PrimitiveObjectInspectorFactory.writableStringObjectInspector;

  }

  private void setLoggedInUser() {
    if (loggedInUser == null) {
      loggedInUser = SessionState.get().getUserName();
      if (loggedInUser != null) {
        int idx = loggedInUser.indexOf('.');
        loggedInApplication = idx > -1 ? loggedInUser.substring(0, idx) : null;
      }
    }
  }

  private void initMap(String f) {
    if (loggedInApplication == null)
      throw new NullPointerException(
              "Unable to retrieve application name from user.");
    String filePath = "/lookup-tables/" + loggedInApplication.toLowerCase() + "/" + f
            + ".txt";
    String line = null;
    try {
      if(this.br == null){
        FileSystem fs = FileSystem.get(new Configuration());
        FSDataInputStream in = fs.open(new Path(filePath));
        this.br = new BufferedReader(new InputStreamReader(in));
      }
      HashMap<String, String> map = new HashMap<String, String>();
      while ((line = br.readLine()) != null) {
        // ignore comment lines
        if (line.startsWith("#")) {
          continue;
        }
        String[] strs = line.split("\t");
        if (strs.length == 2) {
          map.put(strs[0].toUpperCase().trim(), strs[1].trim());
        } else if (strs.length > 2) {
          map.put(getKey(strs), strs[strs.length - 1].trim());
        }
      }
      fileMap.put(f, map);
      br.close();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  private Text getValue(String s, String f) {
    HashMap<String, String> map = fileMap.get(f);
    if (map == null) {
      initMap(f);
      map = fileMap.get(f);
    }
    String v = map.get(s);
    return v == null ? null : new Text(v);
  }

  @Override
  public Object evaluate(DeferredObject[] arguments) throws HiveException {

    //String val = (String) stringConverter[0].convert(arguments[0].get());

    String val = buildVal(arguments);

    String lookupFile = (String) stringConverter[arguments.length-1].convert(arguments[arguments.length-1].get());

    Text returnVal = getValue(val.toUpperCase(), lookupFile.toLowerCase());

    return returnVal == null ? null : returnHelper.setReturnValue(returnVal.toString());
  }

  @Override
  public String getDisplayString(String[] arg0) {
    return "lookup()";
  }

  private String buildVal(DeferredObject[] arguments) throws HiveException {
    StringBuilder builder = new StringBuilder();
    int cnt = arguments.length - 1;
    for (int i = 0; i < cnt; i++) {
      builder.append((String) stringConverter[i].convert(arguments[i].get()));
      if (i < cnt - 1) {
        builder.append(delimiter);
      }
    }
    return builder.toString();
  }

  private String getKey(String[] strs) {
    StringBuilder builder = new StringBuilder();
    int cnt = strs.length - 1;
    for (int i = 0; i < cnt; i++) {
      builder.append(strs[i].toUpperCase().trim());
      if (i < cnt - 1) {
        builder.append(delimiter);
      }
    }
    return builder.toString();
  }
}

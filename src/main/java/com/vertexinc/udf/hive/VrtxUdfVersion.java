package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.io.Text;

@Description(
        name = "vrtxUdfVersion",
        value = "vrtxUdfVersion() - Retrieves the current version",
        extended = "Example: \nSELECT vrtxUdfVersion() FROM hive_table; "
)
public class VrtxUdfVersion extends VertexUDF {

  public Text evaluate(){
    return new Text("1.1");
  }
}

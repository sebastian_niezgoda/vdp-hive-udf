package com.vertexinc.udf.hive.service;

import com.vertexinc.udf.hive.OSeriesUDF;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class ConfigurationPropertiesService {
  private static final Logger LOGGER = LoggerFactory.getLogger(OSeriesUDF.class.getName());

  public Properties loadOSeriesConfiguration(String applicationCode) {
    LOGGER.debug("Loading OSeries configuration for application " + applicationCode);
    String filePath = String.format("/ve/oseries/%s/oseries.properties", applicationCode);

    return loadProperties(applicationCode, filePath);
  }

  public Properties loadUdfThresholdConfiguration(String applicationCode){
    LOGGER.debug("Loading UDF configuration for application " + applicationCode);
    String filePath = String.format("/ve/hive/%s/udf.properties", applicationCode);

    return loadProperties(applicationCode, filePath);
  }

  private Properties loadProperties(String applicationCode, String filePath){
    Properties oSeriesConfig = new Properties();

    if (StringUtils.isEmpty(applicationCode)) {
      throw new NullPointerException("Unable to retrieve application name from user.");
    }

    LOGGER.debug("applicationCode:" + applicationCode);

    try (FileSystem fs = FileSystem.newInstance(new Configuration()); FSDataInputStream in = fs.open(new Path(filePath))) {
      // load a properties file
      oSeriesConfig.load(in);
    } catch (IOException e) {
      LOGGER.error(e.toString());
      oSeriesConfig = null;
    }

    return oSeriesConfig;
  }
}

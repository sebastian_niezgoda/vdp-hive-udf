package com.vertexinc.udf.hive.service;

import org.apache.hadoop.hive.ql.session.SessionState;

public class SessionService {
  public String getLoggedInApplication(SessionState sessionState) {
    if(sessionState == null)
      return null;
    String user = sessionState.getUserName();
    return getLoggedInApplication(user);
  }

  public String getLoggedInApplication(String user) {
    if (user == null) {
      return null;
    } else {
      int idx = user.indexOf('.');
      return user.contains(".") ? user.substring(0, idx) : null;
    }
  }
}

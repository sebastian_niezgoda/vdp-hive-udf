package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.DeferredJavaObject;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.DeferredObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class AccrualRequestIT extends BaseOSeriesIT {
  private AccrualRequest request = new AccrualRequest();

  @Before
  public void before() throws UDFArgumentException {
    request.setLoggedInUser("open.vedemo");
    request.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    request.setOSeriesConfig(getoSeriesProps());

    request.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    request.close();
  }

  @Test
  public void accrualRequestTest_1() throws HiveException {
    String company = "100";
    String division = "101";
    String destCountry = "FRANCE";
    String vendorClass = "vendorclass";
    String vendorCode = "vendorcode";
    String physOriginCountry = "AUSTRALIA";
    String deliveryTerm = "EXW";
    String purchaseCode = "purchasecode";
    String extendedPrice = "100";

    // the value exists
    Object result1 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company), new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry), new DeferredJavaObject(vendorClass),
            new DeferredJavaObject(vendorCode), new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(deliveryTerm), new DeferredJavaObject(purchaseCode), new DeferredJavaObject(extendedPrice)});

    assertTrue(result1.toString().contains("<LineItem deliveryTerm=\"EXW\"><Purchase>purchasecode</Purchase><Quantity>1.0</Quantity><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"VAT\" situs=\"DESTINATION\" inputOutputType=\"INPUT\"><Jurisdiction jurisdictionLevel=\"COUNTRY\" jurisdictionId=\"78282\">FRANCE</Jurisdiction><CalculatedTax>0.0</CalculatedTax><EffectiveRate>0.0</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"VAT\">VAT</Imposition><RecoverableAmount>0.0</RecoverableAmount><RecoverablePercent>1.0</RecoverablePercent><UnrecoverableAmount>0.0</UnrecoverableAmount><InvoiceTextCode>9</InvoiceTextCode></Taxes><TotalTax>0.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"inputTaxLocation.taxAreaId\" phase=\"PRE\">0</AssistedParameter></AssistedParameters></LineItem>"));
    assertTrue(result1.toString().indexOf("</AccrualResponse>") > 1);
  }

  @Test
  public void accrualRequestTest_2() throws HiveException {
    // arguments are null
    String company = "100";
    String division = "101";
    String destCountry = "FRANCE";
    String vendorClass = "vendorclass";
    String vendorCode = "vendorcode";
    String physOriginCountry = "AUSTRALIA";
    String deliveryTerm = "EXW";
    String purchaseCode = "purchasecode";
    String extendedPrice = null;

    Object result2 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company), new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry), new DeferredJavaObject(vendorClass),
            new DeferredJavaObject(vendorCode), new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(deliveryTerm), new DeferredJavaObject(purchaseCode), new DeferredJavaObject(extendedPrice)});

//    System.out.println("result2: " + result2);
    assertNull(result2);
  }

  @Test
  public void accrualRequestTest_3() throws HiveException {
    // arguments are null
    String company = null;
    String division = "101";
    String destCountry = "FRANCE";
    String vendorClass = "vendorclass";
    String vendorCode = "vendorcode";
    String physOriginCountry = "AUSTRALIA";
    String deliveryTerm = "EXW";
    String purchaseCode = "purchasecode";
    String extendedPrice = "23423";

    Object result3 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company), new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry), new DeferredJavaObject(vendorClass),
            new DeferredJavaObject(vendorCode), new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(deliveryTerm), new DeferredJavaObject(purchaseCode), new DeferredJavaObject(extendedPrice)});

//    System.out.println("result3: " + result3);
    assertNull(result3);
  }

  @Test
  public void accrualRequestTest_4() throws HiveException {
    // arguments are " "
    String company = null;
    String division = "101";
    String destCountry = "FRANCE";
    String vendorClass = "vendorclass";
    String vendorCode = "vendorcode";
    String physOriginCountry = "AUSTRALIA";
    String deliveryTerm = "EXW";
    String purchaseCode = "purchasecode";
    String extendedPrice = "  ";

    Object result4 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company), new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry), new DeferredJavaObject(vendorClass),
            new DeferredJavaObject(vendorCode), new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(deliveryTerm), new DeferredJavaObject(purchaseCode), new DeferredJavaObject(extendedPrice)});

//    System.out.println("result4: " + result4);
    assertNull(result4);
  }

  @Test
  public void accrualRequestTest_STE() throws HiveException, SAXException, XPathExpressionException, IOException {
    assertEquals("95.0", runSTEExample("companya,divisiona,USA,vendorclassa,vc1,GBR,CFR,PC1,1000,95.0"));
    assertEquals("28.5", runSTEExample("companya,divisiona,USA,vendorclassa,vc1,GBR,CFR,PC1,300,28.5"));
    assertEquals("38.0", runSTEExample("companya,divisiona,USA,vendorclassa,vc1,GBR,CFR,PC1,400,38.0"));
    assertEquals("66.5", runSTEExample("companya,divisiona,USA,vendorclassa,vc1,GBR,CFR,PC1,700,66.5"));
    assertEquals("104.5", runSTEExample("companyb,divisionb,USA,vendorclassb,vc2,CHN,CIF,PC2,1100,104.5"));
    assertEquals("38.0", runSTEExample("companyb,divisionb,USA,vendorclassb,vc2,CHN,CIF,PC2,400,38.0"));
    assertEquals("47.5", runSTEExample("companyb,divisionb,USA,vendorclassb,vc2,CHN,CIF,PC2,500,47.5"));
    assertEquals("76.0", runSTEExample("companyb,divisionb,USA,vendorclassb,vc2,CHN,CIF,PC2,800,76.0"));
    assertEquals("114.0", runSTEExample("companyc,divisionc,USA,vendorclassc,vc3,NLD,CIP,PC3,1200,114.0"));
    assertEquals("47.5", runSTEExample("companyc,divisionc,USA,vendorclassc,vc3,NLD,CIP,PC3,500,47.5"));
    assertEquals("57.0", runSTEExample("companyc,divisionc,USA,vendorclassc,vc3,NLD,CIP,PC3,600,57.0"));
    assertEquals("85.5", runSTEExample("companyc,divisionc,USA,vendorclassc,vc3,NLD,CIP,PC3,900,85.5"));
  }

  private String runSTEExample(String row) throws HiveException, SAXException, XPathExpressionException, IOException {
    String[] cols = row.split(",");
    return runCalcTax(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5], cols[6], cols[7],cols[8]);
  }

  private String runCalcTax(String company, String division, String destCountry, String vendorClass, String vendorCode, String physOriginCountry, String deliveryTerm, String purchaseCode, String extendedPrice) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company), new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry), new DeferredJavaObject(vendorClass),
            new DeferredJavaObject(vendorCode), new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(deliveryTerm), new DeferredJavaObject(purchaseCode), new DeferredJavaObject(extendedPrice)});
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
    return xpath.evaluate("/AccrualResponse/TotalTax", document);
  }
}
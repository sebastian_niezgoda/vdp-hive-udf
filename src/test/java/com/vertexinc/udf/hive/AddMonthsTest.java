package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddMonthsTest {

  @Test
  public void addMonths() {
    AddMonths addMonths = new AddMonths();

    Text dateString = null;

    assertNull(addMonths.evaluate(null, null));

    dateString = new Text("2018-06-18");
    assertNull(addMonths.evaluate(dateString, null));

    Text result = addMonths.evaluate(dateString, 1);
    assertEquals("2018-07-01", result.toString());

    result = addMonths.evaluate(dateString, -1);
    assertEquals("2018-05-01", result.toString());

    dateString = new Text("2018-12-31");
    result = addMonths.evaluate(dateString, -1);
    assertEquals("2018-11-01", result.toString());

    result = addMonths.evaluate(dateString, 1);
    assertEquals("2019-01-01", result.toString());

    dateString = new Text("2016-02-29");
    result = addMonths.evaluate(dateString, -12);
    assertEquals("2015-02-01", result.toString());

    result = addMonths.evaluate(dateString, 1);
    assertEquals("2016-03-01", result.toString());
  }
}
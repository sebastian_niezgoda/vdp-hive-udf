package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.DeferredJavaObject;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.DeferredObject;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.log4j.Logger;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for simple App.
 */
public class AddressCleanseIT extends BaseOSeriesIT {

  private static final Logger LOGGER = Logger.getLogger(AddressCleanseIT.class);

  private AddressCleanse request;

  @Before
  public void setUp() throws HiveException {
    // set up the models we need
    request = new AddressCleanse();
    request.setLoggedInUser("open.vedemo");
    request.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    request.setOSeriesConfig(getoSeriesProps());

    request.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector, PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector, PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void cleanUp() throws IOException {
    request.close();
  }

  @Test
  public void testHappyPath() throws HiveException {
    String streetAddress1 = "1041 Old Cassatt Rd";
    String city = "Berwyn";
    String state = "PA";
    String postalCode = "19312";

    // the value exists
    Object result1 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(streetAddress1), new DeferredJavaObject(city),
            new DeferredJavaObject(state), new DeferredJavaObject(postalCode)});

    LOGGER.info("result1: " + result1);
//    System.out.println("result1: " + result1);

    assertEquals(true, result1.toString().indexOf("</TaxAreaResult>") > 1);
  }

  @Test
  public void testErrors_1() throws HiveException {
    ExpectedException exception = ExpectedException.none();
    exception.expect(HiveException.class);
    exception.expectMessage(CoreMatchers.containsString("Unable to read response for request: Server returned HTTP response code: 500 for URL"));
    // arguments are null
    String streetAddress1 = "1041 Old Cassatt Rd";
    String city = "Berwyn";
    String state = "PA";
    String postalCode = null;

    Object result2 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(streetAddress1), new DeferredJavaObject(city),
            new DeferredJavaObject(state), new DeferredJavaObject(postalCode)});
    LOGGER.info("result2: " + result2);
  }

  /**
   * This test is the same as the happy path, not worth running.
   */
  @Ignore
  @Test
  public void testErrors_2() throws HiveException {
    // arguments are null
    String streetAddress1 = "1041 Old Cassatt Rd";
    String city = "Berwyn";
    String state = "PA";
    String postalCode = "19312";

    Object result3 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(streetAddress1), new DeferredJavaObject(city),
            new DeferredJavaObject(state), new DeferredJavaObject(postalCode)});

    LOGGER.info("result3: " + result3);
    assertEquals(true, result3.toString().indexOf("</TaxAreaResult>") > 1);

  }

  /**
   * This test is the same as the happy path, not worth running.
   */
  @Ignore
  @Test
  public void testErrors_3() throws HiveException {
    // arguments are " "
    String streetAddress1 = "1041 Old Cassatt Rd";
    String city = "Berwyn";
    String state = "PA";
    String postalCode = "19312";
    int pc = 19312;

    Object result4 = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(streetAddress1), new DeferredJavaObject(city),
            new DeferredJavaObject(state), new DeferredJavaObject(19312)});

    LOGGER.info("result4: " + result4);
    assertEquals(true, result4.toString().indexOf("</TaxAreaResult>") > 1);
  }

  @Ignore
  @Test
  public void testAddressCleanse_STE() throws HiveException, SAXException, XPathExpressionException, IOException {
    System.out.println(runAddressCleanse("328 Landsende Lane", "Devon", "PA", "19333"));
    System.out.println(runAddressCleanse("635 USC McCarthy Way", "Los Angels", "CA", "90089"));
    System.out.println(runAddressCleanse("34 So 24th Stre", "Camp Hill", "Penn", "17011"));
    System.out.println(runAddressCleanse("234 Valley Stream", "Wayne", "PA", "19088"));

  }

  private String runAddressCleanse(String streetAddress, String city, String state, String zip) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = request.evaluate(new DeferredObject[]{
            new DeferredJavaObject(streetAddress), new DeferredJavaObject(city),
            new DeferredJavaObject(state), new DeferredJavaObject(zip)});
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
//    return xpath.evaluate("/AccrualResponse/TotalTax", document);
    return result.toString();
  }
}

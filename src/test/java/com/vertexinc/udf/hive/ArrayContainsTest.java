package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.StandardListObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.io.BooleanWritable;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;

public class ArrayContainsTest {
  private ArrayContains arrayContains = new ArrayContains();

  @Before
  public void before() throws UDFArgumentException {
    ObjectInspector[] inspectors = new ObjectInspector[]{
            ObjectInspectorFactory.getStandardListObjectInspector(PrimitiveObjectInspectorFactory.javaStringObjectInspector),
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    };

    arrayContains.initialize(inspectors);
  }

  @Test
  public void arrayContains_Null1() throws HiveException {
    assertNull(arrayContains.evaluate(null));
  }

  @Test
  public void arrayContains_Null2() throws HiveException {
    assertNull(arrayContains.evaluate(new DeferredObject[]{
            new DeferredJavaObject(null),
            new DeferredJavaObject(null)
    }));
  }

  @Test
  public void arrayContains_Null3() throws HiveException {
    assertNull(arrayContains.evaluate(new DeferredObject[]{
            null,
            null
    }));
  }

  @Test
  public void arrayContains_Null4() throws HiveException {
    assertNull(arrayContains.evaluate(new DeferredObject[]{
            null,
            new DeferredJavaObject(null)
    }));
  }

  @Test
  public void arrayContains_Null5() throws HiveException {
    assertNull(arrayContains.evaluate(new DeferredObject[]{
            new DeferredJavaObject(null),
            null
    }));
  }

  @Test
  public void arrayContains_False1() throws HiveException {
    List<String> list = new ArrayList<>();
    String contains = "false1";

    assertFalse(((BooleanWritable) arrayContains.evaluate(new DeferredObject[]{
            new DeferredJavaObject(list),
            new DeferredJavaObject(contains)
    })).get());
  }

  @Test
  public void arrayContains_False2() throws HiveException {
    List<String> list = new ArrayList<>();
    String contains = "false2";
    list.add(contains);

    assertFalse(((BooleanWritable) arrayContains.evaluate(new DeferredObject[]{
            new DeferredJavaObject(list),
            new DeferredJavaObject("TEST")
    })).get());
  }

  @Test
  public void arrayContains_True1() throws HiveException {
    List<String> list = new ArrayList<>();
    String contains = "true1";
    list.add(contains);

    assertTrue(((BooleanWritable) arrayContains.evaluate(new DeferredObject[]{
            new DeferredJavaObject(list),
            new DeferredJavaObject(contains)
    })).get());
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;

public class AssetMovementRequestIT extends BaseOSeriesIT {
  private boolean debug = false;
  AssetMovementRequest amr = new AssetMovementRequest();


  @Before
  public void before() throws UDFArgumentException {
    amr.setLoggedInUser("open.vedemo");
    amr.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    amr.setOSeriesConfig(getoSeriesProps());

    amr.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    amr.close();
    amr = new AssetMovementRequest();
  }

  private DeferredObject[] createDeferredObjectArray(
          String company,
          String division,
          String destCountry,
          String physOriginCountry,
          String movementMethod,
          String purchaseCode,
          String previousTaxPaid,
          String extendedPrice
  ) {
    return new DeferredObject[]{
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry),
            new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(movementMethod),
            new DeferredJavaObject(purchaseCode),
            new DeferredJavaObject(previousTaxPaid),
            new DeferredJavaObject(extendedPrice)
    };
  }

  @Test
  public void assetMovementRequestTest_Normal() throws HiveException, IOException {
    Object result = amr.evaluate(createDeferredObjectArray(
            "100",
            "101",
            "FRANCE",
            "BELGIUM",
            "CONSIGNMENT",
            "purchasecode",
            "100",
            "1000"
    ));

    if (debug) {
      System.out.println(result);
    }
    Assert.assertEquals(true, result.toString().indexOf("</AssetMovementResponse>") > 1);
  }

  @Test
  public void assetMovementRequest_Null() throws HiveException {
    // arguments are null
    Object result = amr.evaluate(createDeferredObjectArray(
            "100",
            "101",
            "FRANCE",
            "BELGIUM",
            "CONSIGNMENT",
            "purchasecode",
            "100",
            null
    ));

    if(debug){
      System.out.println(result);
    }
    Assert.assertNull(result);
  }

  @Test
  public void assetMovementRequest_Null2() throws HiveException {
    // arguments are null
    Object result = amr.evaluate(createDeferredObjectArray(
            null,
            "101",
            "FRANCE",
            "BELGIUM",
            "CONSIGNMENT",
            "purchasecode",
            "100",
            "33"
    ));

    if(debug){
      System.out.println(result);
    }
    Assert.assertNull(result);
  }

  @Test
  public void assetMovementRequest_Empty() throws HiveException {
    // arguments are null
    Object result = amr.evaluate(createDeferredObjectArray(
            "100",
            "101",
            "FRANCE",
            "BELGIUM",
            "CONSIGNMENT",
            "purchasecode",
            "100",
            " "
    ));

    if(debug){
      System.out.println(result);
    }
    Assert.assertNull(result);
  }

  @Test
  public void accrualRequestTest_STE() throws HiveException, SAXException, XPathExpressionException, IOException {
    assertEquals("0.0", runSTEExample("10000,10001,GERMANY,AUSTRALIA,CALL_OFF,Purchase2,55.38,777,0.0"));
    assertEquals("6825.0", runSTEExample("10000,10001,UNITED STATES,THAILAND,CONSIGNMENT,Purchase1,300,75000,6825.0"));
  }

  private String runSTEExample(String row) throws HiveException, SAXException, XPathExpressionException, IOException {
    String[] cols = row.split(",");
    return runCalcTax(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5], cols[6], cols[7]);
  }

  private String runCalcTax(String company, String division, String destCountry, String physOriginCountry, String movementMethod, String purchaseCode, String previousTaxPaid, String extendedPrice) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = amr.evaluate(new DeferredObject[]{
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(destCountry),
            new DeferredJavaObject(physOriginCountry),
            new DeferredJavaObject(movementMethod),
            new DeferredJavaObject(purchaseCode),
            new DeferredJavaObject(previousTaxPaid),
            new DeferredJavaObject(extendedPrice)
    });
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
    return xpath.evaluate("/AssetMovementResponse/TotalTax", document);
  }
}

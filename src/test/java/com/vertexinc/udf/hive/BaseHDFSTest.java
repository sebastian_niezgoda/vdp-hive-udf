package com.vertexinc.udf.hive;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.ws.rs.HttpMethod;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class BaseHDFSTest extends BaseUDFIT {

  @Autowired
  @Value("${etms.system.id}")
  protected String systemIdentifier = "uninitialized";

  @Autowired
  @Value("${ve.data.prep.web.hdfs.url}")
  private String webHdfsUrl = "uninitialized";

  @Autowired
  @Value("${ve.data.prep.web.hdfs.base.path}")
  private String baseHdfsPath = "uninitialized";

  protected void deleteFile(String file, String path) throws Exception {
    String url = buildHdfsUrl(file, path) + "?op=DELETE";

    HttpURLConnection conn = null;
    try {
      conn = (HttpURLConnection) new URL(url).openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod(HttpMethod.DELETE);
      conn.connect();
    } finally {
      if (null != conn) {
        conn.disconnect();
      }
    }
  }

  protected void createDirectory(String path) throws Exception {
    String url = buildHdfsUrl(null, path) + "?op=MKDIRS&overwrite=true&permission=777&user.name=hdfs";

    HttpURLConnection conn = null;

    try {
      conn = (HttpURLConnection) new URL(url).openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setRequestMethod(HttpMethod.PUT);
      conn.getOutputStream();
    } finally {
      if (null != conn) {
        conn.disconnect();
      }
    }
  }

  protected void writeFile(InputStream inputStream, String file, String path) throws Exception {
    String url = buildHdfsUrl(file, path) + "?op=CREATE&overwrite=true";

    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

    conn.setInstanceFollowRedirects(false);
    HttpURLConnection.setFollowRedirects(false);
    conn.setReadTimeout(5000);
    conn.setRequestMethod(HttpMethod.PUT);

    // normally, 3xx is redirect
    int status = conn.getResponseCode();
    if ((status == 307 || status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
      String location = conn.getHeaderField("Location");
      conn = (HttpURLConnection) new URL(location).openConnection();
    }

    conn.setDoOutput(true);
    conn.setDoInput(true);
    conn.setRequestMethod(HttpMethod.PUT);

    try (OutputStream outputStream = conn.getOutputStream()) {
      IOUtils.copy(inputStream, outputStream);
    }

    conn.disconnect();
  }

  protected boolean checkFileExists(String file, boolean publicFile, String publicFolder) throws Exception {
    boolean found = false;

    String url = buildHdfsUrl(file, publicFile, publicFolder) + "?op=LISTSTATUS";

    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
    // set some connection parameters
    conn.setDoOutput(false);
    conn.setDoInput(true);

    try (InputStream inputStream = conn.getInputStream();
         BufferedReader in = new BufferedReader(new InputStreamReader(inputStream))) {
      StringBuilder sb = new StringBuilder();
      String line;
      while (null != (line = in.readLine())) {
        sb.append(line);
      }

      found = true;
    } catch (FileNotFoundException fnfe) {
      found = false;
    } finally {
      if (null != conn) {
        conn.disconnect();
      }
    }

    return found;
  }

  private String buildHdfsUrl(String file, boolean publicFile, String publicFolder) {
    String folder = "";

    if (!baseHdfsPath.startsWith("/")) {
      folder += "/";
    }
    folder += baseHdfsPath;
    if (!baseHdfsPath.endsWith("/")) {
      folder += "/";
    }

    folder += systemIdentifier;
    if (publicFile) {
      folder += "/public";
      if (StringUtils.isNotEmpty(publicFolder)) {
        folder += "/" + publicFolder;
      }
    } else {
      folder += ".User1";
    }

    return buildHdfsUrl(file, folder);
  }

  private String buildHdfsUrl(String file, String folder) {
    String url = webHdfsUrl;
    if (!url.endsWith("/")) {
      url += "/";
    }

    url += "webhdfs/v1";

    url += folder;

    if (StringUtils.isNotEmpty(file)) {
      url += "/" + file;
    }

    return url;
  }
}

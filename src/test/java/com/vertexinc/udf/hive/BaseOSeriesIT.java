package com.vertexinc.udf.hive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.util.Properties;

import static com.vertexinc.udf.hive.OSeriesUDF.*;

public abstract class BaseOSeriesIT extends BaseUDFIT {

  protected DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
  protected DocumentBuilder db;
  protected XPathFactory xpathFactory = XPathFactory.newInstance();
  protected XPath xpath = xpathFactory.newXPath();

  @Autowired
  @Value("${ve.data.prep.oseries.base.url}")
  private String oSeriesBaseUrl;

  @Autowired
  @Value("${ve.data.prep.oseries.username}")
  private String oSeriesUsername;

  @Autowired
  @Value("${ve.data.prep.oseries.password}")
  private String oSeriesPassword;

  private Properties oSeriesProps;

  protected BaseOSeriesIT() {
    try {
      db = dbf.newDocumentBuilder();
    } catch (ParserConfigurationException e) {
      LOGGER.error(e.toString());
      e.printStackTrace();
    }
  }

  protected Properties getoSeriesProps() {
    if (null == oSeriesProps) {
      oSeriesProps = buildOSeriesProps();
    }
    return oSeriesProps;
  }

  private Properties buildOSeriesProps() {
    Properties properties = new Properties();
    properties.setProperty(OSERIES_USERNAME_PARAM, oSeriesUsername);
    properties.setProperty(OSERIES_PASSWORD_PARAM, oSeriesPassword);
    properties.setProperty(OSERIES_URL_CALC_PARAM, oSeriesBaseUrl + "vertex-ws/services/CalculateTax60");
    properties.setProperty(OSERIES_URL_LOOKUP_PARAM, oSeriesBaseUrl + "vertex-ws/services/LookupTaxAreas60");
    return properties;
  }
}

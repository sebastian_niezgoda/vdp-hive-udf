package com.vertexinc.udf.hive;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.Properties;

public abstract class BaseVertexUDFTest extends BaseUDFIT {

  @Autowired
  @Value("${ve.data.prep.udf.vies.max.invalid}")
  private String viesMaxInvalidReq;

  @Autowired
  @Value("${ve.data.prep.udf.vies.max.hourly}")
  private String viesMaxHourlyReq;

  @Autowired
  @Value("${ve.data.prep.udf.vies.invalid.resetTimeout}")
  private String viesInvalidReqResetTimeout;

  private Properties udfProps;

  protected Properties getUdfProps() {
    if (null == udfProps) {
      udfProps = buildUdfProps();
    }
    return udfProps;
  }

  private Properties buildUdfProps() {
    Properties properties = new Properties();
    properties.setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, viesMaxInvalidReq);
    properties.setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, viesMaxHourlyReq);
    properties.setProperty(VertexUDF.UDF_VIES_INVALID_RESET_TIMEOUT_REQ_PARAM, viesInvalidReqResetTimeout);
    return properties;
  }

}

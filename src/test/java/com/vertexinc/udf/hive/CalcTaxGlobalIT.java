package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalcTaxGlobalIT extends BaseOSeriesIT {
  private CalcTaxGlobal calcTaxGlobal = new CalcTaxGlobal();

  @Before
  public void before() throws UDFArgumentException {
    calcTaxGlobal.setLoggedInUser("open.vedemo");
    calcTaxGlobal.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    calcTaxGlobal.setOSeriesConfig(getoSeriesProps());

    calcTaxGlobal.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    calcTaxGlobal.close();
    calcTaxGlobal = new CalcTaxGlobal();
  }

  @Test
  public void calcTaxGlobal_1() throws HiveException {
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "19312";
    String productClass = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "AZ";
    String shipFromCountry = "USA";
    String billFromState = "AZ";
    String billFromPostalCode = "85254";
    String billFromCountry = "USA";
    String shipToState = "PA";
    String shipToCountry = "United States";
    String billToState = "PA";
    String billToPostalCode = "19312";
    String billToCountry = "USA";
    String customerCode = "1001";
    String customerRegistration = "12345";
    String deliveryTerm = "CUS";
    String currencyCode = "USD";

    Object result = calcTaxGlobal.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(customerCode),
            new DeferredJavaObject(customerRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(productClass)
    });

    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
    assertTrue(result.toString().contains("transactionType=\"SALE\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>106.0</Total><TotalTax>6.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Seller><Company>1001</Company><Division>12345</Division><PhysicalOrigin taxAreaId=\"30010014\"><MainDivision>AZ</MainDivision><Country>USA</Country></PhysicalOrigin><AdministrativeOrigin taxAreaId=\"30130210\"><MainDivision>AZ</MainDivision><PostalCode>85254</PostalCode><Country>USA</Country></AdministrativeOrigin></Seller><Customer><Destination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>United States</Country></Destination><AdministrativeDestination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>USA</Country></AdministrativeDestination></Customer><Quantity>1.0</Quantity><FairMarketValue>100.0</FairMarketValue><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"SELLER_USE\" situs=\"DESTINATION\" taxCollectedFromParty=\"BUYER\"><Jurisdiction jurisdictionLevel=\"STATE\" jurisdictionId=\"31152\">PENNSYLVANIA</Jurisdiction><CalculatedTax>6.0</CalculatedTax><EffectiveRate>0.06</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"General Sales and Use Tax\">Sales and Use Tax</Imposition><TaxRuleId>17423</TaxRuleId></Taxes><TotalTax>6.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"commodityCode\" phase=\"PRE\">null</AssistedParameter><AssistedParameter paramName=\"commodityCodeType\" phase=\"PRE\">UNSPSC</AssistedParameter></AssistedParameters></LineItem></QuotationResponse>"));
  }

  @Test
  public void calcTaxGlobal_2() throws HiveException {
    // TEST 2 - sample values from query
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "";
    String productClass = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "";
    String shipFromCountry = "France";
    String billFromState = "";
    String billFromPostalCode = "";
    String billFromCountry = "France";
    String shipToState = "";
    String shipToCountry = "France";
    String billToState = "";
    String billToPostalCode = "";
    String billToCountry = "France";
    String customerCode = "";
    String customerRegistration = "";
    String deliveryTerm = "";
    String currencyCode = "";
    // test our results

    Object result = calcTaxGlobal.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(customerCode),
            new DeferredJavaObject(customerRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(productClass)
    });

    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
    assertTrue(result.toString().contains(" transactionType=\"SALE\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>120.0</Total><TotalTax>20.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Seller><PhysicalOrigin taxAreaId=\"802500000\"><Country>France</Country></PhysicalOrigin><AdministrativeOrigin taxAreaId=\"802500000\"><Country>France</Country></AdministrativeOrigin></Seller><Customer><Destination taxAreaId=\"802500000\"><Country>France</Country></Destination><AdministrativeDestination taxAreaId=\"802500000\"><Country>France</Country></AdministrativeDestination></Customer><Quantity>1.0</Quantity><FairMarketValue>100.0</FairMarketValue><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"VAT\" rateClassification=\"Standard Rate\" situs=\"DESTINATION\" inputOutputType=\"OUTPUT\" taxCollectedFromParty=\"BUYER\"><Jurisdiction jurisdictionLevel=\"COUNTRY\" jurisdictionId=\"78282\">FRANCE</Jurisdiction><CalculatedTax>20.0</CalculatedTax><EffectiveRate>0.2</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"VAT\">VAT</Imposition><TaxRuleId>778390</TaxRuleId><InvoiceTextCode>21</InvoiceTextCode></Taxes><TotalTax>20.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"commodityCode\" phase=\"PRE\">null</AssistedParameter><AssistedParameter paramName=\"commodityCodeType\" phase=\"PRE\">UNSPSC</AssistedParameter></AssistedParameters></LineItem></QuotationResponse>"));
  }

  @Test
  public void calcTaxGlobal_3() throws HiveException {
    // TEST 3 - only 2 values
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "";
    String productClass = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "";
    String shipFromCountry = "France";
    String billFromState = "";
    String billFromPostalCode = "";
    String billFromCountry = "France";
    String shipToState = "";
    String shipToCountry = "France";
    String billToState = "";
    String billToPostalCode = "";
    String billToCountry = "France";
    String customerCode = "";
    String customerRegistration = "";
    String deliveryTerm = "";
    String currencyCode = "";
    // test our results

    Object result = calcTaxGlobal.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice)
    });

    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
    assertTrue(result.toString().contains("transactionType=\"SALE\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>120.0</Total><TotalTax>20.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Seller></Seller><Customer><Destination taxAreaId=\"802500000\"><Country>France</Country></Destination></Customer><Quantity>1.0</Quantity><FairMarketValue>100.0</FairMarketValue><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"VAT\" rateClassification=\"Standard Rate\" situs=\"DESTINATION\" inputOutputType=\"OUTPUT\" taxCollectedFromParty=\"BUYER\"><Jurisdiction jurisdictionLevel=\"COUNTRY\" jurisdictionId=\"78282\">FRANCE</Jurisdiction><CalculatedTax>20.0</CalculatedTax><EffectiveRate>0.2</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"VAT\">VAT</Imposition><TaxRuleId>778390</TaxRuleId><InvoiceTextCode>21</InvoiceTextCode></Taxes><TotalTax>20.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"commodityCode\" phase=\"PRE\">null</AssistedParameter><AssistedParameter paramName=\"commodityCodeType\" phase=\"PRE\">UNSPSC</AssistedParameter></AssistedParameters></LineItem></QuotationResponse>"));
  }

  @Test
  public void calcTaxGlobal_STE() throws HiveException, SAXException, XPathExpressionException, IOException {
    assertEquals("0.0", runSTEExample("CAN,1000500,,T6G2R3,,,USA,,,,,,,,,CUST001,,CAN,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CAN,1000500,,T6G2R3,PA,19333,USA,,T6G2R3,CAN,PA,19333,USA,,,CUST001,,CAD,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CHN,5000054689,,100010,,,DEU,,,,,,,,,CUST001,,CNY,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CHN,5000054689,,100010,HH,20095,DEU,,100010,CHN,HH,20095,DEU,,,CUST001,,CNY,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("USA,75999,,19333,,,AUS,,,,,,,,,CUST001,,USD,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("USA,75999,,19333,QLD,4211,AUS,,19333,USA,QLD,4211,AUS,,,CUST001,,USD,CUS,PRODUCT,0.0"));
  }

  private String runSTEExample(String row) throws HiveException, SAXException, XPathExpressionException, IOException {
    String[] cols = row.split(",");
    return runCalcTax(cols[0], cols[1], cols[2], cols[3],
            cols[4], cols[5], cols[6], cols[7], cols[8],
            cols[9],cols[10],cols[11], cols[12],cols[15],
            cols[16],cols[13],cols[14],cols[17],cols[18],cols[20]);
  }

  private String runCalcTax(String shipToCountry, String extendedPrice, String shipToState, String shipToPostalCode,
                            String shipFromState, String shipFromPostalCode, String shipFromCountry, String billToState,
                            String billToPostalCode, String billToCountry, String billFromState, String billFromPostalCode,
                            String billFromCountry, String customerCode, String customerRegistration, String company,
                            String division, String currencyCode, String deliveryTerm, String productClass) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = calcTaxGlobal.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(customerCode),
            new DeferredJavaObject(customerRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(productClass)
    });
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
    return xpath.evaluate("/QuotationResponse/TotalTax", document);
  }
}

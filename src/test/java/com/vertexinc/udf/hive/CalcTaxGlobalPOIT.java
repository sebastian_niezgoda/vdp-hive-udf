package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalcTaxGlobalPOIT extends BaseOSeriesIT {
  private CalcTaxGlobalPO calcTaxGlobalPO = new CalcTaxGlobalPO();

  @Before
  public void before() throws UDFArgumentException {
    calcTaxGlobalPO.setLoggedInUser("open.vedemo");
    calcTaxGlobalPO.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    calcTaxGlobalPO.setOSeriesConfig(getoSeriesProps());

    calcTaxGlobalPO.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    calcTaxGlobalPO.close();
    calcTaxGlobalPO = new CalcTaxGlobalPO();
  }

  @Test
  public void calcTaxGlobalPO_1() throws HiveException {
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "19312";
    String purchase = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "AZ";
    String shipFromCountry = "USA";
    String billFromState = "AZ";
    String billFromPostalCode = "85254";
    String billFromCountry = "USA";
    String shipToState = "PA";
    String shipToCountry = "United States";
    String billToState = "PA";
    String billToPostalCode = "19312";
    String billToCountry = "USA";
    String vendorCode = "1001";
    String vendorRegistration = "12345";
    String deliveryTerm = "CUS";
    String currencyCode = "USD";
    String taxDate = "2017-10-01";
    String calcVendorTax = "y";

    Object result = calcTaxGlobalPO.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(vendorCode),
            new DeferredJavaObject(vendorRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(purchase),
            new DeferredJavaObject(taxDate),
            new DeferredJavaObject(calcVendorTax)
    });

    assertTrue(result.toString().indexOf("</PurchaseOrderResponse>") > 1);
    assertTrue(result.toString().contains("transactionType=\"PURCHASE\" calculateVendorTaxIndicator=\"true\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>106.0</Total><TotalTax>6.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Buyer><Company>1001</Company><Division>12345</Division><Destination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>United States</Country></Destination><AdministrativeDestination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>USA</Country></AdministrativeDestination></Buyer><Vendor><PhysicalOrigin taxAreaId=\"30010875\"><MainDivision>AZ</MainDivision><Country>USA</Country></PhysicalOrigin><AdministrativeOrigin taxAreaId=\"30130210\"><MainDivision>AZ</MainDivision><PostalCode>85254</PostalCode><Country>USA</Country></AdministrativeOrigin></Vendor><Quantity>1.0</Quantity><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"CONSUMERS_USE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"STATE\" jurisdictionId=\"31152\">PENNSYLVANIA</Jurisdiction><CalculatedTax>0.0</CalculatedTax><EffectiveRate>0.0</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"General Sales and Use Tax\">Sales and Use Tax</Imposition><TaxRuleId>17422</TaxRuleId></Taxes><Taxes taxResult=\"TAXABLE\" taxType=\"SELLER_USE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"STATE\" jurisdictionId=\"31152\">PENNSYLVANIA</Jurisdiction><CalculatedTax>6.0</CalculatedTax><EffectiveRate>0.06</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"General Sales and Use Tax\">Sales and Use Tax</Imposition><TaxRuleId>17423</TaxRuleId></Taxes><TotalTax>6.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"inputTaxLocation.taxAreaId\" phase=\"PRE\">0</AssistedParameter></AssistedParameters></LineItem></PurchaseOrderResponse>"));
  }

  @Ignore
  @Test
  public void calcTaxGlobalPO_MichelleTest() throws HiveException {
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "19333";
    String purchase = "item1";
    String extendedPrice = "725";
    // MTC - Add VAT params
    String shipFromState = "";
    String shipFromCountry = "";
    String billFromState = "";
    String billFromPostalCode = "";
    String billFromCountry = "";
    String shipToState = "pa";
    String shipToCountry = "UNITED STATES";
    String billToState = "";
    String billToPostalCode = "";
    String billToCountry = "";
    String vendorCode = "";
    String vendorRegistration = "";
    String deliveryTerm = "CUS";
    String currencyCode = "USD";
    String taxDate = "";
    String calcVendorTax = "";

    Object result = calcTaxGlobalPO.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(vendorCode),
            new DeferredJavaObject(vendorRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(purchase),
            new DeferredJavaObject(taxDate),
            new DeferredJavaObject(calcVendorTax)
    });

    assertTrue(result.toString().indexOf("</PurchaseOrderResponse>") > 1);
    assertTrue(result.toString().contains("transactionType=\"PURCHASE\" calculateVendorTaxIndicator=\"true\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>106.0</Total><TotalTax>6.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Buyer><Company>1001</Company><Division>12345</Division><Destination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>United States</Country></Destination><AdministrativeDestination taxAreaId=\"390290000\"><MainDivision>PA</MainDivision><PostalCode>19312</PostalCode><Country>USA</Country></AdministrativeDestination></Buyer><Vendor><PhysicalOrigin taxAreaId=\"30010875\"><MainDivision>AZ</MainDivision><Country>USA</Country></PhysicalOrigin><AdministrativeOrigin taxAreaId=\"30130210\"><MainDivision>AZ</MainDivision><PostalCode>85254</PostalCode><Country>USA</Country></AdministrativeOrigin></Vendor><Quantity>1.0</Quantity><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"TAXABLE\" taxType=\"CONSUMERS_USE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"STATE\" jurisdictionId=\"31152\">PENNSYLVANIA</Jurisdiction><CalculatedTax>0.0</CalculatedTax><EffectiveRate>0.0</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"General Sales and Use Tax\">Sales and Use Tax</Imposition><TaxRuleId>17422</TaxRuleId></Taxes><Taxes taxResult=\"TAXABLE\" taxType=\"SELLER_USE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"STATE\" jurisdictionId=\"31152\">PENNSYLVANIA</Jurisdiction><CalculatedTax>6.0</CalculatedTax><EffectiveRate>0.06</EffectiveRate><Taxable>100.0</Taxable><Imposition impositionType=\"General Sales and Use Tax\">Sales and Use Tax</Imposition><TaxRuleId>17423</TaxRuleId></Taxes><TotalTax>6.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"inputTaxLocation.taxAreaId\" phase=\"PRE\">0</AssistedParameter></AssistedParameters></LineItem></PurchaseOrderResponse>"));
  }

  @Test
  public void calcTaxGlobalPO_2() throws HiveException {
    // TEST 2 - sample values from query
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "";
    String purchase = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "";
    String shipFromCountry = "France";
    String billFromState = "";
    String billFromPostalCode = "";
    String billFromCountry = "France";
    String shipToState = "";
    String shipToCountry = "France";
    String billToState = "";
    String billToPostalCode = "";
    String billToCountry = "France";
    String vendorCode = "";
    String vendorRegistration = "";
    String deliveryTerm = "";
    String currencyCode = "";
    // test our results

    Object result = calcTaxGlobalPO.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(vendorCode),
            new DeferredJavaObject(vendorRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(purchase)
    });

    assertTrue(result.toString().indexOf("</PurchaseOrderResponse>") > 1);
    assertTrue(result.toString().contains(" transactionType=\"PURCHASE\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>100.0</Total><TotalTax>0.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Buyer><Destination taxAreaId=\"802500000\"><Country>France</Country></Destination><AdministrativeDestination taxAreaId=\"802500000\"><Country>France</Country></AdministrativeDestination></Buyer><Vendor><PhysicalOrigin taxAreaId=\"802500000\"><Country>France</Country></PhysicalOrigin><AdministrativeOrigin taxAreaId=\"802500000\"><Country>France</Country></AdministrativeOrigin></Vendor><Quantity>1.0</Quantity><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"NO_TAX\" taxType=\"NONE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"COUNTRY\" jurisdictionId=\"78282\">FRANCE</Jurisdiction><CalculatedTax>0.0</CalculatedTax><EffectiveRate>0.0</EffectiveRate><Taxable>0.0</Taxable><InvoiceTextCode>21</InvoiceTextCode></Taxes><TotalTax>0.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"inputTaxLocation.taxAreaId\" phase=\"PRE\">0</AssistedParameter></AssistedParameters></LineItem></PurchaseOrderResponse>"));
  }

  @Test
  public void calcTaxGlobalPO_3() throws HiveException {
    // TEST 3 - only 2 values
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "";
    String purchase = "";
    String extendedPrice = "100";
    // MTC - Add VAT params
    String shipFromState = "";
    String shipFromCountry = "France";
    String billFromState = "";
    String billFromPostalCode = "";
    String billFromCountry = "France";
    String shipToState = "";
    String shipToCountry = "France";
    String billToState = "";
    String billToPostalCode = "";
    String billToCountry = "France";
    String vendorCode = "";
    String vendorRegistration = "";
    String deliveryTerm = "";
    String currencyCode = "";

    // test our results
    Object result = calcTaxGlobalPO.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice)
    });

    assertTrue(result.toString().indexOf("</PurchaseOrderResponse>") > 1);
    assertTrue(result.toString().contains("transactionType=\"PURCHASE\"><Currency isoCurrencyCodeAlpha=\"USD\" isoCurrencyCodeNum=\"840\" isoCurrencyName=\"US Dollar\"></Currency><SubTotal>100.0</SubTotal><Total>100.0</Total><TotalTax>0.0</TotalTax><LineItem deliveryTerm=\"CUS\"><Buyer><Destination taxAreaId=\"802500000\"><Country>France</Country></Destination></Buyer><Vendor></Vendor><Quantity>1.0</Quantity><ExtendedPrice>100.0</ExtendedPrice><Taxes taxResult=\"NO_TAX\" taxType=\"NONE\" situs=\"DESTINATION\"><Jurisdiction jurisdictionLevel=\"COUNTRY\" jurisdictionId=\"78282\">FRANCE</Jurisdiction><CalculatedTax>0.0</CalculatedTax><EffectiveRate>0.0</EffectiveRate><Taxable>0.0</Taxable><InvoiceTextCode>21</InvoiceTextCode></Taxes><TotalTax>0.0</TotalTax><AssistedParameters><AssistedParameter paramName=\"inputTaxLocation.taxAreaId\" phase=\"PRE\">0</AssistedParameter></AssistedParameters></LineItem></PurchaseOrderResponse>"));
  }

  @Test
  public void calcTaxGlobalPO_STE() throws HiveException, SAXException, XPathExpressionException, IOException {
    // this is the same data as the CalcTaxGlobal function
    assertEquals("0.0", runSTEExample("CAN,1000500,,T6G2R3,,,USA,,,,,,,,,CUST001,,CAN,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CAN,1000500,,T6G2R3,PA,19333,USA,,T6G2R3,CAN,PA,19333,USA,,,CUST001,,CAD,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CHN,5000054689,,100010,,,DEU,,,,,,,,,CUST001,,CNY,CUS,PRODUCT,0.0"));
    assertEquals("0.0", runSTEExample("CHN,5000054689,,100010,HH,20095,DEU,,100010,CHN,HH,20095,DEU,,,CUST001,,CNY,CUS,PRODUCT,0.0"));
    assertEquals("4559.94", runSTEExample("USA,75999,,19333,,,AUS,,,,,,,,,CUST001,,USD,CUS,PRODUCT,0.0"));
    assertEquals("4559.94", runSTEExample("USA,75999,,19333,QLD,4211,AUS,,19333,USA,QLD,4211,AUS,,,CUST001,,USD,CUS,PRODUCT,0.0"));

    // this is the data from the actual CalcTaxGlobalPO
    assertEquals("0.6", runCalcTax(null, "10", "PENNSYLVANIA",
            null, null, null, null, null, null,
            null, null, null, null, "CUST1",
            null, "TP1", null, null, null, null));
  }

  private String runSTEExample(String row) throws HiveException, SAXException, XPathExpressionException, IOException {
    String[] cols = row.split(",");
    return runCalcTax(cols[0], cols[1], cols[2], cols[3],
            cols[4], cols[5], cols[6], cols[7], cols[8],
            cols[9],cols[10],cols[11], cols[12],cols[15],
            cols[16],cols[13],cols[14],cols[17],cols[18],cols[20]);
  }

  private String runCalcTax(String shipToCountry, String extendedPrice, String shipToState, String shipToPostalCode,
                            String shipFromState, String shipFromPostalCode, String shipFromCountry, String billToState,
                            String billToPostalCode, String billToCountry, String billFromState, String billFromPostalCode,
                            String billFromCountry, String customerCode, String customerRegistration, String company,
                            String division, String currencyCode, String deliveryTerm, String productClass) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = calcTaxGlobalPO.evaluate(new DeferredObject[]{
            new DeferredJavaObject(shipToCountry),
            new DeferredJavaObject(extendedPrice),
            new DeferredJavaObject(shipToState),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(shipFromState),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipFromCountry),
            new DeferredJavaObject(billToState),
            new DeferredJavaObject(billToPostalCode),
            new DeferredJavaObject(billToCountry),
            new DeferredJavaObject(billFromState),
            new DeferredJavaObject(billFromPostalCode),
            new DeferredJavaObject(billFromCountry),
            new DeferredJavaObject(customerCode),
            new DeferredJavaObject(customerRegistration),
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(currencyCode),
            new DeferredJavaObject(deliveryTerm),
            new DeferredJavaObject(productClass)
    });
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
    return xpath.evaluate("/PurchaseOrderResponse/TotalTax", document);
  }

}

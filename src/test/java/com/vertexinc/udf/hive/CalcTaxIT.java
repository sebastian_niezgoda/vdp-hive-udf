package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.io.StringReader;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CalcTaxIT extends BaseOSeriesIT {
  private CalcTax calcTax = new CalcTax();

  @Before
  public void before() throws UDFArgumentException, ParserConfigurationException {
    calcTax.setLoggedInUser("open.vedemo");
    calcTax.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    calcTax.setOSeriesConfig(getoSeriesProps());

    calcTax.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    calcTax.close();
    calcTax = new CalcTax();
  }

  @Test
  public void calcTax_1() throws HiveException {
    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "19312";
    String productClass = "";
    String extendedPrice = "100";

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
  }

  @Test
  public void calcTax_2() throws HiveException {
    String company = "1001";
    String division = "100";
    String shipFromPostalCode = "19312";
    String shipToPostalCode = "19312";
    String productClass = "39";
    String extendedPrice = "199";

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
  }

  @Test
  public void calcTax_STE_1() throws HiveException {
    String company = "CompanyA";
    String division = "DivisionA";
    String shipFromPostalCode = "90.000.0089";
    String shipToPostalCode = "19333";
    String productClass = "Product1";
    String extendedPrice = "150.00";

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    assertNull(result);
  }

  @Test
  public void calcTax_STE_2() throws HiveException, IOException, SAXException, XPathExpressionException {
    assertEquals("900.0", runSTEExample("CompanyB|DivisionB|17011|19333|Product2|15000.00|"));
    assertEquals("1.98", runSTEExample("CompanyC|DivisionC|90089|19333|Product1|33.00|"));
    assertEquals("9.0", runSTEExample("CompanyA|DivisionA|90089|19333|Product1|150.00|"));
    assertEquals("0.6", runSTEExample("CompanyB|DivisionB|17011|19333|Product1|10.00|"));
    assertEquals("3.0", runSTEExample("CompanyC|DivisionC|90089|19333|Product2|50.00|"));
    assertEquals("10.2", runSTEExample("CompanyA|DivisionA|90089|19333|Product2|170.00|"));
    assertEquals("6000.0", runSTEExample("CompanyB|DivisionB|17011|19333|Product5|100000.00|"));
    assertEquals("5.28", runSTEExample("CompanyC|DivisionC|90089|17011|Product1|88.00|"));
    assertEquals("9.0", runSTEExample("CompanyA|DivisionA|90089|19333|Product1|150.00|"));
  }

  private String runSTEExample(String row) throws HiveException, SAXException, XPathExpressionException, IOException {
    String[] cols = row.split("\\|");
    return runCalcTax(cols[0], cols[1], cols[2], cols[3], cols[4], cols[5]);
  }

  private String runCalcTax(String company, String division, String shipFromPostalCode, String shipToPostalCode, String productClass, String extendedPrice) throws HiveException, IOException, SAXException, XPathExpressionException {
    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    Document document = db.parse(new InputSource(new StringReader(result.toString())));
    return xpath.evaluate("/QuotationResponse/TotalTax", document);
  }


  @Test
  public void calcTax_Null() throws HiveException {
    String company = "10000";
    String division = "10000";
    String shipFromPostalCode = "85254";
    String shipToPostalCode = "19312";
    String productClass = "productclass";
    String extendedPrice = null;

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    assertNull(result);
  }

  @Test
  public void calcTax_Null2() throws HiveException {
    String company = "10000";
    String division = "10000";
    String shipFromPostalCode = "85254";
    String shipToPostalCode = "19312";
    String productClass = "productclass";
    String extendedPrice = " ";

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    assertNull(result);
  }

  @Test
  public void calcTax_3() throws HiveException {
    ExpectedException exception = ExpectedException.none();
    exception.expect(HiveException.class);
    exception.expectMessage(CoreMatchers.containsString("Unable to read response for request: Server returned HTTP response code: 500 for URL"));

    String company = "";
    String division = "";
    String shipFromPostalCode = "";
    String shipToPostalCode = "";
    String productClass = "";
    String extendedPrice = "100";

    Object result = calcTax.evaluate(createDeferredObjectArray(company, division, shipFromPostalCode, shipToPostalCode, productClass, extendedPrice));
    // this assertion is from the field dev code, but it fails in the field dev code too
//    assertTrue(result.toString().indexOf("</QuotationResponse>") > 1);
  }

  private DeferredObject[] createDeferredObjectArray(
          String company,
          String division,
          String shipFromPostalCode,
          String shipToPostalCode,
          String product,
          String extendedPrice
  ) {
    return new DeferredObject[]{
            new DeferredJavaObject(company),
            new DeferredJavaObject(division),
            new DeferredJavaObject(shipFromPostalCode),
            new DeferredJavaObject(shipToPostalCode),
            new DeferredJavaObject(product),
            new DeferredJavaObject(extendedPrice)
    };
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static com.vertexinc.udf.hive.CheckVat.PROCESSING_ERROR;
import static com.vertexinc.udf.hive.CheckVatOrig.INVALID_RESP;
import static org.junit.Assert.assertEquals;

public class CheckVatOrigIT {
  private CheckVatOrig checkVatOrig = new CheckVatOrig();

  /**
   * We have to do this mess because we want the counters and initialized marker to be static, so that across multiple
   * mapping operations, the counters remain constant. So, we have to try to make the class appear to be destroyed
   * and recreated, which is tough to do since it's static.
   */
  @Before
  public void setup() {
    checkVatOrig.getCheckVat().clearCounters();
    checkVatOrig.getCheckVat().resetInitializedMarker();
  }

  /**
   * This test ensures that the invalid request counter resets after the specified time, in this case, 1 minute.
   */
  @Test
  public void checkVatInvalidRequestsResetCounter() throws InterruptedException {
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, String.valueOf(1));
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, String.valueOf(100));
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_VIES_INVALID_RESET_TIMEOUT_REQ_PARAM, String.valueOf(1));

    assertEquals(INVALID_RESP, checkVatOrig.evaluate(new Text("EE"), new Text("9999992")).toString());
    assertEquals(PROCESSING_ERROR, checkVatOrig.evaluate(new Text("EE"), new Text("9999991")).toString());
    TimeUnit.SECONDS.sleep(65);
    checkVatOrig.evaluate(new Text("EE"), new Text("9999991"));
    assertEquals(INVALID_RESP, checkVatOrig.evaluate(new Text("EE"), new Text("9999992")).toString());
    assertEquals(PROCESSING_ERROR, checkVatOrig.evaluate(new Text("EE"), new Text("9999990")).toString());
  }
}

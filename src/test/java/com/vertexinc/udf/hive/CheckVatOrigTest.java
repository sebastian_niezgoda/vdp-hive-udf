package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

import static com.vertexinc.udf.hive.CheckVat.HOURLY_LIMIT_ERROR;
import static com.vertexinc.udf.hive.CheckVat.PROCESSING_ERROR;
import static com.vertexinc.udf.hive.CheckVatOrig.INVALID_RESP;
import static com.vertexinc.udf.hive.CheckVatOrig.VALID_RESP;
import static org.junit.Assert.assertEquals;

public class CheckVatOrigTest {

  private CheckVatOrig checkVatOrig = new CheckVatOrig();

  /**
   * We have to do this mess because we want the counters and initialized marker to be static, so that across multiple
   * mapping operations, the counters remain constant. So, we have to try to make the class appear to be destroyed
   * and recreated, which is tough to do since it's static.
   */
  @Before
  public void setup() {
    checkVatOrig.getCheckVat().clearCounters();
    checkVatOrig.getCheckVat().resetInitializedMarker();
  }

  @Test
  public void testCheckVatOrig() {
    String countryCode = "EE";
    String vatNum = "100852323";

    assertEquals(VALID_RESP, checkVatOrig.evaluate(new Text(countryCode), new Text(vatNum)).toString());
    assertEquals(INVALID_RESP, checkVatOrig.evaluate(new Text(countryCode), new Text("999999999")).toString());

    assertEquals("Mustamäe tee 4   10612 Kristiine linnaosa Tallinn",
            checkVatOrig.evaluate(new Text(countryCode), new Text(vatNum), new Text("address")).toString());

    assertEquals(INVALID_RESP, checkVatOrig.evaluate(new Text("RR"), new Text("9999999999999999966666666699999999999")).toString());
  }

  @Test
  public void checkVatInvalidRequests() {
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, String.valueOf(3));
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, String.valueOf(100));

    checkVatOrig.evaluate(new Text("EE"), new Text("9999995"));
    checkVatOrig.evaluate(new Text("EE"), new Text("9999994"));
    checkVatOrig.evaluate(new Text("EE"), new Text("9999993"));
    assertEquals(PROCESSING_ERROR, checkVatOrig.evaluate(new Text("EE"), new Text("9999992")).toString());
  }

  @Test
  public void checkVatHourlyLimit() {
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, String.valueOf(100));
    checkVatOrig.getCheckVat().getUdfConfig().setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, String.valueOf(3));

    checkVatOrig.evaluate(new Text("EE"), new Text("9999999"));
    checkVatOrig.evaluate(new Text("EE"), new Text("9999998"));
    checkVatOrig.evaluate(new Text("EE"), new Text("9999997"));
    assertEquals(HOURLY_LIMIT_ERROR, checkVatOrig.evaluate(new Text("EE"), new Text("9999991")).toString());
  }
}

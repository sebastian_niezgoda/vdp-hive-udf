package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.vertexinc.udf.hive.CheckVat.HOURLY_LIMIT_ERROR;
import static com.vertexinc.udf.hive.CheckVat.PROCESSING_ERROR;
import static com.vertexinc.udf.hive.IVertexUDF.HIVE_DATE_FORMAT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheckVatTest extends BaseVertexUDFTest {
  private CheckVat checkVat = new CheckVat();

  @Before
  public void before() {
    checkVat.clearCounters();
    checkVat.resetInitializedMarker();
  }

  @Test
  public void checkVatTest() {
    String currentDate = new SimpleDateFormat(HIVE_DATE_FORMAT).format(new Date());
    Text countryCode = new Text("EE");
    Text vatNum = new Text("100852323");

    String result = checkVat.evaluate(countryCode, vatNum).toString();
    assertTrue(result.indexOf("|true|OÜ APPLAFORD ES TRANSPORT &amp; LOGISTICS (PANKROTIS)|Mustamäe tee 4   10612 Kristiine linnaosa Tallinn") > -1);
    assertTrue(result.indexOf(currentDate) > -1);

    assertEquals("true",
            checkVat.evaluate(countryCode, vatNum, new Text("valid")).toString());

    assertTrue(checkVat.evaluate(countryCode, vatNum, new Text("requestDate")).toString().contains(currentDate));

    assertEquals("OÜ APPLAFORD ES TRANSPORT &amp; LOGISTICS (PANKROTIS)",
            checkVat.evaluate(countryCode, vatNum, new Text("name")).toString());

    assertEquals("Mustamäe tee 4   10612 Kristiine linnaosa Tallinn",
            checkVat.evaluate(countryCode, vatNum, new Text("address")).toString());

    Text res = checkVat.evaluate(new Text("RR"), new Text("9999999999999999966666666699999999999"));
    System.out.println(res.toString());
  }

  @Test
  public void checkVatInvalidRequests() {
    checkVat.getUdfConfig().setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, String.valueOf(3));
    checkVat.getUdfConfig().setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, String.valueOf(100));

    checkVat.evaluate(new Text("EE"), new Text("9999985"));
    checkVat.evaluate(new Text("EE"), new Text("9999984"));
    checkVat.evaluate(new Text("EE"), new Text("9999983"));
    assertEquals(PROCESSING_ERROR, checkVat.evaluate(new Text("EE"), new Text("9999982")).toString());
  }

  @Test
  public void checkVatHourlyLimit() {
    checkVat.getUdfConfig().setProperty(VertexUDF.UDF_MAX_CONSEC_INVALID_VIES_REQ_PARAM, String.valueOf(100));
    checkVat.getUdfConfig().setProperty(VertexUDF.UDF_MAX_HOURLY_VIES_REQ_PARAM, String.valueOf(3));

    checkVat.evaluate(new Text("EE"), new Text("9999989"));
    checkVat.evaluate(new Text("EE"), new Text("9999988"));
    checkVat.evaluate(new Text("EE"), new Text("9999987"));
    assertEquals(HOURLY_LIMIT_ERROR, checkVat.evaluate(new Text("EE"), new Text("9999981")).toString());
  }
}

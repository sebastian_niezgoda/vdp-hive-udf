package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CurrXRateTest {
  private CurrXRate currXRate = new CurrXRate();

  @Test
  public void currXRateTest_Default(){
    assertEquals((Double)0.2371,currXRate.evaluate(new Text("2015-09-11"), new Text("EUR"), new Text("PLN")));
    // check to make sure lowercase is handled the same way
    assertEquals((Double)0.2371,currXRate.evaluate(new Text("2015-09-11"), new Text("eur"), new Text("pln")));
    assertEquals((Double)0.0032,currXRate.evaluate(new Text("2015-09-12"), new Text("EUR"), new Text("HUF")));
    assertEquals((Double)0.0032,currXRate.evaluate(new Text("2015-09-10"), new Text("EUR"), new Text("HUF")));
    assertEquals((Double)0.0033,currXRate.evaluate(new Text("2015-03-24"), new Text("EUR"), new Text("HUF")));
    assertEquals((Double)0.2378,currXRate.evaluate(new Text("2015-09-14"), new Text("EUR"), new Text("PLN")));
    assertEquals((Double)0.1036,currXRate.evaluate(new Text("2014-12-27"), new Text("GBP"), new Text("CNY")));
    assertEquals((Double)0.005,currXRate.evaluate(new Text("2015-11-12"), new Text("GBP"), new Text("ISK")));
    assertEquals((Double)0.0319,currXRate.evaluate(new Text("2016-11-02"), new Text("GBP"), new Text("UAH")));
  }

  @Test
  public void currXRate_Europe(){
    assertEquals((Double)0.74519, currXRate.evaluate(new Text("2016-01-10"), new Text("EUR"), new Text("GBP"), new Text("europe")));
    // check to make sure lowercase is handled the same way
    assertEquals((Double)0.74519, currXRate.evaluate(new Text("2016-01-10"), new Text("eur"), new Text("gbp"), new Text("europe")));
    // for caching test:
    assertEquals((Double)0.85085, currXRate.evaluate(new Text("2016-07-07"), new Text("EUR"), new Text("GBP"), new Text("europe")));
    // the endpoint does not return a value for this date, so the result should be 0.0
    assertEquals((Double)0.0, currXRate.evaluate(new Text("2014-01-01"), new Text("EUR"), new Text("GBP"), new Text("europe")));
    assertEquals((Double)0.7903, currXRate.evaluate(new Text("2014-07-16"), new Text("EUR"), new Text("GBP"), new Text("europe")));
    assertEquals((Double)1.0, currXRate.evaluate(new Text("2018-02-21"), new Text("PLN"), new Text("PLN"), new Text("europe")));;
    assertEquals((Double)1.0, currXRate.evaluate(new Text("2018-02-21"), new Text("EUR"), new Text("EUR"), new Text("europe")));;
    assertEquals((Double)0.0, currXRate.evaluate(new Text("2018-02-21"), new Text("PLN"), new Text("EUR"), new Text("europe")));;
  }

  @Test
  public void currXRate_Poland(){
    assertEquals((Double)0.2301, currXRate.evaluate(new Text("2016-09-12"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)17.2551, currXRate.evaluate(new Text("2016-09-12"), new Text("PLN"), new Text("INR"), new Text("poland")));
    assertEquals((Double)0.2376, currXRate.evaluate(new Text("2015-09-11"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)0.2343, currXRate.evaluate(new Text("2017-09-13"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)0.2346, currXRate.evaluate(new Text("2016-08-14"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)0.2363, currXRate.evaluate(new Text("2015-10-18"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)0.9674, currXRate.evaluate(new Text("2018-02-15"),  new Text("PLN"),new Text("BRL"), new Text("poland")));
    assertEquals((Double)2.3574, currXRate.evaluate(new Text("2018-02-16"), new Text("PLN"), new Text("HKD"), new Text("poland")));
    assertEquals((Double)0.3777, currXRate.evaluate(new Text("2018-02-17"), new Text("PLN"), new Text("AUD"), new Text("poland")));
    assertEquals((Double)19.3401, currXRate.evaluate(new Text("2018-02-18"), new Text("PLN"), new Text("INR"), new Text("poland")));
    assertEquals((Double)0.2412, currXRate.evaluate(new Text("2018-02-19"), new Text("PLN"), new Text("EUR"), new Text("poland")));
    assertEquals((Double)1.8896, currXRate.evaluate(new Text("2018-02-20"), new Text("PLN"), new Text("CNY"), new Text("poland")));
    assertEquals((Double)0.2124, currXRate.evaluate(new Text("2018-02-21"), new Text("PLN"), new Text("GBP"), new Text("poland")));
    assertEquals((Double)1.0, currXRate.evaluate(new Text("2018-02-21"), new Text("PLN"), new Text("PLN"), new Text("poland")));;
    assertEquals((Double)1.0, currXRate.evaluate(new Text("2018-02-21"), new Text("EUR"), new Text("EUR"), new Text("poland")));;
    assertEquals((Double)0.0, currXRate.evaluate(new Text("2018-02-21"), new Text("EUR"), new Text("PLN"), new Text("poland")));;
  }
}

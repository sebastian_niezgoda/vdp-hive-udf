package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class IsAlphaNumericTest {
  private IsAlphaNumeric isAlphaNumeric = new IsAlphaNumeric();

  @Test
  public void isAlphaNumericTest(){
    assertTrue(isAlphaNumeric.evaluate(new Text("0")));
    assertTrue(isAlphaNumeric.evaluate(new Text("1234567891111")));
    assertTrue(isAlphaNumeric.evaluate(new Text("hello there dude")));
    assertTrue(isAlphaNumeric.evaluate(new Text("9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999")));
    assertTrue(isAlphaNumeric.evaluate(new Text("HELLOhello65456")));
    assertFalse(isAlphaNumeric.evaluate(new Text("letters**")));

    // we are emulating the way that field handles these numbers. even though they are in fact numbers, they are not in field dev world.
    assertFalse(isAlphaNumeric.evaluate(new Text("-123")));
    assertFalse(isAlphaNumeric.evaluate(new Text("455.29")));
    assertFalse(isAlphaNumeric.evaluate(new Text("(17)")));
  }
}

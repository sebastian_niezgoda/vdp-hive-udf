package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class IsDateTest {
  private IsDate isDate = new IsDate();

  @Test
  public void isDateTest() {
    assertTrue(isDate.evaluate(new Text("01-01-1900")));
    assertTrue(isDate.evaluate(new Text("JAN-01-2000")));
    assertTrue(isDate.evaluate(new Text("JAN-01-2000 10:14:05")));
    assertTrue(isDate.evaluate(new Text("JAN-01-2000 10:14")));
    assertTrue(isDate.evaluate(new Text("JAN-01-2000 17:14")));
    assertTrue(isDate.evaluate(new Text("JAN-1-15")));
    assertTrue(isDate.evaluate(new Text("FEB-29-2016")));
    // in my opinion, this should fail as it's not a valid date, waiting for more info on this
//    assertFalse(isDate.evaluate(new Text("FEB-29-2017")));
    assertFalse(isDate.evaluate(new Text("February 29th, 2017")));
    assertFalse(isDate.evaluate(new Text("February 27th, 2017")));
    assertFalse(isDate.evaluate(new Text("February 20, 2017")));
    assertFalse(isDate.evaluate(new Text("01,01,15")));
    assertFalse(isDate.evaluate(new Text("01.01.15")));
    assertFalse(isDate.evaluate(new Text("jan")));
  }

  @Test
  public void isDateTest_CustomDateFormat() {
    assertTrue(isDate.evaluate(new Text("01-01-1900"),  new Text("MM-DD-YYYY")));
    assertFalse(isDate.evaluate(new Text("01-01-1900"),  new Text("mmmmmdmdmdmmdmmddyyyy")));
    assertTrue(isDate.evaluate(new Text("JAN-01-2000"), new Text("MMM-DD-YYYY")));
    assertFalse(isDate.evaluate(new Text("JAN-01-2000"), new Text("MM-DD-YYYY")));
  }
}

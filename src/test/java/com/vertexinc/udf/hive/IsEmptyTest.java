package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.io.BooleanWritable;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class IsEmptyTest {
  private IsEmpty isEmpty = new IsEmpty();

  @Test
  public void isEmptyTest() throws HiveException {
    assertNull(isEmpty.evaluate(null));

    assertTrue(((BooleanWritable) isEmpty.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject(null)
    })).get());

    assertFalse(((BooleanWritable) isEmpty.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject("100")
    })).get());

    assertTrue(((BooleanWritable) isEmpty.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject("")
    })).get());

    assertTrue(((BooleanWritable) isEmpty.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject(" ")
    })).get());
  }
}

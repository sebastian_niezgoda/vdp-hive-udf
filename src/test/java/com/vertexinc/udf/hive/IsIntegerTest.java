package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class IsIntegerTest {
  private IsInteger isInteger = new IsInteger();

  @Test
  public void isIntegerTest(){
    assertTrue(isInteger.evaluate(new Text("0")));
    assertTrue(isInteger.evaluate(new Text("2147483647")));
    // value is outside range
    assertFalse(isInteger.evaluate(new Text("2147483648")));
    assertFalse(isInteger.evaluate(new Text("1234567891111")));
    assertTrue(isInteger.evaluate(new Text("-123")));
    assertFalse(isInteger.evaluate(new Text("letters**")));
    assertFalse(isInteger.evaluate(new Text("12.55")));
    assertFalse(isInteger.evaluate(new Text("fa7875rt")));
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsNumericTest {
  private IsNumeric isNumeric = new IsNumeric();

  @Test
  public void isNumericTest(){
    assertTrue(isNumeric.evaluate(new Text("0")));
    assertTrue(isNumeric.evaluate(new Text("1234567891111")));
    assertTrue(isNumeric.evaluate(new Text("-123")));
    assertTrue(isNumeric.evaluate(new Text("-123.456")));
    assertFalse(isNumeric.evaluate((new Text("(343434)"))));
    assertFalse(isNumeric.evaluate(new Text("letters**")));
    assertFalse(isNumeric.evaluate(new Text("letters546")));
  }
}

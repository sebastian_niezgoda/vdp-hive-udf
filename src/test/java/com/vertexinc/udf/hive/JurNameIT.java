package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF.*;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class JurNameIT extends BaseOSeriesIT{

  private JurName jurName = new JurName();

  @Before
  public void before() throws UDFArgumentException {
    jurName.setLoggedInUser("open.vedemo");
    jurName.setLoggedInApplication("open.vedemo", Boolean.TRUE);
    jurName.setOSeriesConfig(getoSeriesProps());

    jurName.initialize(new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    });
  }

  @After
  public void after() throws IOException {
    jurName.close();
    jurName = new JurName();
  }

  @Test
  public void testJurName_1() throws HiveException {
    String taid = "700151610";  // Ontario Canada
    String jur = "State";
    // test our results

    // the value exists
    Object result1 = jurName.evaluate(new DeferredObject[]{
            new DeferredJavaObject(taid), new DeferredJavaObject(jur)});

    assertEquals("ONTARIO", result1.toString());
  }

  @Test
  public void testJurName_2() throws HiveException {
    // arguments are null
    String taid = "391013000";
    String jur = "City";

    Object result2 = jurName.evaluate(new DeferredObject[]{
            new DeferredJavaObject(taid), new DeferredJavaObject(jur)});

    assertEquals("PHILADELPHIA", result2.toString());
  }

  @Test
  public void testJurName_3() throws HiveException {
    // bad taid
    String taid = "700909500";
    String jur = "State";

    Object result3 = jurName.evaluate(new DeferredObject[]{
            new DeferredJavaObject(taid), new DeferredJavaObject(jur)});

    assertNull(result3);
  }

  @Test
  public void testJurName_4() throws HiveException {
    // bad jur name
    String taid = "391013000";
    String jur = "Cityxx";

    Object result4 = jurName.evaluate(new DeferredObject[]{
            new DeferredJavaObject(taid), new DeferredJavaObject(jur)});

    assertNull(result4);
  }

  @Test
  public void testJurName_5() throws HiveException {
    // arguments are null
    String taid = "10014151";
    String jur = "State";

    Object result2 = jurName.evaluate(new DeferredObject[]{
            new DeferredJavaObject(taid), new DeferredJavaObject(jur)});

    assertEquals("ALABAMA", result2.toString());
  }
}

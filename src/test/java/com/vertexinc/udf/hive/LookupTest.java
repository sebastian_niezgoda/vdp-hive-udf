package com.vertexinc.udf.hive;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.JavaConstantStringObjectInspector;
import org.apache.hadoop.io.Text;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class LookupTest extends BaseHDFSTest {

  @Test
  public void evaluate() throws Exception {
    List<String> inputValues = Arrays.asList(new String[] {"val1", "val2"});
    String outputValue = "val3";

    String lookupName = "test-lookup";

    Lookup lookup = new Lookup();
    lookup.setLoggedInApplication(systemIdentifier);
    lookup.setLoggedInUser("User1");

    Map<String, Lookup.VELookup> lookups = new HashMap<>();
    Lookup.VELookup veLookup = new Lookup.VELookup(lookupName);
    lookups.put(lookupName, veLookup);
    veLookup.addLookup(inputValues.get(0).toUpperCase() + "|" + inputValues.get(1).toUpperCase(), outputValue);
    lookup.setLookups(lookups);

    ObjectInspector[] arguments = new ObjectInspector[3];
    arguments[0] = new JavaConstantStringObjectInspector(inputValues.get(0));
    arguments[1] = new JavaConstantStringObjectInspector(inputValues.get(1));
    arguments[2] = new JavaConstantStringObjectInspector(lookupName);
    lookup.initialize(arguments);

    GenericUDF.DeferredObject[] args = new GenericUDF.DeferredObject[3];
    args[0] = new GenericUDF.DeferredJavaObject(inputValues.get(0));
    args[1] = new GenericUDF.DeferredJavaObject(inputValues.get(1));
    args[2] = new GenericUDF.DeferredJavaObject(lookupName);

    Object result = lookup.evaluate(args);
    assertNotNull(result);

    Text text = (Text) result;
    assertEquals(outputValue, text.toString());

    args[0] = new GenericUDF.DeferredJavaObject(inputValues.get(1));
    args[1] = new GenericUDF.DeferredJavaObject(inputValues.get(0));

    result = lookup.evaluate(args);
    assertNotNull(result);

    text = (Text) result;
    assertTrue(StringUtils.isEmpty(text.toString()));
  }
}
package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumColsTest {

  @Test
  public void evaluate() {
    SumCols sumCols = new SumCols();
    
    assertEquals(new Text("-9388.1869788"), sumCols.evaluate(new Text("(9,388.18697877)")));
    assertEquals(new Text("-9378.1869788"), sumCols.evaluate(new Text("(9,388.18697877)"), new Text("10")));
    assertEquals(new Text("13"), sumCols.evaluate(new Text("1"), new Text("10"), new Text("2")));
    assertEquals(new Text("-97"), sumCols.evaluate(new Text("1"), new Text("($1,00.00)"), new Text("aa"), new Text("2")));
    assertEquals(new Text("16.99"), sumCols.evaluate(new Text("15.99"), new Text("1")));
  }
}
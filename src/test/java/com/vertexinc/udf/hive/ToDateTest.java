package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ToDateTest {
  ToDate toDate = new ToDate();
  
  @Test
  public void toDateTest(){
    assertEquals("2015-05-03",toDate.evaluate(new Text("5/3/15"),new Text("MM/dd/yy")).toString());
    assertEquals("2015-05-03", toDate.evaluate(new Text("5/3/2015")).toString());
    assertEquals("2015-05-03", toDate.evaluate(new Text("5/3/15")).toString());
    assertEquals("0010-09-05", toDate.evaluate(new Text("5-3-2015")).toString());
    assertEquals("0005-03-15", toDate.evaluate(new Text("5-3-15")).toString());
    assertEquals("2015-05-03", toDate.evaluate(new Text("3-MAY-15")).toString());
    assertEquals("2015-05-03", toDate.evaluate(new Text("3-MAY-2015")).toString());
    assertEquals("2015-05-03", toDate.evaluate(new Text("MAY-3-15")).toString());
    assertEquals("2015-04-12", toDate.evaluate(new Text("20150412")).toString());
    assertEquals("2012-03-01", toDate.evaluate(new Text("12-3-01")).toString());
    assertNull(null, toDate.evaluate(new Text("12;3l;01")));
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ToDoubleTest {
  private ToDouble td = new ToDouble();

  @Test
  public void toDoubleTest(){
    assertEquals("-9388.18", td.evaluate(new Text("(9,388.18  )   ")).toString());
    assertEquals("-9388", td.evaluate(new Text("(9388)")).toString());
    assertEquals("-0.18",td.evaluate(new Text("(.18)")).toString());
    assertEquals("-0",td.evaluate(new Text("(0)")).toString());
    assertEquals("0",td.evaluate(new Text("")).toString());
    assertEquals("0",td.evaluate(new Text("dd")).toString());
    assertEquals("0",td.evaluate(null).toString());
    assertEquals("0",td.evaluate(new Text("dd")).toString());
    assertEquals("-0.18",td.evaluate(new Text("(18 % )")).toString());
    assertEquals("-0.18",td.evaluate(new Text("(18 % ) ")).toString());
    assertEquals("0.01",td.evaluate(new Text(" 1 % ")).toString());
    assertEquals("1",td.evaluate(new Text("100%")).toString());
    assertEquals("-1",td.evaluate(new Text(" ( 100 % ) ")).toString());
    assertEquals("0.078",td.evaluate(new Text("7.8%")).toString());
    assertEquals("0.005",td.evaluate(new Text(".005")).toString());
  }
}

package com.vertexinc.udf.hive;

import org.apache.hadoop.io.Text;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ToNumberTest {
  private ToNumber toNum = new ToNumber();
  
  @Test
  public void toNumberTest(){

    // I ascertained the expected results by running the ToNumber function from field-dev, not sure if that's right though
    assertEquals("0", toNum.evaluate(new Text("-92233720368547758085574"), new Text("INT")).toString());
    assertEquals("-9223372036854775808",toNum.evaluate(new Text("-9223372036854775808"), new Text("INT")).toString());
    assertEquals("-9223372036854775808", toNum.evaluate(new Text("-9223372036854775808"), new Text("BIGINT")).toString());
    assertEquals("0.005",toNum.evaluate(new Text(".005"), new Text("DOUBLE")).toString());
    assertEquals("-9388.1869788",toNum.evaluate(new Text("(9,388.18697877)"), new Text("DOUBLE")).toString());
    assertEquals("-1234567890123.1235",toNum.evaluate(new Text("(1,234,567,890,123.123456789)"), new Text("DOUBLE")).toString());
    assertEquals("-21000000000001.125",toNum.evaluate(new Text("(21,000,000,000,001.123456789)"), new Text("DOUBLE")).toString());
    assertEquals("-123.1234568",toNum.evaluate(new Text("(123.123456789)"), new Text("DOUBLE")).toString());
    assertEquals("-9388.18",toNum.evaluate(new Text("(9,388.18)"), new Text("DECIMAL")).toString());
    assertEquals("-9388",toNum.evaluate(new Text("(9,388.10)"), new Text("INT")).toString());
    assertEquals("-9389",toNum.evaluate(new Text("(9,388.51)"), new Text("INT")).toString());
    assertEquals("-9388", toNum.evaluate(new Text("(9,388)"), new Text("INT")).toString());
    assertEquals("-9388",toNum.evaluate(new Text("($9,388)"), new Text("INT")).toString());
    assertEquals("9388",toNum.evaluate(new Text("$9,388"), new Text("INT")).toString());
    assertEquals("0",toNum.evaluate(new Text("(x9,388)"), new Text("INT")).toString());
  }
}

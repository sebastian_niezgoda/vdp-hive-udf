package com.vertexinc.udf.hive;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.BufferedReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class VrtxLookupTest {
  private VrtxLookup vrtxLookup = new VrtxLookup();

  @Mock
  private BufferedReader bufferedReader;

  @Before
  public void before() throws UDFArgumentException {
    initMocks(this);

    vrtxLookup.setBr(bufferedReader);
    vrtxLookup.setLoggedInUser("open");
    vrtxLookup.setLoggedInApplication("vedemo");
  }

  @Test(expected = UDFArgumentException.class)
  public void vrtxLookupTest_Null() throws UDFArgumentException {
    ObjectInspector[] inspectors = new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    };

    vrtxLookup.initialize(inspectors);
  }

  @Test
  public void vrtxLookupTest1() throws HiveException, IOException {
    ObjectInspector[] inspectors = new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    };

    vrtxLookup.initialize(inspectors);

    String col1 = "hello";
    String delimiter = "\t";
    String expectedResponse = "resp";
    String filename = "vrtxLookupTest1";

    when(bufferedReader.readLine()).thenReturn(col1 + delimiter + expectedResponse).thenReturn(null);

    Object result = vrtxLookup.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject(col1),
            new GenericUDF.DeferredJavaObject(filename)
    });

    assertNotNull(result);
    assertEquals(expectedResponse, result.toString());
  }

  @Test
  public void vrtxLookupTest() throws HiveException, IOException {
    ObjectInspector[] inspectors = new ObjectInspector[]{
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector,
            PrimitiveObjectInspectorFactory.javaStringObjectInspector
    };

    vrtxLookup.initialize(inspectors);

    String col1 = "hello";
    String col2 = "world";
    String delimiter = "\t";
    String expectedResponse = "resp";
    String filename = "vrtxLookupTest";

    when(bufferedReader.readLine()).thenReturn(col1 + delimiter + col2 + delimiter + expectedResponse).thenReturn(null);

    Object result = vrtxLookup.evaluate(new GenericUDF.DeferredObject[]{
            new GenericUDF.DeferredJavaObject(col1),
            new GenericUDF.DeferredJavaObject(col2),
            new GenericUDF.DeferredJavaObject(filename)
    });

    assertNotNull(result);
    assertEquals(expectedResponse, result.toString());
  }
}

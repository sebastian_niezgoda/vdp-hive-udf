-- addmonths

SELECT IF(NULL <=> DEFAULT.addMonths(null, null), 'working', 'not') UNION ALL
SELECT IF(NULL <=> DEFAULT.addMonths('2018-06-18', null), 'working', 'not') UNION ALL
SELECT IF('2018-07-01' = DEFAULT.addMonths('2018-06-18', 1), 'working', 'not') UNION ALL
SELECT IF('2018-05-01' = DEFAULT.addMonths('2018-06-18', -1), 'working', 'not') UNION ALL
SELECT IF('2018-11-01' = DEFAULT.addMonths('2018-12-31', -1), 'working', 'not') UNION ALL
SELECT IF('2019-01-01' = DEFAULT.addMonths('2018-12-31', 1), 'working', 'not') UNION ALL
SELECT IF('2015-02-01' = DEFAULT.addMonths('2016-02-29', -12), 'working', 'not') UNION ALL
SELECT IF('2016-03-01' = DEFAULT.addMonths('2016-02-29', 1), 'working', 'not');

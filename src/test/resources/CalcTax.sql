-- have to run these manually as a select as statement in order to get any meaningful results back

SELECT IF('900.0' = calcTax('CompanyB','DivisionB','17011','19333','Product2','15000.00'), 'working', 'not');
SELECT IF('1.98' = calcTax('CompanyC','DivisionC','90089','19333','Product1','33.00'), 'working', 'not');
SELECT IF('9.0' = calcTax('CompanyA','DivisionA','90089','19333','Product1','150.00'), 'working', 'not');
SELECT IF('0.6' = calcTax('CompanyB','DivisionB','17011','19333','Product1','10.00'), 'working', 'not');
SELECT IF('3.0' = calcTax('CompanyC','DivisionC','90089','19333','Product2','50.00'), 'working', 'not');
SELECT IF('10.2' = calcTax('CompanyA','DivisionA','90089','19333','Product2','170.00'), 'working', 'not');
SELECT IF('6000.0' = calcTax('CompanyB','DivisionB','17011','19333','Product5','100000.00'), 'working', 'not');
SELECT IF('5.28' = calcTax('CompanyC','DivisionC','90089','17011','Product1','88.00'), 'working', 'not');
SELECT IF('9.0' = calcTax('CompanyA','DivisionA','90089','19333','Product1','150.00'), 'working', 'not');

SELECT calcTax('CompanyB','DivisionB','17011','19333','Product2','15000.00') AS b FROM cdp_dev.calctax7

SELECT calcTax('CompanyB','DivisionB','17011','19333','Product1','10.00') AS a FROM cdp_dev.calctax7
-- default: fixer
SELECT IF (0.2371 = DEFAULT.currXRate('2015-09-11', 'EUR', 'PLN'), 'working', 'not') UNION ALL
SELECT IF (0.0032 = DEFAULT.currXRate('2015-09-12', 'EUR', 'HUF'), 'working', 'not') UNION ALL
SELECT IF (0.0032 = DEFAULT.currXRate('2015-09-10', 'EUR', 'HUF'), 'working', 'not') UNION ALL
SELECT IF (0.0033 = DEFAULT.currXRate('2015-03-24', 'EUR', 'HUF'), 'working', 'not') UNION ALL
SELECT IF (0.2378 = DEFAULT.currXRate('2015-09-14', 'EUR', 'PLN'), 'working', 'not') UNION ALL
SELECT IF (0.0 = DEFAULT.currXRate('2014-12-27', 'GBP', 'CNY'), 'working', 'not') UNION ALL
SELECT IF (0.0 = DEFAULT.currXRate('2015-11-12', 'GBP', 'ISK'), 'working', 'not') UNION ALL
SELECT IF (0.0 = DEFAULT.currXRate('2016-11-02', 'GBP', 'UAH'), 'working', 'not') UNION ALL

-- europe
SELECT IF (0.74519 = DEFAULT.currXRate('2016-01-10', 'EUR', 'GBP', 'europe'), 'working', 'not') UNION ALL
-- for caching test:
SELECT IF (0.85085 = DEFAULT.currXRate('2016-07-07', 'EUR', 'GBP', 'europe'), 'working', 'not') UNION ALL
-- the endpoint does not return a value for this date, so the result should be 0.0
SELECT IF (0.0 = DEFAULT.currXRate('2014-01-01', 'EUR', 'GBP', 'europe'), 'working', 'not') UNION ALL
SELECT IF (0.7903 = DEFAULT.currXRate('2014-07-16', 'EUR', 'GBP', 'europe'), 'working', 'not') UNION ALL

-- poland
SELECT IF (0.2301 = DEFAULT.currXRate('2016-09-12', 'PLN', 'EUR', 'poland'), 'working', 'not') UNION ALL
SELECT IF (17.2551 = DEFAULT.currXRate('2016-09-12', 'PLN', 'INR', 'poland'), 'working', 'not') UNION ALL
SELECT IF (0.2376 = DEFAULT.currXRate('2015-09-11', 'PLN', 'EUR', 'poland'), 'working', 'not') UNION ALL
SELECT IF (0.2343 = DEFAULT.currXRate('2017-09-13', 'PLN', 'EUR', 'poland'), 'working', 'not') UNION ALL
SELECT IF (0.2346 = DEFAULT.currXRate('2016-08-14', 'PLN', 'EUR', 'poland'), 'working', 'not') UNION ALL
SELECT IF (0.2363 = DEFAULT.currXRate('2015-10-18', 'PLN', 'EUR', 'poland'), 'working', 'not');


-- isEmpty

-- can't run this one because it returns an exception
--SELECT IF('true' = isEmpty(NULL), 'working', 'not') UNION ALL
SELECT IF(FALSE = isEmpty('100'), 'working', 'not') UNION ALL
SELECT IF(TRUE = isEmpty(''), 'working', 'not') UNION ALL
SELECT IF(TRUE = isEmpty(' '), 'working', 'not');
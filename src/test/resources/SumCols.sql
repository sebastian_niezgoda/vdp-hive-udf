-- sum cols

SELECT IF('-9388.1869788' = sumCols('(9,388.18697877)'), 'working', 'not');
SELECT IF('-9378.1869788' = sumCols('(9,388.18697877)', '10'), 'working', 'not');
SELECT IF('13' = sumCols('1','10','2'), 'working', 'not');
SELECT IF('-97' = sumCols('1', '($1,00.00)', 'aa', '2'), 'working', 'not');
SELECT IF('16.99' = sumCols('15.99', '1'), 'working', 'not');
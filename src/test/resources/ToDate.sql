SELECT IF('2015-05-03' = toDate('5/3/15', 'MM/dd/yy'), 'working', 'not') UNION ALL
SELECT IF('2015-05-03' = toDate('5/3/2015'), 'working', 'not') UNION ALL
SELECT IF('2015-05-03' = toDate('5/3/15'), 'working', 'not') UNION ALL
SELECT IF('0010-09-05' = toDate('5-3-2015'), 'working', 'not') UNION ALL
SELECT IF('0005-03-15' = toDate('5-3-15'), 'working', 'not') UNION ALL
SELECT IF('2015-05-03' = toDate('3-MAY-15'), 'working', 'not') UNION ALL
SELECT IF('2015-05-03' = toDate('3-MAY-2015'), 'working', 'not') UNION ALL
SELECT IF('2015-05-03' = toDate('MAY-3-15'), 'working', 'not') UNION ALL
SELECT IF('2015-04-12' = toDate('20150412'), 'working', 'not') UNION ALL
SELECT IF('2012-03-01' = toDate('12-3-01'), 'working', 'not') UNION ALL
SELECT IF(NULL <=> toDate("12;3l;01"), 'working', 'not');

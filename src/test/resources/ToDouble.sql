-- running this is extremely slow for some reason
SELECT IF('-9388.18' = toDouble('(9,388.18  )   '), 'working', 'not') UNION ALL
SELECT IF('-9388' = toDouble('(9388)'), 'working', 'not') UNION ALL
SELECT IF('-0.18' = toDouble('(.18)'), 'working', 'not') UNION ALL
SELECT IF('-0' = toDouble('(0)'), 'working', 'not') UNION ALL
SELECT IF('0' = toDouble(''), 'working', 'not') UNION ALL
SELECT IF('0' = toDouble('dd'), 'working', 'not') UNION ALL
SELECT IF('0' = toDouble(null), 'working', 'not') UNION ALL
SELECT IF('0' = toDouble('dd'), 'working', 'not') UNION ALL
SELECT IF('-0.18' = toDouble('(18 % )'), 'working', 'not') UNION ALL
SELECT IF('-0.18' = toDouble('(18 % ) '), 'working', 'not') UNION ALL
SELECT IF('0.01' = toDouble(' 1 % '), 'working', 'not') UNION ALL
SELECT IF('1' = toDouble('100%'), 'working', 'not') UNION ALL
SELECT IF('-1' = toDouble(' ( 100 % ) '), 'working', 'not') UNION ALL
SELECT IF('0.078' = toDouble('7.8%'), 'working', 'not') UNION ALL
SELECT IF('0.005' = toDouble('.005'), 'working', 'not');
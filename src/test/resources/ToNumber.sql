-- have to be run individually

SELECT IF('0' =  toNumber('-92233720368547758085574', 'INT'), 'working', 'not');
SELECT IF('-9223372036854775808' = toNumber('-9223372036854775808', 'INT'), 'working', 'not');
SELECT IF('-9223372036854775808' =  toNumber('-9223372036854775808', 'BIGINT'), 'working', 'not');
SELECT IF('0.005' = toNumber('.005', 'DOUBLE'), 'working', 'not');
SELECT IF('-9388.1869788' = toNumber('(9,388.18697877)', 'DOUBLE'), 'working', 'not');
SELECT IF('-1234567890123.1235' = toNumber('(1,234,567,890,123.123456789)', 'DOUBLE'), 'working', 'not');
SELECT IF('-21000000000001.125' = toNumber('(21,000,000,000,001.123456789)', 'DOUBLE'), 'working', 'not');
SELECT IF('-123.1234568' = toNumber('(123.123456789)', 'DOUBLE'), 'working', 'not');
SELECT IF('-9388.18' = toNumber('(9,388.18)', 'DECIMAL'), 'working', 'not');
SELECT IF('-9388' = toNumber('(9,388.10)', 'INT'), 'working', 'not');
SELECT IF('-9389' = toNumber('(9,388.51)', 'INT'), 'working', 'not');
SELECT IF('-9388' =  toNumber('(9,388)', 'INT'), 'working', 'not');
SELECT IF('-9388' = toNumber('($9,388)', 'INT'), 'working', 'not');
SELECT IF('9388' = toNumber('$9,388', 'INT'), 'working', 'not');
SELECT IF('0' = toNumber('(x9,388)', 'INT'), 'working', 'not');